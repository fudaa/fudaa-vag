/*
 * @creation     1998-12-04
 * @modification $Date: 2007-06-28 09:28:18 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.vag;
import org.fudaa.fudaa.commun.impl.Fudaa;
/**
 * L'application cliente Vag.
 *
 * @version      $Id: Vag.java,v 1.10 2007-06-28 09:28:18 deniger Exp $
 * @author       Axel von Arnim 
 */
public class Vag {
  public static void main(String[] args) {
    Fudaa f=new Fudaa();
    f.launch(args, VagImplementation.informationsSoftware(),true);
    f.startApp(new VagImplementation());
  }
}
