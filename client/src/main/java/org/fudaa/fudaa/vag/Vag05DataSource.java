/*
 * @creation     1999-08-04
 * @modification $Date: 2007-06-28 09:28:18 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.vag;
/**
 * @version      $Revision: 1.7 $ $Date: 2007-06-28 09:28:18 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class Vag05DataSource {
  private org.fudaa.dodico.corba.vag.SResultatsInterOrth resultats_;
  private double alpha_;
  private double T_;
  private int nb_points_= 0;
  public Vag05DataSource(
    org.fudaa.dodico.corba.vag.SResultatsInterOrth _resultats,
    int _alpha,
    double _T) {
    resultats_= _resultats;
    alpha_= _alpha;
    T_= _T;
  }
  //  public void setGeometrie(int nb_noeuds,Point3d[] noeuds,int nb_indices,int[] indices,Color[] _c)
  public double[] getDonnees(double t) {
    if (nb_points_ == 0)
      for (int i= 0; i < resultats_.interOrth.length; i++)
        nb_points_ += resultats_.interOrth[i].pas.length;
    //		System.out.println("k="+k);	}
    double[] h= new double[nb_points_];
    int k= 0;
    for (int i= 0; i < resultats_.interOrth.length; i++)
      for (int j= 0; j < resultats_.interOrth[i].pas.length; j++) {
        double pi= Math.PI;
        double H= resultats_.interOrth[i].pas[j].hauteurHoule;
        int indice= resultats_.interOrth[i].pas[j].numeroPas;
        //					double longueurOnde=resultats_.interOrth[i].pas[j].longueurOnde;
        double phi= 1d / alpha_ * 2 * pi * indice;
        h[k]= H * Math.cos(phi - 2 * pi * t / T_);
        //h[k]=resultats_.interOrth[i].pas[j].hauteurHoule*Math.cos(resultats_.interOrth[i].pas[j].numeroPas*Math.PI*2*resultats_.interOrth[i].pas[j].longueurOnde/alpha_-2*Math.PI*t/T_);
        k++;
      }
    return h;
  }
}
