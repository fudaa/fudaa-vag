/*
 * @creation     1999-03-01
 * @modification $Date: 2007-06-28 09:28:18 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.vag;
import com.memoire.bu.BuPreferences;

import org.fudaa.fudaa.commun.FudaaAstuces;
import org.fudaa.fudaa.commun.FudaaAstucesAbstract;
/**
 * @version      $Revision: 1.9 $ $Date: 2007-06-28 09:28:18 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class VagAstuces extends FudaaAstucesAbstract {
  public static VagAstuces VAG= new VagAstuces();
  protected FudaaAstucesAbstract getParent() {
    return FudaaAstuces.FUDAA;
  }
  protected BuPreferences getPrefs() {
    return VagPreferences.VAG;
  }
  /*
  public static void main( String argv[])
  {
    System.out.println("nb ligne de Fudaa "+FudaaAstuces.FUDAA.getNombreLigne());
    System.out.println("nb ligne de Vag "+VagAstuces.VAG.getNombreLigne());
    System.out.println(VagAstuces.VAG.getAstuce());
  }*/
}
