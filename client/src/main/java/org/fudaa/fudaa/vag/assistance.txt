# parametres

PARAMETRE_ACTION
Vous pouvez modifier
les parametres du
calcul.

PANEL01_FOCUS
Remplissez les champs.
Certains champs ne sont
pas �ditables.
Vous pouvez aussi importer
un fichier VAG dans le
menu "Fichier".

PANEL01_NOM_ETUDE_FOCUS
Entrez un nom pour l'�tude.

PANEL01_CODE_INCONNU_FOCUS
Ce code est pr�sent dans
les fichiers VAG01, mais
je ne sais pas � quoi il
correspond...

PANEL01_TERMINAL_FOCUS
Ce champ est temporaire
et vou� � disparaitre.

PANEL01_DIGITALISEUR_FOCUS
Ce champ est temporaire
et vou� � disparaitre.

PANEL01_TRACEUR_FOCUS
Ce champ est temporaire
et vou� � disparaitre.

PANEL01_SEG_ARRET_DEBUTX_FOCUS
Segment d'arret=endroit
ou l'on d�sire connaitre
la hauteur et la
direction de la houle.

PANEL01_SEG_ARRET_DEBUTY_FOCUS
Segment d'arret=endroit
ou l'on d�sire connaitre
la hauteur et la
direction de la houle.

PANEL01_SEG_ARRET_FINX_FOCUS
Segment d'arret=endroit
ou l'on d�sire connaitre
la hauteur et la
direction de la houle.

PANEL01_SEG_ARRET_FINY_FOCUS
Segment d'arret=endroit
ou l'on d�sire connaitre
la hauteur et la
direction de la houle.

PANEL01_FLECHE_NORDX_FOCUS
La fl�che Nord indique
la position du Nord
sur la fenetre de r�sultats.

PANEL01_FLECHE_NORDY_FOCUS
La fl�che Nord indique
la position du Nord
sur la fenetre de r�sultats.

PANEL01_FLECHE_NORD_DIR_FOCUS
La fl�che Nord indique
la position du Nord
sur la fenetre de r�sultats.

PANEL01_CODE_IMP_FOCUS
Le pas d'impression est
un nombre entier de pas de
calcul. Il r�gle la
r�solution (densit�) des
r�sultats � l'�cran.

PANEL01_PAS_CALCUL_FOCUS
Le pas de calcul est la
longueur d'onde que divise
la valeur enti�re entr�e �
l'�cran.

PANEL02_FOCUS
Si vous voulez changer
des param�tres d�j�
charg�s, utilisez
l'importation dans le
menu "Fichier".

PANEL03_FOCUS
Si vous voulez changer
des param�tres d�j�
charg�s, utilisez
l'importation dans le
menu "Fichier".

PANEL04_FOCUS
Remplissez les champs.
Certains champs ne sont
pas �ditables.
Vous pouvez aussi importer
un fichier VAG dans le
menu "Fichier".

# resultats

CALCULER_ACTION
Je lance le calcul
sur le serveur...

ORTHOGONALE_ACTION
Astuce :
En maintenant le bouton
gauche de la souris enfonc�
sur une orthogonale, vous
pouvez avoir les informations
associ�es au point s�lectionn�.

# specific tools
PLAN
Fonctionnalit� non encore
impl�ment�e.

TEXTURE
Fonctionnalit� non encore
impl�ment�e.

# divers
CREER_ACTION
Nouveau projet.
Le fichier doit avoir
l'extension ".vag".
Entrez les param�tres
(menu "Calcul") ou
importez des fichiers
Vag (menu "Fichier").

OUVRIR_ACTION
Ouvrir un projet Vag.
(fichier d'extension
".vag").

ENREGISTRERSOUS_ACTION
Le fichier doit avoir
l'extension ".vag".

CASCADE_ACTION
Cascade!
