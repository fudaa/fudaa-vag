/*
 * @file         VagImplementation.java
 * @creation     1998-10-02
 * @modification $Date: 2007-05-04 14:07:12 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.vag;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.geom.Rectangle2D;
import java.beans.PropertyVetoException;
import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;

import com.memoire.bu.*;

import org.fudaa.dodico.corba.dunes.ICalculDunes;
import org.fudaa.dodico.corba.dunes.ICalculDunesHelper;
import org.fudaa.dodico.corba.objet.IConnexion;
import org.fudaa.dodico.corba.vag.*;

import org.fudaa.dodico.dunes.DCalculDunes;
import org.fudaa.dodico.vag.DCalculVag;

import org.fudaa.ebli.calque.*;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMaillage;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.graphe.BGraphe;
import org.fudaa.ebli.tableau.EbliFilleTableau;
import org.fudaa.ebli.trace.BParametresGouraud;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.volume.BChampVecteurs;
import org.fudaa.ebli.volume.BGrilleReguliere;
import org.fudaa.ebli.volume.BGroupeLumiere;
import org.fudaa.ebli.volume.BGroupeStandard;
import org.fudaa.ebli.volume.BGroupeVolume;
import org.fudaa.ebli.volume.BLumiereDirectionnelle;
import org.fudaa.ebli.volume.EbliFilleVue3D;
import org.fudaa.ebli.volume.VolumePreferencesPanel;

import org.fudaa.fudaa.commun.FudaaAstucesAbstract;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.commun.dodico.FudaaDodicoTacheConnexion;
import org.fudaa.fudaa.commun.dodico.FudaaImplementation;
import org.fudaa.fudaa.commun.impl.FudaaFilleRapport;
import org.fudaa.fudaa.commun.projet.*;
import org.fudaa.fudaa.utilitaire.ServeurCopieEcran;

/**
 * L'implementation du client Vag.
 * 
 * @version $Revision: 1.24 $ $Date: 2007-05-04 14:07:12 $ by $Author: deniger $
 * @author Axel von Arnim
 */
public class VagImplementation extends FudaaImplementation implements FudaaParamListener, FudaaProjetListener {

  // public final static String LOCAL_UPDATE = ".";
  public static java.awt.Frame FRAME;
  public final static boolean IS_APPLICATION = true;
  public static ICalculVag SERVEUR_VAG;
  public static ICalculDunes SERVEUR_DUNES;
  public static IConnexion CONNEXION_VAG;
  public static IConnexion CONNEXION_DUNES;
  // VOIR SI ON PEUT VIRER CA:
  private IParametresVag vagParams;
  private IResultatsVag vagResults;
  protected static BuInformationsSoftware isVag_ = new BuInformationsSoftware();
  protected static BuInformationsDocument idVag_ = new BuInformationsDocument();
  protected final static int RECENT_COUNT = 10;
  protected VagFilleParametres fp_;
  protected FudaaProjet projet_;
  protected VagFilleCalques fCalques_;
  protected VagFilleMailleur fMailleur_;
  protected VagFilleGraphe fGraphe_;
  protected EbliFilleVue3D fVue3D_;
  protected FudaaFilleRapport fRapport_;
  protected EbliFilleTableau fTableau_;
  protected BArbreCalque arbre_;
  protected BGroupeCalque gc_;
  protected BuAssistant assistant_;
  protected BuTaskView taches_;
  protected FudaaParamEventView msgView_;
  protected FudaaProjetInformationsFrame fProprietes_;
  protected GrMaillage hauteur_houle_maillage;
  protected double[] hauteur_houle_valeurs;
  protected boolean j3d;
  protected boolean mailleurActive_;
  static {
    isVag_.name = "Vag";
    isVag_.version = "1.1-SNAPSHOT";
    isVag_.date = "05-07-2013";
    isVag_.rights = "Tous droits r�serv�s. CETMEF (c)1999-2013";
    isVag_.contact = "fudaa.cetmef@equipement.gouv.fr";
    isVag_.license = "GPL2";
    isVag_.languages = "fr,en";
    isVag_.logo = VagResource.VAG.getIcon("vag_logo");
    isVag_.banner = VagResource.VAG.getIcon("vag_banner");
    isVag_.http = "http://www.cetmef.developpement-durable.gouv.fr/fudaa-vag-r36.html";
    isVag_.update = "http://www.cetmef.developpement-durable.gouv.fr/fudaa-vag-r36.html";
    isVag_.man = FudaaLib.LOCAL_MAN + "vag/";
    isVag_.authors = new String[] { "Axel von Arnim" };
    isVag_.contributors = new String[] { "Equipes Dodico, Ebli et Fudaa", "Christophe Delhorbe", "Mickael Rubens", "Guillaume Poissonnier" };
    isVag_.documentors = new String[] { "Patrick Gomi" };
    isVag_.testers = new String[] { "Patrick Gomi" };
    BuPrinter.INFO_LOG = isVag_;
    BuPrinter.INFO_DOC = idVag_;
  }

  public BuInformationsSoftware getInformationsSoftware() {
    return isVag_;
  }

  public static BuInformationsSoftware informationsSoftware() {
    return isVag_;
  }

  public BuInformationsDocument getInformationsDocument() {
    if (projet_ != null) {
      System.out.println("non nul");
      return projet_.getInformationsDocument();
    }
    return idVag_;
  }

  public void init() {
    super.init();
    FRAME = getFrame();
    /* TEST JAVA3D */
    j3d = true;
    try {
      Class.forName("javax.media.j3d.BranchGroup");
    } catch (ClassNotFoundException ex) {
      j3d = false;
      System.out.println("Pas de 3D!!!");
    }
    fp_ = null;
    projet_ = null;
    fGraphe_ = null;
    fCalques_ = null;
    fMailleur_ = null;
    fRapport_ = null;
    fTableau_ = null;
    gc_ = null;
    aide_ = null;
    mailleurActive_ = false;
    try {
      setTitle(isVag_.name + " " + isVag_.version);
      BuMenuBar mb = getMainMenuBar();
      mb.addMenu(buildCalculMenu(IS_APPLICATION));
      ((BuMenu) mb.getMenu("IMPORTER")).addMenuItem("Code Vag", "IMPORTVAG", false);
      BuToolBar tb = getMainToolBar();
      tb.addSeparator();
      tb.addToolButton("Calculer", "CALCULER", false);
      setEnabledForAction("CREER", true);
      setEnabledForAction("OUVRIR", true);
      setEnabledForAction("REOUVRIR", true);
      setEnabledForAction("ENREGISTRER", false);
      setEnabledForAction("ENREGISTRERSOUS", false);
      setEnabledForAction("FERMER", false);
      setEnabledForAction("PROPRIETE", false);
      setEnabledForAction("QUITTER", true);
      setEnabledForAction("IMPORTER", true);
      setEnabledForAction("EXPORTER", false);
      setEnabledForAction("IMPRIMER", false);
      setEnabledForAction("AIDE_INDEX", true);
      setEnabledForAction("AIDE_ASSISTANT", true);
      BuMenuRecentFiles mr = (BuMenuRecentFiles) mb.getMenu("REOUVRIR");
      if (mr != null) {
        mr.setPreferences(VagPreferences.VAG);
        mr.setResource(VagResource.VAG);
        mr.setEnabled(true);
      }
      BuMainPanel mp = getApp().getMainPanel();
      BuColumn lc = mp.getLeftColumn();
      BuColumn rc = mp.getRightColumn();
      assistant_ = new VagAssistant();
      taches_ = new BuTaskView();
      BuScrollPane sp = new BuScrollPane(taches_);
      sp.setPreferredSize(new Dimension(150, 80));
      // new BuTaskOperation(this, "SpyTask", "oprServeurCopie" ).run();
      rc.addToggledComponent("Assistant", "ASSISTANT", assistant_, this);
      rc.addToggledComponent(BuResource.BU.getString("T�ches"), "TACHE", sp, this);
      mp.setLogo(isVag_.logo);
      mp.setTaskView(taches_);
      msgView_ = new FudaaParamEventView();
      BuScrollPane sp2 = new BuScrollPane(msgView_);
      sp2.setPreferredSize(new Dimension(150, 80));
      lc.addToggledComponent("Messages", "MESSAGE", sp2, this);
      setParamEventView(msgView_);
      FudaaParamChangeLog.CHANGE_LOG.setApplication((BuApplication) VagImplementation.FRAME, msgView_);
    } catch (Throwable t) {
      System.err.println("$$$ " + t);
      t.printStackTrace();
    }
  }

  public void start() {
    super.start();
    // about();
    // (new VagConnexion(this)).start();
    assistant_.changeAttitude(BuAssistant.PAROLE, "Bienvenue !\n" + isVag_.name + " " + isVag_.version);
    BuMainPanel mp = getMainPanel();
    arbre_ = new BArbreCalque();
    gc_ = new BGroupeCalque();
    gc_.setTitle("Calques Vag");
    // arbre_.setCalque(gc_);
    JScrollPane sp = new JScrollPane(arbre_);
    sp.setSize(150, 150);
    BuColumn rc = mp.getRightColumn();
    rc.addToggledComponent("Calques", "CALQUE", sp, this);
    projet_ = new FudaaProjet(getApp(), new FudaaFiltreFichier("vag"));
    projet_.addFudaaProjetListener(this);
    mp.doLayout();
    mp.validate();
    assistant_.addEmitters((Container) getApp());
    assistant_.changeAttitude(BuAssistant.ATTENTE, "Vous pouvez cr�er un\nnouveau projet Vag\nou en ouvrir un");
    // Preferences Vag
    // VagPreferences.VAG.applyOn(this);
    // ajoutePreferences(new VolumePreferencesPanel(this));
    fProprietes_ = new FudaaProjetInformationsFrame(this);
    fProprietes_.setProjet(projet_);
    FudaaParamEventProxy.FUDAA_PARAM.addFudaaParamListener(this);
    // Graphes (doit etre fait avant calque!!)
    if (fGraphe_ == null) {
      fGraphe_ = new VagFilleGraphe(getApp(), getInformationsDocument());
      installContextHelp(fGraphe_.getRootPane(), "vag/p-graphe.html");
      fGraphe_.setSize(new Dimension(640, 480));
      try {
        fGraphe_.setClosed(true);
      } catch (PropertyVetoException e) {
        addInternalFrame(fGraphe_);
      }
    }
    // Tableau
    if (fTableau_ == null) {
      fTableau_ = new EbliFilleTableau();
      fTableau_.setInformationsDocument(getInformationsDocument());
      installContextHelp(fTableau_.getRootPane(), "vag/p-tableau.html");
      fTableau_.setClosable(false);
      fTableau_.setTable(new VagTableauResultats());
      fTableau_.setBackground(Color.white);
      fTableau_.setSize(new Dimension(640, 480));
      try {
        fTableau_.setClosed(true);
      } catch (PropertyVetoException e) {
        addInternalFrame(fTableau_);
      }
    }
  }

  public FudaaAstucesAbstract getAstuces() {
    return VagAstuces.VAG;
  }

  public BuPreferences getApplicationPreferences() {
    return VagPreferences.VAG;
  }

  public void setParamEventView(FudaaParamEventView v) {
    msgView_ = v;
  }

  public FudaaParamEventView getParamEventView() {
    return msgView_;
  }

  // Accesseurs
  public VagFilleCalques getFilleCalques() {
    return fCalques_;
  }

  // Menu Calcul
  protected BuMenu buildCalculMenu(boolean _app) {
    BuMenu r = new BuMenu("Vag", "CALCUL");
    r.addSeparator("Outils");
    // r.addMenuItem("Calculatrice", "CALCULATRICE", true);
    r.addSeparator("Param�tres");
    r.addMenuItem("Param�tres", "PARAMETRE", false);
    r.addMenuItem("Mailleur", "MAILLEUR", false);
    r.addMenuItem("Rapport", "RAPPORT", false);
    r.addMenuItem("Calculer", "CALCULER", false);
    // r.addMenuItem("Pas d'impression" ,"PASIMPRESSION",false );
    r.addSeparator("R�sultats");
    r.addMenuItem("Orthogonales", "ORTHOGONALE", VagResource.VAG.getIcon("houle"), false);
    r.addMenuItem("Vue 3D", "VUE3D", VagResource.VAG.getIcon("houle"), false);
    BuMenu exp = new BuMenu("Exporter r�sultats", "EXPORTRES");
    exp.addMenuItem("Orthogonales", "EXPORT05", true);
    exp.addMenuItem("Inter-Orthogonales", "EXPORTIO", true);
    r.addSubMenu(exp, false);
    r.addMenuItem("Informations calcul", "INFOCALCUL", false);
    return r;
  }

  // Actions
  public void actionPerformed(ActionEvent _evt) {
    String action = _evt.getActionCommand();
    System.err.println("ACTION=" + action);
    int i = action.indexOf('(');
    String arg = null;
    if (i >= 0) {
      arg = action.substring(i + 1, action.length() - 1);
      action = action.substring(0, i);
    }
    if (action.equals("CREER")) creer();
    else if (action.equals("OUVRIR")) ouvrir(null);
    else if (action.equals("REOUVRIR")) ouvrir(arg);
    else if (action.equals("ENREGISTRER")) enregistrer();
    else if (action.equals("ENREGISTRERSOUS")) enregistrerSous();
    else if (action.equals("FERMER")) fermer();
    else if (action.startsWith("IMPORTVAG")) importer(action.length() > 9 ? action.substring(9) : null);
    else if (action.endsWith("IN") && (action.startsWith("ASC") || action.startsWith("XML"))) importerProjet();
    else if (action.equals("PROPRIETE")) {
      if (fProprietes_.getDesktopPane() != getMainPanel().getDesktop()) addInternalFrame(fProprietes_);
      else
        activateInternalFrame(fProprietes_);
    }

    else if (action.equals("PARAMETRE")) parametre();
    else if (action.equals("MAILLEUR")) mailleur();
    else if (action.equals("RAPPORT")) rapport();
    else if (action.equals("PASIMPRESSION")) pas_impression();
    else if (action.equals("CALCULER")) calculer();
    else if (action.equals("ORTHOGONALE")) orthogonale();
    else if (action.equals("VUE3D")) vue3d();
    else if (action.equals("EXPORT05")) exportResultats(VagResource.VAG05);
    else if (action.equals("EXPORTIO")) exportResultats(VagResource.VAGIO);
    else if (action.equals("INFOCALCUL")) {
      if (projet_.containsResult(VagResource.VAGOUT)) {
        SResultatsOUT out = (SResultatsOUT) projet_.getResult(VagResource.VAGOUT);
        afficheResultatOUT(out);
      }
    } else if (action.equals("ASSISTANT") || action.equals("TACHE") || action.equals("CALQUE")) {
      BuColumn rc = getMainPanel().getRightColumn();
      rc.toggleComponent(action);
      setCheckedForAction(action, rc.isToggleComponentVisible(action));
    } else if (action.equals("MESSAGE")) {
      BuColumn lc = getMainPanel().getLeftColumn();
      lc.toggleComponent(action);
      setCheckedForAction(action, lc.isToggleComponentVisible(action));
    } else
      super.actionPerformed(_evt);
  }

  // LidoParamListener
  public void paramStructCreated(FudaaParamEvent e) {
    if (projet_.containsResults()) {
      new BuDialogMessage(getApp(), getInformationsSoftware(), "Vous avez modifi� un param�tre.\n"
          + "Les r�sultats ne sont plus valides:\n" + "ils vont etre effac�s.\n").activate();
      setEnabledForAction("RESULTAT", false);
      setEnabledForAction("EXPORTRES", false);
      setEnabledForAction("INFOCALCUL", false);
      projet_.clearResults();
    }
    if ((fMailleur_ != null) && e.getKey().equals(VagResource.VAG02)) {
      trieCotes(fMailleur_.getRectangle());
    }
    FudaaParamChangeLog.CHANGE_LOG.addChangeLog(e.getMessage());
    setEnabledForAction("ENREGISTRER", true);
  }

  public void paramStructDeleted(FudaaParamEvent e) {
    if (projet_.containsResults()) {
      new BuDialogMessage(getApp(), getInformationsSoftware(), "Vous avez modifi� un param�tre.\n"
          + "Les r�sultats ne sont plus valides:\n" + "ils vont etre effac�s.\n").activate();
      setEnabledForAction("RESULTAT", false);
      setEnabledForAction("EXPORTRES", false);
      setEnabledForAction("INFOCALCUL", false);
      projet_.clearResults();
    }
    FudaaParamChangeLog.CHANGE_LOG.addChangeLog(e.getMessage());
    setEnabledForAction("ENREGISTRER", true);
  }

  public void paramStructModified(FudaaParamEvent e) {
    String cmd = e.getMessage();
    if (projet_.containsResults()) {
      new BuDialogMessage(getApp(), getInformationsSoftware(), "Vous avez modifi� un param�tre.\n"
          + "Les r�sultats ne sont plus valides:\n" + "ils vont etre effac�s.\n").activate();
      setEnabledForAction("RESULTAT", false);
      setEnabledForAction("EXPORTRES", false);
      setEnabledForAction("INFOCALCUL", false);
      projet_.clearResults();
    }
    if ((e.getSource() == fMailleur_)) {
      if (cmd.endsWith(":rectangle")) {
        trieCotes(fMailleur_.getRectangle());
      }
      // if( (fp_!=null)&&(!mailleurActive_) ) fp_.grilleDisabled();
      mailleurActive_ = true;
    }
    FudaaParamChangeLog.CHANGE_LOG.addChangeLog(e.getMessage());
    setEnabledForAction("ENREGISTRER", true);
  }

  // fudaaprojet listener
  public void dataChanged(FudaaProjetEvent _e) {
    switch (_e.getID()) {
    case FudaaProjetEvent.RESULT_ADDED:
    case FudaaProjetEvent.RESULTS_CLEARED:
      FudaaParamChangeLog.CHANGE_LOG.setDirty(true);
      setEnabledForAction("ENREGISTRER", true);
      break;
    default:
      break;
    }
  }

  public void statusChanged(FudaaProjetEvent _e) {
    if (_e.getID() == FudaaProjetEvent.HEADER_CHANGED) {
      FudaaParamChangeLog.CHANGE_LOG.setDirty(true);
      setEnabledForAction("ENREGISTRER", true);
      System.out.println("infos modifiees");
      if ((_e.getSource()) instanceof FudaaProjet) {
        System.out.println("maj des internalframe");
        majInformationsDocument(((FudaaProjet) _e.getSource()).getInformationsDocument());
      }
    }
  }

  private void majInformationsDocument(BuInformationsDocument _id) {
    if (fCalques_ != null) fCalques_.setInformationsDocument(_id);
    if (fMailleur_ != null) fMailleur_.setInformationsDocument(_id);
    if (fGraphe_ != null) fGraphe_.setInformationsDocument(_id);
    if (fVue3D_ != null) fVue3D_.setInformationsDocument(_id);
    if (fTableau_ != null) fTableau_.setInformationsDocument(_id);
    if (fRapport_ != null) fRapport_.setInformationsDocument(_id);
  }

  public void oprServeurCopie() {
    System.err.println("Lancement du serveur de copie d'ecran");
    new ServeurCopieEcran(getMainPanel(), "ScreenSpy");
  }

  public void oprCalculer() {
    if (!isConnected()) {
      new BuDialogError(getApp(), isVag_, "vous n'etes pas connect� � un serveur VAG ! ").activate();
      return;
    }
    setEnabledForAction("CALCULER", false);
    setEnabledForAction("MAILLEUR", false);
    BuMainPanel mp = getMainPanel();
    System.err.println("Transmission des parametres...");
    mp.setMessage("Transmission des parametres...");
    mp.setProgression(0);
    vagParams.parametres01((SParametres01) projet_.getParam(VagResource.VAG01));
    vagParams.parametres02((SParametres02) projet_.getParam(VagResource.VAG02));
    vagParams.parametres03((SParametres03) projet_.getParam(VagResource.VAG03));
    vagParams.parametres04((SParametres04) projet_.getParam(VagResource.VAG04));
    System.err.println("Execution du calcul...");
    mp.setMessage("Execution du calcul...");
    mp.setProgression(20);
    try {
      SERVEUR_VAG.calcul(CONNEXION_VAG);
      vagResults = IResultatsVagHelper.narrow(SERVEUR_VAG.resultats(CONNEXION_VAG));
      receptionResultats(true);
    } catch (Throwable u) {
      new BuDialogError(getApp(), isVag_, u.toString()).activate();
      setEnabledForAction("CALCULER", true);
      setEnabledForAction("MAILLEUR", true);
      u.printStackTrace();
      SResultatsOUT out = vagResults.resultatsOUT();
      if (out != null) afficheResultatOUT(out);
      return;
    }
    if (fMailleur_ != null) fMailleur_.setLectureSeule();
    setEnabledForAction("CALCULER", true);
    setEnabledForAction("PASIMPRESSION", true);
  } 

  public void oprReceptionResultats() {
    try {
      receptionResultats(false);
    } catch (Exception e) {
      new BuDialogError(getApp(), isVag_, e.toString()).activate();
      e.printStackTrace();
      SResultatsOUT out = vagResults.resultatsOUT();
      if (out != null) afficheResultatOUT(out);
    }
  }

  // Methodes privees
  protected void creer() {
    if (!projet_.estVierge()) fermer();
    projet_.creer();
    if (!projet_.estConfigure()) projet_.fermer(); // si creation de projet annulee
    else { // nouveau projet cree
      if (fp_ != null) closeFilleParams();
      creeParametres();
      setEnabledForAction("PARAMETRE", true);
      setEnabledForAction("MAILLEUR", true);
      setEnabledForAction("RAPPORT", true);
      setEnabledForAction("IMPORTVAG", true);
      setEnabledForAction("EXPORTER", true);
      setEnabledForAction("FERMER", true);
      setEnabledForAction("ENREGISTRER", true);
      setEnabledForAction("ENREGISTRERSOUS", true);
      setEnabledForAction("PROPRIETE", true);
      setEnabledForAction("CALCULER", true);
      setEnabledForAction("PASIMPRESSION", false);
      setEnabledForAction("ORTHOGONALE", false);
      setEnabledForAction("VUE3D", false);
      setEnabledForAction("EXPORTRES", false);
      setEnabledForAction("INFOCALCUL", false);
      setTitle(projet_.getFichier());
      majInformationsDocument(getInformationsDocument());
    }
  }

  protected void ouvrir(String arg) {
    if (!projet_.estVierge()) fermer();
    projet_.ouvrir(arg);
    if (!projet_.estConfigure()) {
      projet_.fermer(); // si ouverture de projet echouee
      setEnabledForAction("PARAMETRE", false);
      setEnabledForAction("MAILLEUR", false);
      setEnabledForAction("RAPPORT", false);
      setEnabledForAction("IMPORTVAG", false);
      setEnabledForAction("EXPORTER", false);
      setEnabledForAction("FERMER", false);
      setEnabledForAction("ENREGISTRER", false);
      setEnabledForAction("ENREGISTRERSOUS", false);
      setEnabledForAction("PROPRIETE", false);
      setEnabledForAction("CALCULER", false);
      setEnabledForAction("PASIMPRESSION", false);
      setEnabledForAction("ORTHOGONALE", false);
      setEnabledForAction("VUE3D", false);
      setEnabledForAction("EXPORTRES", false);
      setEnabledForAction("INFOCALCUL", false);
    } else {
      if (fp_ != null) closeFilleParams();
      // HACK pour recuperer les projets ancien modele
      if (!projet_.containsParam(VagResource.VAG02)) projet_.addParam(VagResource.VAG02, new SParametres02());
      if (!projet_.containsParam(VagResource.VAG03)) projet_.addParam(VagResource.VAG03, new SParametres03());
      new BuDialogMessage(getApp(), isVag_, "Param�tres charg�s").activate();
      setEnabledForAction("PARAMETRE", true);
      setEnabledForAction("MAILLEUR", true);
      setEnabledForAction("RAPPORT", true);
      setEnabledForAction("IMPORTVAG", true);
      setEnabledForAction("EXPORTER", true);
      setEnabledForAction("FERMER", true);
      setEnabledForAction("ENREGISTRER", false);
      setEnabledForAction("ENREGISTRERSOUS", true);
      setEnabledForAction("PROPRIETE", true);
      setEnabledForAction("CALCULER", true);
      setEnabledForAction("ORTHOGONALE", false);
      setEnabledForAction("VUE3D", false);
      setEnabledForAction("EXPORTRES", false);
      setEnabledForAction("INFOCALCUL", false);
      setTitle(projet_.getFichier());
      majInformationsDocument(getInformationsDocument());
      if (arg == null) {
        String r = projet_.getFichier();
        getMainMenuBar().addRecentFile(r, "vag");
        VagPreferences.VAG.writeIniFile();
      }
    }
  }

  protected void enregistrer() {
    if (fp_ != null) {
      if (!fp_.isClosed()) fp_.valider();
    }
    projet_.enregistre();
    new BuDialogMessage(getApp(), isVag_, "Param�tres enregistr�s").activate();
    boolean found = false;
    String r = projet_.getFichier();
    for (int i = 1; i < RECENT_COUNT; i++) {
      if (r.equals(VagPreferences.VAG.getStringProperty("file.recent." + i + ".path"))) found = true;
    }
    if (!found) {
      getMainMenuBar().addRecentFile(r, "vag");
      VagPreferences.VAG.writeIniFile();
    }
    setEnabledForAction("REOUVRIR", true);
    FudaaParamChangeLog.CHANGE_LOG.setDirty(false);
    setEnabledForAction("ENREGISTRER", false);
  }

  protected void enregistrerSous() {
    if (fp_ != null) {
      if (!fp_.isClosed()) fp_.valider();
    }
    projet_.enregistreSous();
    new BuDialogMessage(getApp(), isVag_, "Param�tres enregistr�s").activate();
    boolean found = false;
    String r = projet_.getFichier();
    for (int i = 1; i < RECENT_COUNT; i++) {
      if (r.equals(VagPreferences.VAG.getStringProperty("file.recent." + i + ".path"))) found = true;
    }
    if (!found) {
      getMainMenuBar().addRecentFile(r, "vag");
      VagPreferences.VAG.writeIniFile();
    }
    FudaaParamChangeLog.CHANGE_LOG.setDirty(false);
    setEnabledForAction("ENREGISTRER", false);
    setTitle(projet_.getFichier());
  }

  protected void fermer() {
    if (projet_.estConfigure() && FudaaParamChangeLog.CHANGE_LOG.isDirty()) {
      int res = new BuDialogConfirmation(getApp(), isVag_, "Votre projet va etre ferm�.\n"
          + "Voulez-vous l'enregistrer avant?\n").activate();
      if (res == JOptionPane.YES_OPTION) enregistrer();
    }
    if ((previsuFille_ != null) && (previsuFille_.isVisible())) previsuFille_.setVisible(false);
    if (fProprietes_.isVisible()) fProprietes_.setVisible(false);
    BuDesktop dk = getMainPanel().getDesktop();
    if (fp_ != null) {
      closeFilleParams();
      dk.removeInternalFrame(fp_);
      fp_.removeInternalFrameListener(this);
      fp_.delete();
      fp_ = null;
    }
    if (fCalques_ != null) {
      arbre_.removeTreeSelectionListener(fCalques_);
      try {
        fCalques_.setClosed(true);
        fCalques_.setSelected(false);
      } catch (PropertyVetoException e) {}
      dk.removeInternalFrame(fCalques_);
      gc_.detruire(gc_);
      gc_ = new BGroupeCalque();
      gc_.setTitle("Calques Vag");
      arbre_.setModel(null);
      arbre_.refresh();
      fCalques_.removeInternalFrameListener(this);
      fCalques_ = null;
    }
    if (fVue3D_ != null) {
      dk.removeInternalFrame(fVue3D_);
      fVue3D_.cache();
      fVue3D_.removeInternalFrameListener(this);
      fVue3D_ = null;
    }
    if (fMailleur_ != null) {
      arbre_.removeTreeSelectionListener(fMailleur_);
      try {
        fMailleur_.setClosed(true);
        fMailleur_.setSelected(false);
      } catch (PropertyVetoException e) {}
      dk.removeInternalFrame(fMailleur_);
      gc_.detruire(gc_);
      gc_ = new BGroupeCalque();
      gc_.setTitle("Calques Vag");
      arbre_.setModel(null);
      arbre_.refresh();
      fMailleur_.removeInternalFrameListener(this);
      fMailleur_.delete();
      fMailleur_ = null;
      mailleurActive_ = false;
    }
    if (fRapport_ != null) {
      try {
        fRapport_.setClosed(true);
        fRapport_.setSelected(false);
      } catch (PropertyVetoException e) {}
      dk.removeInternalFrame(fRapport_);
      fRapport_.removeInternalFrameListener(this);
      fRapport_ = null;
    }
    if (fGraphe_ != null) {
      fGraphe_.setGraphe(new BGraphe());
      try {
        fGraphe_.setClosed(true);
        fGraphe_.setSelected(false);
      } catch (PropertyVetoException e) {}
      dk.removeInternalFrame(fGraphe_);
    }
    if (fTableau_ != null) {
      fTableau_.setTable(new VagTableauResultats());
      fTableau_.setSize(new Dimension(640, 480));
      fTableau_.setTitle("Tableau");
      try {
        fTableau_.setClosed(true);
        fTableau_.setSelected(false);
      } catch (PropertyVetoException e) {}
      dk.removeInternalFrame(fTableau_);
    }
    projet_.fermer();
    FudaaParamChangeLog.CHANGE_LOG.setDirty(false);
    dk.repaint();
    setEnabledForAction("PARAMETRE", false);
    setEnabledForAction("MAILLEUR", false);
    setEnabledForAction("RAPPORT", false);
    setEnabledForAction("IMPORTVAG", false);
    setEnabledForAction("EXPORTER", false);
    setEnabledForAction("FERMER", false);
    setEnabledForAction("ENREGISTRER", false);
    setEnabledForAction("ENREGISTRERSOUS", false);
    setEnabledForAction("PROPRIETE", false);
    setEnabledForAction("CALCULER", false);
    setEnabledForAction("PASIMPRESSION", false);
    setEnabledForAction("ORTHOGONALE", false);
    setEnabledForAction("VUE3D", false);
    setEnabledForAction("EXPORTRES", false);
    setEnabledForAction("INFOCALCUL", false);
    setTitle("Vag");
  }

  protected void importerProjet() {
    if (!projet_.estConfigure()) {
      projet_.fermer(); // si ouverture de projet echouee
      setEnabledForAction("PARAMETRE", false);
      setEnabledForAction("MAILLEUR", false);
      setEnabledForAction("RAPPORT", false);
      setEnabledForAction("IMPORTVAG", false);
      setEnabledForAction("EXPORTER", false);
      setEnabledForAction("FERMER", false);
      setEnabledForAction("ENREGISTRER", false);
      setEnabledForAction("ENREGISTRERSOUS", false);
      setEnabledForAction("CALCULER", false);
      setEnabledForAction("PASIMPRESSION", false);
      setEnabledForAction("ORTHOGONALE", false);
      setEnabledForAction("VUE3D", false);
      setEnabledForAction("EXPORTRES", false);
      setEnabledForAction("INFOCALCUL", false);
    } else {
      if (fp_ != null) closeFilleParams();
      new BuDialogMessage(getApp(), isVag_, "Param�tres charg�s").activate();
      setEnabledForAction("PARAMETRE", true);
      setEnabledForAction("MAILLEUR", true);
      setEnabledForAction("RAPPORT", true);
      setEnabledForAction("IMPORTVAG", true);
      setEnabledForAction("EXPORTER", true);
      setEnabledForAction("FERMER", true);
      setEnabledForAction("ENREGISTRER", true);
      setEnabledForAction("ENREGISTRERSOUS", true);
      setEnabledForAction("CALCULER", true);
      setEnabledForAction("ORTHOGONALE", false);
      setEnabledForAction("VUE3D", false);
      setEnabledForAction("EXPORTRES", false);
      setEnabledForAction("INFOCALCUL", false);
      setTitle(projet_.getFichier());
      majInformationsDocument(getInformationsDocument());
    }
  }

  protected void importer(String _vagId) {
    VagImport vi = new VagImport(getApp());
    FudaaParamEvent evt = null;
    if (_vagId != null) vi.onlyEnable(_vagId);
    vi.show();
    String tmp;
    tmp = vi.getImport(VagResource.VAG01);
    if ((tmp != null) && (!"".equals(tmp.trim()))) {
      projet_.addImport(VagResource.VAG01, tmp, null);
      evt = new FudaaParamEvent(this, 0, VagResource.VAG01, null, "import " + VagResource.VAG01);
      FudaaParamEventProxy.FUDAA_PARAM.fireParamStructCreated(evt);
    }
    tmp = vi.getImport(VagResource.VAG02);
    if ((tmp != null) && (!"".equals(tmp.trim()))) {
      projet_.addImport(VagResource.VAG02, tmp, null);
      evt = new FudaaParamEvent(this, 0, VagResource.VAG02, null, "import " + VagResource.VAG02);
      FudaaParamEventProxy.FUDAA_PARAM.fireParamStructCreated(evt);
    }
    tmp = vi.getImport(VagResource.VAG03);
    if ((tmp != null) && (!"".equals(tmp.trim()))) {
      projet_.addImport(VagResource.VAG03, tmp, null);
      evt = new FudaaParamEvent(this, 0, VagResource.VAG03, null, "import " + VagResource.VAG03);
      FudaaParamEventProxy.FUDAA_PARAM.fireParamStructCreated(evt);
    }
    tmp = vi.getImport(VagResource.VAG04);
    if ((tmp != null) && (!"".equals(tmp.trim()))) {
      projet_.addImport(VagResource.VAG04, tmp, null);
      evt = new FudaaParamEvent(this, 0, VagResource.VAG04, null, "import " + VagResource.VAG04);
      FudaaParamEventProxy.FUDAA_PARAM.fireParamStructCreated(evt);
    }
    setEnabledForAction("CALCULER", true);
  }

  protected void closeFilleParams() {
    try {
      fp_.setClosed(true);
      fp_.setSelected(false);
    } catch (PropertyVetoException e) {}
  }

  protected void parametre() {
    if (fp_ == null) {
      fp_ = new VagFilleParametres(getApp(), projet_);
      installContextHelp(fp_.getRootPane(), "vag/p-parametres.html");
      // if( mailleurActive_ ) fp_.grilleDisabled();
      addInternalFrame(fp_);
    } else {
      if (fp_.isClosed()) addInternalFrame(fp_);
      else
        activateInternalFrame(fp_);
    }
  }

  protected void mailleur() {
    if (fMailleur_ == null) {
      fMailleur_ = new VagFilleMailleur(arbre_, projet_, new VagMaillage(this, SERVEUR_DUNES));
      BVueCalque vc = fMailleur_.getVueCalque();
      vc.setBackground(Color.white);
      vc.setTaskView(getMainPanel().getTaskView());
      installContextHelp(fMailleur_.getRootPane(), "vag/p-mailleur.html");
      fMailleur_.setClosable(true);
      fMailleur_.setSize(new Dimension(450, 400));
      addInternalFrame(fMailleur_);
      fMailleur_.restaurer();
    } else {
      fMailleur_.repaint(0);
      if (fMailleur_.isClosed()) addInternalFrame(fMailleur_);
      else
        activateInternalFrame(fMailleur_);
      fMailleur_.restaurer();
    }
  }

  protected void rapport() {
    if (fRapport_ == null) {
      fRapport_ = new FudaaFilleRapport(getApp());
      fRapport_.setClosable(true);
      fRapport_.insereEnteteEtude(getInformationsDocument());
      addInternalFrame(fRapport_);
    } else {
      if (fRapport_.isClosed()) addInternalFrame(fRapport_);
      else
        activateInternalFrame(fRapport_);
    }
  }

  protected void pas_impression() {
    // if( vagResults==null )
    // vagResults=IResultatsVagHelper.narrow(SERVEUR_VAG.resultats());
    try {
      SParametres01 par = (SParametres01) projet_.getParam(VagResource.VAG01);
      BuTextField tf = BuTextField.createIntegerField();
      tf.setValue(new Integer(par.codeImp));
      Object resp = null;
      int ret = JOptionPane.OK_OPTION;
      while ((ret == JOptionPane.OK_OPTION) && (resp == null)) {
        ret = new BuDialogMessage(getApp(), getInformationsSoftware(), tf).activate();
        resp = tf.getValue();
      }
      if (ret == JOptionPane.CANCEL_OPTION) return;
      if (par.codeImp == ((Integer) resp).intValue()) return;
      FudaaParamEventProxy.FUDAA_PARAM.fireParamStructModified(new FudaaParamEvent(this, 0, VagResource.VAG01, par,
          VagResource.VAG01 + ":" + "codeImp"));
      par.codeImp = ((Integer) resp).intValue();
      vagResults.setPasImpression(((Integer) resp).intValue());
      projet_.addResult(VagResource.VAG05, vagResults.resultats05());
      projet_.addResult(VagResource.VAGIO, vagResults.resultatsInterOrth());
    } catch (NullPointerException e) {
      new BuDialogError(getApp(), isVag_, "pas de r�sultat sur le serveur => \nvous devez lancer un calcul ")
          .activate();
      return;
    }
    if (projet_.getResult(VagResource.VAG05) != null) {
      new BuTaskOperation(this, "Calcul", "oprReceptionResultats").start();
    } else
      new BuDialogError(getApp(), isVag_, "pas de r�sultat sur le serveur => \nvous devez lancer un calcul ")
          .activate();
  }

  protected void calculer() {
    if (fp_ != null) {
      if (!fp_.isClosed()) fp_.valider();
    }
    if (valideContraintesCalcul()) new BuTaskOperation(this, "Calcul", "oprCalculer").start();
  }

  protected void orthogonale() {
    if (fCalques_ != null) {
      if (fCalques_.isClosed()) addInternalFrame(fCalques_);
      else
        activateInternalFrame(fCalques_);
      fCalques_.restaurer();
    }
  }

  protected void vue3d() {
    if (fVue3D_ == null) {
      System.out.println("Creation vue3D");
      BGroupeVolume gv = construitVolumes();
      BGroupeLumiere gl = new BGroupeLumiere();
      gl.setName("Lumi�res");
      BLumiereDirectionnelle l1 = new BLumiereDirectionnelle(new Vector3f(1, 0, 1), Color.white);
      BLumiereDirectionnelle l2 = new BLumiereDirectionnelle(new Vector3f(-1, 0, -1), Color.white);
      l1.setName("Droite");
      l2.setName("Gauche");
      gl.add(l1);
      gl.add(l2);
      l1.setRapide(false);
      l1.setVisible(true);
      l2.setRapide(false);
      l2.setVisible(true);
      BGroupeStandard gs = new BGroupeStandard();
      gs.setName("Univers");
      gs.add(gv);
      gs.add(gl);
      fVue3D_ = new EbliFilleVue3D(idVag_, getApp(), true);
      fVue3D_.setRoot(gs);
      fVue3D_.getUnivers().init();
      fVue3D_.setInformationsDocument(getInformationsDocument());
      fVue3D_.setSize(new Dimension(600, 400));
      addInternalFrame(fVue3D_);
      try {
        fVue3D_.setIcon(true);
      } catch (PropertyVetoException e) {}
      activateInternalFrame(fVue3D_);
      fVue3D_.getUnivers().repaintCanvas();
    } else {
      if (fVue3D_.isClosed()) {
        addInternalFrame(fVue3D_);
        fVue3D_.montre();
      } else {
        activateInternalFrame(fVue3D_);
        fVue3D_.montre();
      }
    }
  }

  protected void exportResultats(String key) {
    JFileChooser chooser = new JFileChooser();
    chooser.setFileFilter(new javax.swing.filechooser.FileFilter() {

      public boolean accept(java.io.File f) {
        return true;
      }

      public String getDescription() {
        return "*.*";
      }
    });
    chooser.setCurrentDirectory(new java.io.File(System.getProperty("user.dir")));
    int returnVal = chooser.showSaveDialog(VagImplementation.FRAME);
    String filename = null;
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      filename = chooser.getSelectedFile().getPath();
      if (filename == null) return;
      if (new File(filename).exists()) {
        if (new BuDialogConfirmation(getApp(), isVag_, "Le fichier :\n" + filename
            + "\nexiste d�j�. Voulez-vous l'�craser?\n").activate() == JOptionPane.NO_OPTION) return;
      }
      try {
        if (VagResource.VAG05.equals(key)) {
          if (projet_.containsResult(VagResource.VAG05)) {
            VagExport.exportResultats05((SResultats05) projet_.getResult(VagResource.VAG05),
                projet_.getInformations().titre, filename);
          } else
            new BuDialogError(getApp(), getInformationsSoftware(),
                "Il n'y a pas de r�sultats :\nvous n'avez pas lance de calcul").activate();
        } else if (VagResource.VAGIO.equals(key)) {
          if (projet_.containsResult(VagResource.VAGIO)) {
            VagExport.exportResultatsIO((SResultatsInterOrth) projet_.getResult(VagResource.VAGIO), projet_
                .getInformations().titre, filename);
          } else
            new BuDialogError(getApp(), getInformationsSoftware(),
                "Il n'y a pas de r�sultats :\nvous n'avez pas lance de calcul").activate();
        }
      } catch (java.io.IOException ex) {
        new BuDialogError(getApp(), getInformationsSoftware(), ex.toString()).activate();
      }
    }
  }

  protected void receptionResultats(boolean calcul) {
    BuMainPanel mp = getMainPanel();
    System.err.println("Reception des resultats...");
    mp.setMessage("Reception des resultats...");
    mp.setProgression(90);
    projet_.addResult(VagResource.VAG05, vagResults.resultats05());
    projet_.addResult(VagResource.VAGIO, vagResults.resultatsInterOrth());
    projet_.addResult(VagResource.VAGOUT, vagResults.resultatsOUT());
    System.err.println("Operation terminee.");
    mp.setMessage("Operation terminee.");
    mp.setProgression(100);
    if (calcul) new BuDialogMessage(getApp(), isVag_,
        "Calcul termin�\nCliquez sur \"Continuer\" pour voir les r�sultats.").activate();
    else
      new BuDialogMessage(getApp(), isVag_, "R�sultats charg�s").activate();
    mp.setMessage("Affichage...");
    mp.setProgression(10);
    if (fGraphe_.isClosed()) {
      fGraphe_.setInformationsDocument(getInformationsDocument());
      addInternalFrame(fGraphe_);
    }
    mp.setProgression(20);
    if (fTableau_.isClosed()) {
      addInternalFrame(fTableau_);
      fTableau_.setInformationsDocument(getInformationsDocument());
    }
    mp.setProgression(30);
    if (fCalques_ == null) {
      gc_ = construitCalques();
      mp.setProgression(40);
      gc_.setTitle("Calques Vag");
      gc_.setBackground(Color.white);
      // arbre_.setCalque(gc_);
      mp.setProgression(50);
      fCalques_ = new VagFilleCalques(gc_, arbre_, getApp(), getInformationsDocument());
      BVueCalque vc = fCalques_.getVueCalque();
      vc.setBackground(Color.white);
      // vc.setCalque(gc_);
      vc.setTaskView(mp.getTaskView());
      if (vc.getTaskView() == null) System.out.println("task view nullle !!!!!!!!!!!!!");
      installContextHelp(fCalques_.getRootPane(), "vag/p-calques.html");
      fCalques_.setSize(new Dimension(450, 300));
      addInternalFrame(fCalques_);
      fCalques_.restaurer();
      mp.setProgression(100);
    } else {
      updateCalques(gc_);
      fVue3D_ = null;
      mp.setProgression(40);
      arbre_.refresh();
      fCalques_.repaint(0);
      mp.setProgression(80);
      if (fCalques_.isClosed()) addInternalFrame(fCalques_);
      else
        activateInternalFrame(fCalques_);
      fCalques_.restaurer();
      mp.setProgression(100);
    }
    setEnabledForAction("ORTHOGONALE", true);
    setEnabledForAction("EXPORTRES", true);
    setEnabledForAction("INFOCALCUL", true);
    if (j3d) setEnabledForAction("VUE3D", true);
    try {
      Thread.sleep(2000);
    } catch (InterruptedException e) {}
    mp.setMessage(" ");
    mp.setProgression(0);
  }

  protected void trieCotes(Rectangle2D.Double rect) {
    SParametres02 param2 = (SParametres02) projet_.getParam(VagResource.VAG02);
    if (param2.xy == null) return;
    Vector v = new Vector();
    boolean nouvellePoly = true;
    for (int i = 0; i < param2.xy.length; i++) {
      if ((param2.xy[i].x >= rect.x) && (param2.xy[i].x <= (rect.x + rect.width)) && (param2.xy[i].y >= rect.y)
          && (param2.xy[i].y <= (rect.y + rect.height))) {
        param2.xy[i].x -= rect.x;
        param2.xy[i].y -= rect.y;
        v.add(param2.xy[i]);
        if (nouvellePoly) {
          param2.xy[i].trait = 0;
          nouvellePoly = false;
        }
      } else
        nouvellePoly = true;
    }
    param2.xy = new SParametresLigne02[v.size()];
    for (int i = 0; i < v.size(); i++) {
      param2.xy[i] = (SParametresLigne02) v.get(i);
    }
    param2.nbLignes = param2.xy.length;
  }

  protected void afficheResultatOUT(SResultatsOUT out) {
    System.err.println("affiche OUT");
    JTextArea taOUT = new JTextArea();
    taOUT.setEditable(false);
    taOUT.setText(out.sortieEcran);
    new BuDialogMessage(getApp(), isVag_, taOUT).activate();
  }

  protected BGroupeCalque construitCalques() {
    try {
      BCalqueCartouche cart = new BCalqueCartouche();
      cart.setName("cqCARTOUCHE");
      cart.setTitle("Cartouche");
      cart.setInformations(idVag_);
      cart.setForeground(Color.black);
      cart.setBackground(new Color(255, 255, 224));
      cart.setFont(new Font("SansSerif", Font.PLAIN, 10));
      cart.setVisible(false);
      BCalqueDomaine dom = new BCalqueDomaine();
      dom.setName("cqDOMAINE");
      dom.setTitle("Domaine");
      dom.setFont(new Font("SansSerif", Font.PLAIN, 8));
      dom.setAttenue(false);
      dom.setVisible(false);
      BCalqueGrille cg = new BCalqueGrille();
      cg.setForeground(Color.lightGray);
      cg.setTitle("Grille");
      cg.setName("cqGRILLE");
      BCalqueLegende leg = new BCalqueLegende();
      leg.setTitle("L�gende");
      leg.setName("cqLEGENDE");
      leg.setForeground(java.awt.Color.blue);
      VagCalqueCotes c = new VagCalqueCotes();
      c.setTitle("Cotes");
      c.setName("cqCOTES");
      c.setParametres((SParametres02) projet_.getParam(VagResource.VAG02));
      VagCalqueOrthogonales s = new VagCalqueOrthogonales();
      s.setTitle("Orthogonales");
      s.setName("cqORTHOGONALES");
      s.setResultats05((SResultats05) projet_.getResult(VagResource.VAG05));
      s.setResultatsInterOrth((SResultatsInterOrth) projet_.getResult(VagResource.VAGIO));
      s.setTypeTrait(TraceLigne.POINT_MARQUE);
      s.setForeground(Color.red);
      VagCalqueOrthogonalesInteraction si = new VagCalqueOrthogonalesInteraction(s, fGraphe_.getGraphe(),
          (VagTableauResultats) fTableau_.getTable());
      si.setTitle("O-Interaction");
      si.setName("cqORTHOGONALESI");
      si.setGele(false);
      si.setFlecheNord(((SParametres01) projet_.getParam(VagResource.VAG01)).flecheNordDir);
      VagCalqueCretes s2 = new VagCalqueCretes();
      s2.setTitle("Cretes");
      s2.setName("cqCRETES");
      s2.setResultats05((SResultats05) projet_.getResult(VagResource.VAG05));
      s2.setTypeTrait(TraceLigne.LISSE);
      s2.setForeground(Color.blue);
      VagCalqueHauteurHoule hh = new VagCalqueHauteurHoule();
      hh.setTitle("Hauteur de Houle");
      // hh.setName("cqHAUTEURHOULE");
      hh.setIsosurfaces(false);
      hh.setSurface(true);
      hh.setContour(false);
      hh.setIsolignes(true);
      BParametresGouraud pg = new BParametresGouraud();
      pg.setNiveau(64);
      pg.setTaille(64);
      hh.setParametresGouraud(pg);
      hh.setResultatsInterOrth((SResultatsInterOrth) projet_.getResult(VagResource.VAGIO));
      hh.setLegende(leg);
      leg.setBackground(hh, new Color(255, 255, 230));
      hh.setVisible(false);
      hauteur_houle_maillage = hh.getMaillage();
      hauteur_houle_valeurs = hh.getValeurs();
      VagCalqueBathymetrie cb = new VagCalqueBathymetrie();
      cb.setTitle("Bathymetrie");
      cb.setName("cqBATHYMETRIE");
      cb.setIsosurfaces(false);
      cb.setSurface(true);
      cb.setContour(false);
      cb.setIsolignes(true);
      pg = new BParametresGouraud();
      pg.setNiveau(8);
      pg.setTaille(16);
      cb.setParametresGouraud(pg);
      cb.setParametres01((SParametres01) projet_.getParam(VagResource.VAG01));
      cb.setParametres03((SParametres03) projet_.getParam(VagResource.VAG03));
      // cb.setTypePoint(TracePoint.CARRE_PLEIN);
      // cb.setTaillePoint(2+(int)(((SParametres01)projet_.getParam(VagResource.VAG01)).rectLargeur/
      // ((((SParametres01)projet_.getParam(VagResource.VAG01)).nbMaillesX+1)*2)));
      cb.setLegende(leg);
      leg.setBackground(cb, new Color(255, 230, 250));
      cb.setVisible(false);
      BCalqueRosace r = new BCalqueRosace();
      r.setTitle("Fl�che Nord");
      r.setName("cqROSACE");
      r.setAngleNord(((SParametres01) projet_.getParam(VagResource.VAG01)).flecheNordDir * Math.PI / 180.);
      r.setBackground(new Color(224, 224, 255));
      BCalqueDessin d = new BCalqueDessin();
      d.setTitle("Dessin");
      d.setName("cqDESSIN");
      BCalqueDessinInteraction di = new BCalqueDessinInteraction(d);
      di.setTitle("D-Interaction");
      di.setName("cqDESSINI");
      di.setGele(true);
      BGroupeCalque gc = new BGroupeCalque();
      gc.add(leg);
      gc.add(cart);
      gc.add(d);
      d.add(di);
      gc.add(s);
      s.add(si);
      gc.add(s2);
      gc.add(hh);
      gc.add(c);
      gc.add(r);
      gc.add(cg);
      gc.add(cb);
      gc.add(dom);
      cg.setBoite(gc.getDomaine());
      return gc;
    } catch (Throwable t) {
      t.printStackTrace();
    }
    return null;
  }

  protected BGroupeVolume construitVolumes() {
    try {
      System.out.println("Creation de la Bathym�trie");
      BGrilleReguliere bathy = new BGrilleReguliere("Bathymetrie");
      SParametres01 param1 = (SParametres01) projet_.getParam(VagResource.VAG01);
      int alpha = param1.pasCalcul;
      SParametres03 param3 = (SParametres03) projet_.getParam(VagResource.VAG03);
      SParametres04 param4 = (SParametres04) projet_.getParam(VagResource.VAG04);
      double T = param4.periode;
      param4 = null;
      bathy.setBoite(new GrPoint(0, 0, 0.), new GrPoint(param1.rectLargeur, param1.rectHauteur, 1.));
      bathy.setInversionZ(true);
      bathy.setGeometrie(param1.nbMaillesX + 1, param1.nbMaillesY + 1, param3.vz);
      bathy.setCouleur(new Color(255, 200, 100));
      bathy.setEclairage(true);
      bathy.setTransparence(0);
      GrBoite boite = bathy.getBoite();
      double x = (boite.e_.x_ + boite.o_.x_) / 2;
      double y = (boite.e_.y_ + boite.o_.y_) / 2;
      double z2 = boite.e_.z_ - boite.o_.z_;
      bathy.setNouvelleEchelleZ((float) ((x + y) / (z2 * 2 * 2)));
      System.err.println("echelle: " + ((x + y) / (z2 * 2 * 2)));
      System.out.println("Creation de la Mer");
      BGrilleReguliere mer = new BGrilleReguliere("Mer");
      mer.setBoite(new GrPoint(0, 0, 0.), new GrPoint(param1.rectLargeur, param1.rectHauteur, 1.));
      double[] zero = new double[4];
      for (int i = 0; i < 4; i++) {
        zero[i] = 0;
      }
      mer.setGeometrie(2, 2, zero);
      mer.setCouleur(Color.blue);
      mer.setTransparence(.5f);
      param3 = null;
      System.out.println("Creation de la Houle");
      int nb_noeuds = hauteur_houle_maillage.noeuds_.nombre();
      // Construction des noeuds (format J3D)
      Point3d[] noeuds = new Point3d[nb_noeuds];
      for (int i = 0; i < nb_noeuds; i++) {
        noeuds[i] = new Point3d(hauteur_houle_maillage.noeuds_.renvoie(i).x_,
            hauteur_houle_maillage.noeuds_.renvoie(i).y_, hauteur_houle_valeurs[i]);
      }
      int nb_connect = hauteur_houle_maillage.connectivites_.size();
      // Construction des triangles
      int[] triangles = new int[nb_connect * 3 * 2];
      for (int i = 0; i < nb_connect; i++) {
        // premier triangle: points 0,1,2
        triangles[6 * i + 0] = ((int[]) hauteur_houle_maillage.connectivites_.get(i))[0];
        triangles[6 * i + 1] = ((int[]) hauteur_houle_maillage.connectivites_.get(i))[1];
        triangles[6 * i + 2] = ((int[]) hauteur_houle_maillage.connectivites_.get(i))[2];
        // premier triangle: points 1,2,3
        triangles[6 * i + 3] = ((int[]) hauteur_houle_maillage.connectivites_.get(i))[2];
        triangles[6 * i + 4] = ((int[]) hauteur_houle_maillage.connectivites_.get(i))[3];
        triangles[6 * i + 5] = ((int[]) hauteur_houle_maillage.connectivites_.get(i))[0];
      }
      VagGrilleIrreguliere houle = new VagGrilleIrreguliere("Houle");
      houle.setBoite(hauteur_houle_maillage.boite());
      hauteur_houle_maillage = null;
      hauteur_houle_valeurs = null;
      houle.setGeometrie(noeuds.length, noeuds, triangles.length, triangles);
      // houle.setTransparence(.5);
      // BPaletteCouleurSimple pc = new BPaletteCouleurSimple();
      houle.setCouleur(Color.BLUE);
      houle.setSourceDonnees(new Vag05DataSource((SResultatsInterOrth) projet_.getResult(VagResource.VAGIO), alpha, T));
      BChampVecteurs chp = new BChampVecteurs("Axes");
      Point3d[] orig = new Point3d[] { new Point3d(), new Point3d(), new Point3d() };
      Vector3d[] vect = new Vector3d[] { new Vector3d(100, 0, 0), new Vector3d(0, 100, 0), new Vector3d(0, 0, 100) };
      chp.setGeometrie(orig, vect);
      BGroupeVolume imp = new BGroupeVolume();
      imp.setName("Import");
      BGroupeVolume gv = new BGroupeVolume();
      gv.add(mer);
      gv.add(houle);
      gv.add(bathy);
      gv.add(chp);
      gv.add(imp);
      imp.setVisible(true);
      imp.setRapide(false);
      /*
       * DEMO BObjetVRML obj=new BObjetVRML("c:\\demo\\vrml\\arbre.wrl"); obj.setRotationX(90f); obj.setEchelleX(.02f);
       * obj.setEchelleY(.02f); obj.setEchelleZ(.02f); obj.setTranslationX(400f); obj.setTranslationY(400f);
       * obj.setTranslationZ(30f); imp.setNouvelObjet(obj); BObjetVRML obj2=new BObjetVRML("c:\\demo\\vrml\\femme.wrl");
       * obj2.setRotationX(180f); obj2.setRotationZ(-90f); obj2.setEchelleX(.001f); obj2.setEchelleY(.001f);
       * obj2.setEchelleZ(.001f); obj2.setTranslationX(440f); obj2.setTranslationY(450f); obj2.setTranslationZ(7f);
       * imp.setNouvelObjet(obj2);
       */
      mer.setRapide(false);
      mer.setVisible(true);
      houle.setRapide(false);
      houle.setVisible(true);
      bathy.setRapide(false);
      bathy.setVisible(true);
      chp.setVisible(true);
      chp.setRapide(false);
      gv.setName("Volumes");
      return gv;
    } catch (Throwable t) {
      t.printStackTrace();
    }
    return null;
  }

  protected void updateCalques(BGroupeCalque gc) {
    BCalque[] calques = gc.getTousCalques();
    BCalqueGrille cg = null;
    for (int i = 0; i < calques.length; i++) {
      if (calques[i] instanceof BCalqueCartouche) {
        ((BCalqueCartouche) calques[i]).setInformations(idVag_);
      } else if (calques[i] instanceof BCalqueDomaine) {} else if (calques[i] instanceof BCalqueGrille) {
        cg = (BCalqueGrille) calques[i];
      } else if (calques[i] instanceof VagCalqueCotes) {
        ((VagCalqueCotes) calques[i]).setParametres((SParametres02) projet_.getParam(VagResource.VAG02));
      } else if (calques[i] instanceof VagCalqueOrthogonalesInteraction) {
        ((VagCalqueOrthogonalesInteraction) calques[i]).setFlecheNord(((SParametres01) projet_
            .getParam(VagResource.VAG01)).flecheNordDir);
        // calques[i].getParent().remove(calques[i]);
      } else if (calques[i] instanceof VagCalqueOrthogonales) {
        ((VagCalqueOrthogonales) calques[i]).setResultats05((SResultats05) projet_.getResult(VagResource.VAG05));
        ((VagCalqueOrthogonales) calques[i]).setResultatsInterOrth((SResultatsInterOrth) projet_
            .getResult(VagResource.VAGIO));
        // VagCalqueOrthogonalesInteraction si=new
        // VagCalqueOrthogonalesInteraction((VagCalqueOrthogonales)calques[i],
        // fGraphe_.getGraphe());
        // si.setTitle("O-Interaction");
        // calques[i].add(si);
      } else if (calques[i] instanceof VagCalqueHauteurHoule) {
        ((VagCalqueHauteurHoule) calques[i]).setResultatsInterOrth((SResultatsInterOrth) projet_
            .getResult(VagResource.VAGIO));
      } else if (calques[i] instanceof VagCalqueCretes) {
        ((VagCalqueCretes) calques[i]).setResultats05((SResultats05) projet_.getResult(VagResource.VAG05));
      } else if (calques[i] instanceof VagCalqueBathymetrie) {
        ((VagCalqueBathymetrie) calques[i]).reinitialise();
        ((VagCalqueBathymetrie) calques[i]).setParametres01((SParametres01) projet_.getParam(VagResource.VAG01));
        ((VagCalqueBathymetrie) calques[i]).setParametres03((SParametres03) projet_.getParam(VagResource.VAG03));
      } else if (calques[i] instanceof BCalqueDessin) {} else if (calques[i] instanceof BCalqueDessinInteraction) {} else if (calques[i] instanceof BCalqueRosace) {
        ((BCalqueRosace) calques[i]).setAngleNord(((SParametres01) projet_.getParam(VagResource.VAG01)).flecheNordDir
            * Math.PI / 180.);
      }
    }
    if (cg != null) cg.setBoite(gc.getDomaine());
  }

  public void fullscreen() {
    if (getCurrentInternalFrame() == fVue3D_) fVue3D_.fullscreen();
    else
      super.fullscreen();
  }

  private void creeParametres() {
    SParametres01 p01 = new SParametres01();
    // p01.titreEtude=projet_.getTitre();
    p01.titreEtude = projet_.getInformations().titre;
    p01.codeInconnu = "";
    p01.pasCalcul = 1;
    p01.codeImp = 1;
    p01.flecheNordDir = 90.;
    SParametres04 p04 = new SParametres04();
    p04.angleHouleHoriz = 90;
    projet_.addParam(VagResource.VAG01, p01);
    projet_.addParam(VagResource.VAG02, new SParametres02());
    projet_.addParam(VagResource.VAG03, new SParametres03());
    projet_.addParam(VagResource.VAG04, p04);
  }

  private boolean valideContraintesCalcul() {
    // SParametres01 p01=(SParametres01)projet_.getParam(VagResource.VAG01);
    // SParametres02 p02=(SParametres02)projet_.getParam(VagResource.VAG02);
    // SParametres03 p03=(SParametres03)projet_.getParam(VagResource.VAG03);
    // SParametres04 p04=(SParametres04)projet_.getParam(VagResource.VAG04);
    if (!projet_.containsParam(VagResource.VAG03)) {
      new BuDialogError(getApp(), isVag_, "il n'y a pas de param�tre Vag 03 ! ").activate();
      return false;
    }
    return true;
  }

  public void exit() {
    fermer();
    closeConnexions();
    super.exit();
  }

  public void finalize() {
    closeConnexions();
  }

  public boolean confirmExit() {
    return true;
  }

  public boolean isCloseFrameMode() {
    return false;
  }

  protected void buildPreferences(List _frAddTab) {
    super.buildPreferences(_frAddTab);
    _frAddTab.add(new VolumePreferencesPanel(this));
  }

  /**
   * @see org.fudaa.fudaa.commun.dodico.FudaaImplementation#clearVariables()
   */
  protected void clearVariables() {
    CONNEXION_DUNES = null;
    CONNEXION_VAG = null;
    SERVEUR_DUNES = null;
    SERVEUR_VAG = null;
  }

  /**
   * @see org.fudaa.fudaa.commun.dodico.FudaaImplementation#getTacheConnexionMap()
   */
  protected FudaaDodicoTacheConnexion[] getTacheConnexionMap() {
    FudaaDodicoTacheConnexion c = new FudaaDodicoTacheConnexion(SERVEUR_VAG, CONNEXION_VAG);
    FudaaDodicoTacheConnexion c1 = new FudaaDodicoTacheConnexion(SERVEUR_DUNES, CONNEXION_DUNES);
    return new FudaaDodicoTacheConnexion[] { c, c1 };
  }

  /**
   * @see org.fudaa.fudaa.commun.dodico.FudaaImplementation#getTacheDelegateClass()
   */
  protected Class[] getTacheDelegateClass() {
    return new Class[] { DCalculDunes.class, DCalculVag.class };
  }

  /**
   * @see org.fudaa.fudaa.commun.dodico.FudaaImplementation#initConnexions(java.util.Map)
   */
  protected void initConnexions(Map _r) {
    FudaaDodicoTacheConnexion c = (FudaaDodicoTacheConnexion) _r.get(DCalculVag.class);
    CONNEXION_VAG = c.getConnexion();
    SERVEUR_VAG = ICalculVagHelper.narrow(c.getTache());
    vagParams = IParametresVagHelper.narrow(SERVEUR_VAG.parametres(CONNEXION_VAG));
    c = (FudaaDodicoTacheConnexion) _r.get(DCalculDunes.class);
    CONNEXION_DUNES = c.getConnexion();
    SERVEUR_DUNES = ICalculDunesHelper.narrow(c.getTache());
  }

  /**
   * @see org.fudaa.fudaa.commun.impl.FudaaCommonImplementation#cmdOuvrirFile(java.io.File)
   */
  public void cmdOuvrirFile(File _f) {
    ouvrir(_f.getAbsolutePath());
  }
}