/*
 * @creation     1998-08-25
 * @modification $Date: 2007-06-28 09:28:18 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.vag;
import java.awt.Dimension;

import org.fudaa.dodico.corba.vag.SParametres02;

import org.fudaa.ebli.calque.BCalquePolyligne;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolyligne;
/**
 * Un calque de cotes de Vag.
 *
 * @version      $Revision: 1.7 $ $Date: 2007-06-28 09:28:18 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class VagCalqueCotes extends BCalquePolyligne {
  // donnees membres publiques
  // donnees membres privees
  // Constructeur
  public VagCalqueCotes() {
    super();
    params_= null;
    //setTypeTrait(TraceLigne.TIRETE);
    setPreferredSize(new Dimension(640, 480));
  }
  /**********************************************/
  // PROPRIETES INTERNES
  /**********************************************/
  // Propriete resultats
  private SParametres02 params_;
  public SParametres02 getParametres() {
    return params_;
  }
  public void setParametres(SParametres02 _params) {
    if (_params == null)
      return;
    SParametres02 vp= params_;
    params_= _params;
    reinitialise();
    construitPolylignes();
    firePropertyChange("parametres", vp, params_);
  }
  // Methodes privees
  private void construitPolylignes() {
    if (params_.nbLignes == 0)
      return;
    GrPolyligne nouvLigne= new GrPolyligne();
    for (int i= 0; i < params_.xy.length; i++) {
      if ((params_.xy[i].trait == 0) && (i > 0)) {
        ajoute(nouvLigne);
        nouvLigne= new GrPolyligne();
      }
      nouvLigne.sommets_.ajoute(
        new GrPoint(params_.xy[i].x, params_.xy[i].y, 0.));
    }
    ajoute(nouvLigne);
  }
}
