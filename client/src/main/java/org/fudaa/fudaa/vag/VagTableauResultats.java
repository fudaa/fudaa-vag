/*
 * @file         VagTableauResultats.java
 * @creation     1999-03-01
 * @modification $Date: 2005-08-16 14:17:03 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.vag;
import java.text.NumberFormat;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import com.memoire.bu.BuTable;
import com.memoire.bu.BuTableCellRenderer;

import org.fudaa.dodico.corba.vag.SResultatUnitaire05;
import org.fudaa.dodico.corba.vag.SResultatUnitaireInterOrth;
/**
 * Tableau des resultats 05.
 *
 * @version      $Revision: 1.5 $ $Date: 2005-08-16 14:17:03 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class VagTableauResultats extends BuTable {
  public VagTableauResultats() {
    super();
    setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    //    setPreferredSize(new Dimension(640, 480));
  }
  private double nord_;
  public void setFlecheNord(double angle) {
    nord_= angle;
  }
  public void reinitialise() {
    reinitialise05();
  }
  private void reinitialise05() {
    if (resultats05_ == null)
      return;
    VagTableauResultatsModel05 model=
      new VagTableauResultatsModel05(resultats05_);
    model.setFlecheNord(nord_);
    setModel(model);
    BuTableCellRenderer tcr= new BuTableCellRenderer();
    NumberFormat nf= NumberFormat.getInstance();
    nf.setMaximumFractionDigits(2);
    tcr.setNumberFormat(nf);
    for (int i= 0; i < getModel().getColumnCount(); i++)
      getColumn(getColumnName(i)).setCellRenderer(tcr);
    getColumn(getColumnName(0)).setWidth(60);
    getColumn(getColumnName(1)).setWidth(60);
    getColumn(getColumnName(2)).setWidth(60);
    getColumn(getColumnName(3)).setWidth(120);
    getColumn(getColumnName(4)).setWidth(120);
    getColumn(getColumnName(5)).setWidth(120);
  }
  private void reinitialiseIO() {
    if (resultatsIO_ == null)
      return;
    VagTableauResultatsModelIO model=
      new VagTableauResultatsModelIO(resultatsIO_);
    model.setFlecheNord(nord_);
    setModel(model);
    BuTableCellRenderer tcr= new BuTableCellRenderer();
    NumberFormat nf= NumberFormat.getInstance();
    nf.setMaximumFractionDigits(2);
    tcr.setNumberFormat(nf);
    for (int i= 0; i < getModel().getColumnCount(); i++)
      getColumn(getColumnName(i)).setCellRenderer(tcr);
    getColumn(getColumnName(0)).setWidth(60);
    getColumn(getColumnName(1)).setWidth(60);
    getColumn(getColumnName(2)).setWidth(60);
    getColumn(getColumnName(3)).setWidth(120);
    getColumn(getColumnName(4)).setWidth(120);
    getColumn(getColumnName(5)).setWidth(120);
    getColumn(getColumnName(6)).setWidth(120);
  }
  /**********************************************/
  // PROPRIETES INTERNES
  /**********************************************/
  // Propriete resultats
  private SResultatUnitaire05 resultats05_;
  SResultatUnitaire05 getResultats05() {
    return resultats05_;
  }
  public void setResultats05(SResultatUnitaire05 _resultats) {
    SResultatUnitaire05 vp= resultats05_;
    resultats05_= _resultats;
    reinitialise05();
    repaint();
    firePropertyChange("resultats05", vp, resultats05_);
  }
  private SResultatUnitaireInterOrth resultatsIO_;
  SResultatUnitaireInterOrth getResultatsInterOrth() {
    return resultatsIO_;
  }
  public void setResultatsInterOrth(SResultatUnitaireInterOrth _resultats) {
    SResultatUnitaireInterOrth vp= resultatsIO_;
    resultatsIO_= _resultats;
    reinitialiseIO();
    repaint();
    firePropertyChange("resultatsInterOrth", vp, resultatsIO_);
  }
}
class VagTableauResultatsModel05 implements TableModel {
  Vector listeners_;
  SResultatUnitaire05 resultats05_;
  public VagTableauResultatsModel05(SResultatUnitaire05 _resultats) {
    listeners_= new Vector();
    resultats05_= _resultats;
  }
  private double nord_;
  public void setFlecheNord(double angle) {
    nord_= angle;
  }
  public Class getColumnClass(int column) {
    return (column == 0 ? Integer.class : Double.class);
  }
  public int getColumnCount() {
    return 6;
  }
  public String getColumnName(int column) {
    String r= "";
    switch (column) {
      case 0 :
        r= "indice";
        break;
      case 1 :
        r= "X";
        break;
      case 2 :
        r= "Y";
        break;
      case 3 :
        r= "angle/Nord";
        break;
      case 4 :
        r= "Longueur d'onde";
        break;
      case 5 :
        r= "Hauteur d'eau";
        break;
    }
    return r;
  }
  public int getRowCount() {
    return resultats05_.pas.length;
  }
  public Object getValueAt(int row, int column) {
    Object r= null;
    switch (column) {
      case 0 :
        r= new Integer(resultats05_.pas[row].numeroPas);
        break;
      case 1 :
        r= new Double(resultats05_.pas[row].pointCourantX);
        break;
      case 2 :
        r= new Double(resultats05_.pas[row].pointCourantY);
        break;
      case 3 :
        r=
          new Double(resultats05_.pas[row].angleHoriz * 180. / Math.PI - nord_);
        break;
      case 4 :
        r= new Double(resultats05_.pas[row].longueurOnde);
        break;
      case 5 :
        r= new Double(resultats05_.pas[row].hauteurEau);
        break;
    }
    return r;
  }
  public boolean isCellEditable(int row, int column) {
    return false;
  }
  public void setValueAt(Object value, int row, int column) {}
  public void addTableModelListener(TableModelListener _l) {
    listeners_.addElement(_l);
  }
  public void removeTableModelListener(TableModelListener _l) {
    listeners_.removeElement(_l);
  }
}
class VagTableauResultatsModelIO implements TableModel {
  Vector listeners_;
  SResultatUnitaireInterOrth resultatsIO_;
  public VagTableauResultatsModelIO(SResultatUnitaireInterOrth _resultats) {
    listeners_= new Vector();
    resultatsIO_= _resultats;
  }
  private double nord_;
  public void setFlecheNord(double angle) {
    nord_= angle;
  }
  public Class getColumnClass(int column) {
    return (column == 0 ? Integer.class : Double.class);
  }
  public int getColumnCount() {
    return 7;
  }
  public String getColumnName(int column) {
    String r= "";
    switch (column) {
      case 0 :
        r= "indice";
        break;
      case 1 :
        r= "X";
        break;
      case 2 :
        r= "Y";
        break;
      case 3 :
        r= "angle/Nord";
        break;
      case 4 :
        r= "Longueur d'onde";
        break;
      case 5 :
        r= "Hauteur d'eau";
        break;
      case 6 :
        r= "Hauteur de houle";
        break;
    }
    return r;
  }
  public int getRowCount() {
    return resultatsIO_.pas.length;
  }
  public Object getValueAt(int row, int column) {
    Object r= null;
    switch (column) {
      case 0 :
        r= new Integer(resultatsIO_.pas[row].numeroPas);
        break;
      case 1 :
        r= new Double(resultatsIO_.pas[row].pointCourantX);
        break;
      case 2 :
        r= new Double(resultatsIO_.pas[row].pointCourantY);
        break;
      case 3 :
        r=
          new Double(resultatsIO_.pas[row].angleHoriz * 180. / Math.PI - nord_);
        break;
      case 4 :
        r= new Double(resultatsIO_.pas[row].longueurOnde);
        break;
      case 5 :
        r= new Double(resultatsIO_.pas[row].hauteurEau);
        break;
      case 6 :
        r= new Double(resultatsIO_.pas[row].hauteurHoule);
        break;
    }
    return r;
  }
  public boolean isCellEditable(int row, int column) {
    return false;
  }
  public void setValueAt(Object value, int row, int column) {}
  public void addTableModelListener(TableModelListener _l) {
    listeners_.addElement(_l);
  }
  public void removeTableModelListener(TableModelListener _l) {
    listeners_.removeElement(_l);
  }
}
