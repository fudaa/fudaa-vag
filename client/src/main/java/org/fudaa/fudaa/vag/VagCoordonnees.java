/*
 * @creation     1999-03-01
 * @modification $Date: 2007-06-28 09:28:18 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.vag;
import org.fudaa.ebli.repere.CoordonneesEvent;
import org.fudaa.ebli.repere.CoordonneesListener;
/**
 * @version      $Revision: 1.6 $ $Date: 2007-06-28 09:28:18 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class VagCoordonnees implements CoordonneesListener {
  public void coordonneesModifiees(CoordonneesEvent _evt) {
    System.err.println("CoordonneesEvent");
  }
}
