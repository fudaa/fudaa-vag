/*
 * @creation     1999-06-09
 * @modification $Date: 2007-06-28 09:28:18 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.vag;
import java.awt.Dimension;

import org.fudaa.dodico.corba.vag.SResultatsInterOrth;

import org.fudaa.ebli.calque.BCalqueCarte;
import org.fudaa.ebli.geometrie.GrMaillage;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.palette.BPaletteCouleurSimple;
/**
 * Un calque de surface de hauteur de houle de la mer de la cote.
 *
 * @version      $Revision: 1.7 $ $Date: 2007-06-28 09:28:18 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class VagCalqueHauteurHoule extends BCalqueCarte {
  // donnees membres publiques
  // donnees membres privees
  // Constructeur
  public VagCalqueHauteurHoule() {
    super();
    resultatsInterOrth_= null;
    super.setPaletteCouleur(new BPaletteCouleurSimple());
    setPreferredSize(new Dimension(640, 480));
  }
  /**********************************************/
  // PROPRIETES INTERNES
  /**********************************************/
  // Propriete resultats
  private SResultatsInterOrth resultatsInterOrth_;
  SResultatsInterOrth getResultatsInterOrth() {
    return resultatsInterOrth_;
  }
  public void setResultatsInterOrth(SResultatsInterOrth _resultats) {
    SResultatsInterOrth vp= resultatsInterOrth_;
    resultatsInterOrth_= _resultats;
    reinitialise();
    construitMaillage();
    firePropertyChange("resultatsInterOrth", vp, resultatsInterOrth_);
  }
  // Methodes privees
  private void construitMaillage() {
    if ((resultatsInterOrth_.interOrth == null)
      || (resultatsInterOrth_.interOrth.length == 0))
      return;
    int nb= 0;
    int pos= 0;
    GrMaillage m= new GrMaillage();
    // calcul du nombre total de points
    for (int o= 0; o < resultatsInterOrth_.interOrth.length; o++) {
      nb += resultatsInterOrth_.interOrth[o].pas.length;
    }
    double[] vals= new double[nb];
    // nombre de points ajoutes sur les o-2 orth precedentes
    nb= 0;
    // o=0;
    // p=0;
    m.noeuds_.ajoute(
      new GrPoint(
        resultatsInterOrth_.interOrth[0].pas[0].pointCourantX,
        resultatsInterOrth_.interOrth[0].pas[0].pointCourantY,
        0.));
    vals[pos]= resultatsInterOrth_.interOrth[0].pas[0].hauteurHoule;
    pos++;
    for (int p= 1; p < resultatsInterOrth_.interOrth[0].pas.length; p++) {
      m.noeuds_.ajoute(
        new GrPoint(
          resultatsInterOrth_.interOrth[0].pas[p].pointCourantX,
          resultatsInterOrth_.interOrth[0].pas[p].pointCourantY,
          0.));
      vals[pos]= resultatsInterOrth_.interOrth[0].pas[p].hauteurHoule;
      // pas de connectivite sur la premiere orth
      pos++;
    }
    // on ne touche pas a nb car nb totalise les points des o-2 orth prec
    for (int o= 1; o < resultatsInterOrth_.interOrth.length; o++) {
      // p=0;
      m.noeuds_.ajoute(
        new GrPoint(
          resultatsInterOrth_.interOrth[o].pas[0].pointCourantX,
          resultatsInterOrth_.interOrth[o].pas[0].pointCourantY,
          0.));
      vals[pos]= resultatsInterOrth_.interOrth[o].pas[0].hauteurHoule;
      // pas de connectivite pour le premier pas
      pos++;
      for (int p= 1; p < resultatsInterOrth_.interOrth[o].pas.length; p++) {
        m.noeuds_.ajoute(
          new GrPoint(
            resultatsInterOrth_.interOrth[o].pas[p].pointCourantX,
            resultatsInterOrth_.interOrth[o].pas[p].pointCourantY,
            0.));
        vals[pos]= resultatsInterOrth_.interOrth[o].pas[p].hauteurHoule;
        // test: le point p de l'orth prec existe-t-il?
        if (p < resultatsInterOrth_.interOrth[o - 1].pas.length) {
          int c[]= new int[4];
          // p-1 sur l'orth prec
          c[0]= nb + p - 1;
          // p   sur l'orth prec
          c[1]= nb + p;
          // p   sur l'orth cour
          c[2]= pos;
          // p-1 sur l'orth cour
          c[3]= pos - 1;
          m.connectivites_.add(c);
        }
        pos++;
      }
      nb += resultatsInterOrth_.interOrth[o - 1].pas.length;
    }
    super.setMaillage(m);
    super.setValeurs(vals);
  }
}
