/*
 * @file         VagImport.java
 * @creation     1998-10-06
 * @modification $Date: 2007-01-19 13:14:35 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.vag;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.FileDialog;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuGridLayout;

import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.fudaa.commun.impl.FudaaDialogImport;
/**
 * Boite d'importation de fichiers Vag.
 *
 * @version      $Revision: 1.8 $ $Date: 2007-01-19 13:14:35 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class VagImport
  extends FudaaDialogImport
  implements ActionListener, WindowListener {
  // Donnees membres privees
  private JTextField tf_vag01_, tf_vag02_, tf_vag03_, tf_vag04_;
  private JButton b_vag01_, b_vag02_, b_vag03_, b_vag04_, ok_, cancel_;
  private BuGridLayout lo_vag_;
  private JPanel pl_vag_, pl_ok_;
  private Frame parent_;
  // Constructeur
  public VagImport(BuCommonInterface _parent) {
    super(_parent instanceof Frame ? (Frame)_parent : null);
    parent_= _parent instanceof Frame ? (Frame)_parent : null;
    int n= 0;
    pl_vag_= new JPanel();
    lo_vag_= new BuGridLayout();
    lo_vag_.setColumns(3);
    lo_vag_.setVgap(5);
    lo_vag_.setHgap(5);
    pl_vag_.setLayout(lo_vag_);
    pl_vag_.add(new JLabel("Vag01: "), n++);
    tf_vag01_= new JTextField();
    tf_vag01_.setColumns(20);
    pl_vag_.add(tf_vag01_, n++);
    b_vag01_= new JButton("Parcourir");
    b_vag01_.setActionCommand(VagResource.VAG01);
    b_vag01_.addActionListener(this);
    pl_vag_.add(b_vag01_, n++);
    pl_vag_.add(new JLabel("Vag02: "), n++);
    tf_vag02_= new JTextField();
    tf_vag02_.setColumns(20);
    pl_vag_.add(tf_vag02_, n++);
    b_vag02_= new JButton("Parcourir");
    b_vag02_.setActionCommand(VagResource.VAG02);
    b_vag02_.addActionListener(this);
    pl_vag_.add(b_vag02_, n++);
    pl_vag_.add(new JLabel("Vag03: "), n++);
    tf_vag03_= new JTextField();
    tf_vag03_.setColumns(20);
    pl_vag_.add(tf_vag03_, n++);
    b_vag03_= new JButton("Parcourir");
    b_vag03_.setActionCommand(VagResource.VAG03);
    b_vag03_.addActionListener(this);
    pl_vag_.add(b_vag03_, n++);
    pl_vag_.add(new JLabel("Vag04: "), n++);
    tf_vag04_= new JTextField();
    tf_vag04_.setColumns(20);
    pl_vag_.add(tf_vag04_, n++);
    b_vag04_= new JButton("Parcourir");
    b_vag04_.setActionCommand(VagResource.VAG04);
    b_vag04_.addActionListener(this);
    pl_vag_.add(b_vag04_, n++);
    pl_ok_= new JPanel();
    ok_= new JButton("Valider");
    ok_.setActionCommand("VALIDER");
    ok_.addActionListener(this);
    cancel_= new JButton("Annuler");
    cancel_.setActionCommand("ANNULER");
    cancel_.addActionListener(this);
    pl_ok_.add(ok_);
    pl_ok_.add(cancel_);
    Container content= getContentPane();
    content.setLayout(new BorderLayout());
    content.add(pl_vag_, BorderLayout.NORTH);
    content.add(pl_ok_, BorderLayout.SOUTH);
    pack();
    setResizable(false);
    if (_parent instanceof Component)
      setLocationRelativeTo((Component)_parent);
  }
  // Methodes publiques
  // Actions
  public void actionPerformed(ActionEvent _evt) {
    String action= _evt.getActionCommand();
    System.err.println("ACTION=" + action);
    if (action.equals("ANNULER")) {
      //      imports_=null;
      dispose();
    } else if (action.equals("VALIDER")) {
      imports_.put(VagResource.VAG01, tf_vag01_.getText());
      imports_.put(VagResource.VAG02, tf_vag02_.getText());
      imports_.put(VagResource.VAG03, tf_vag03_.getText());
      imports_.put(VagResource.VAG04, tf_vag04_.getText());
      dispose();
    } else if (
      action.equals(VagResource.VAG01)
        || action.equals(VagResource.VAG02)
        || action.equals(VagResource.VAG03)
        || action.equals(VagResource.VAG04)) {
      popVagFileChooser(action);
    }
  }
  public void onlyEnable(String _fieldId) {
    System.err.println("onlyEnable :" + _fieldId + CtuluLibString.DOT);
    if (!_fieldId.equals(VagResource.VAG01)) {
      tf_vag01_.setEnabled(false);
      b_vag01_.setEnabled(false);
    }
    if (!_fieldId.equals(VagResource.VAG02)) {
      tf_vag02_.setEnabled(false);
      b_vag02_.setEnabled(false);
    }
    if (!_fieldId.equals(VagResource.VAG03)) {
      tf_vag03_.setEnabled(false);
      b_vag03_.setEnabled(false);
    }
    if (!_fieldId.equals(VagResource.VAG04)) {
      tf_vag04_.setEnabled(false);
      b_vag04_.setEnabled(false);
    }
  }
  // Window events
  public void windowActivated(WindowEvent e) {}
  public void windowClosed(WindowEvent e) {}
  public void windowClosing(WindowEvent e) {
    imports_= null;
    dispose();
  }
  public void windowDeactivated(WindowEvent e) {}
  public void windowDeiconified(WindowEvent e) {}
  public void windowIconified(WindowEvent e) {}
  public void windowOpened(WindowEvent e) {}
  // Methodes privees
  private void popVagFileChooser(String _type) {
    FileDialog FDOpen= null;
    String DIROpen= null;
    String r= "";
    if (FDOpen == null) {
      FDOpen= new FileDialog(parent_, _type, FileDialog.LOAD);
    }
    if (DIROpen == null)
      DIROpen= FDOpen.getDirectory();
    if (DIROpen != null)
      FDOpen.setDirectory(DIROpen);
    FDOpen.show();
    DIROpen= FDOpen.getDirectory();
    r= FDOpen.getFile();
    if (r != null)
      r= DIROpen + r;
    /**** version Swing
    
    JFileChooser chooser=new JFileChooser();
    
    // ExtensionFileFilter filter = new ExtensionFileFilter();
    // filter.addExtension("jpg");
    // filter.addExtension("gif");
    // filter.setDescription("JPG & GIF Images");
    // chooser.setFileFilter(filter);
    
    int returnVal = chooser.showOpenDialog(this);
    if(returnVal == JFileChooser.APPROVE_OPTION)
    {
      r=chooser.getSelectedFile().getPath(); // .getName();
      System.out.println("You chose to open this file: "+r);
    }
    else r=null;
    
    ****/
    if (r != null) {
      if (_type.equals(VagResource.VAG01))
        tf_vag01_.setText(r);
      else if (_type.equals(VagResource.VAG02))
        tf_vag02_.setText(r);
      else if (_type.equals(VagResource.VAG03))
        tf_vag03_.setText(r);
      else if (_type.equals(VagResource.VAG04))
        tf_vag04_.setText(r);
    }
  }
}
