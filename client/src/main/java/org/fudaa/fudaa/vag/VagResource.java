/*
 * @file         VagResource.java
 * @creation     1998-10-16
 * @modification $Date: 2005-08-16 14:17:03 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.vag;
import com.memoire.bu.BuResource;
/**
 * Le gestionnaire de ressources de Vag.
 *
 * @version      $Revision: 1.5 $ $Date: 2005-08-16 14:17:03 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class VagResource extends BuResource {
  public final static String VAG01= "01";
  public final static String VAG02= "02";
  public final static String VAG03= "03";
  public final static String VAG04= "04";
  public final static String VAG05= "05";
  public final static String VAGIO= "InterOrth";
  public final static String VAGOUT= "OUT";
  public final static VagResource VAG= new VagResource();
  public final static int PRECISION= 1000;
}
