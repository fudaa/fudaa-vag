/*
 * @creation     2000-04-07
 * @modification $Date: 2007-06-28 09:28:18 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.vag;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.text.NumberFormat;
import java.util.Locale;

import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.border.TitledBorder;

import com.memoire.bu.*;

import org.fudaa.dodico.corba.vag.SParametres01;
import org.fudaa.dodico.corba.vag.SParametres02;
import org.fudaa.dodico.corba.vag.SParametres03;
import org.fudaa.dodico.corba.vag.SParametres04;

import org.fudaa.ebli.calque.*;
import org.fudaa.ebli.calque.dessin.DeForme;
import org.fudaa.ebli.controle.BSelecteurReduitParametresGouraud;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMaillageElement;
import org.fudaa.ebli.geometrie.GrObjet;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.ebli.geometrie.GrSegment;
import org.fudaa.ebli.palette.BPaletteProprietesSurface;
import org.fudaa.ebli.repere.RepereEvent;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.trace.TraceLigne;

import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.commun.projet.FudaaParamEvent;
import org.fudaa.fudaa.commun.projet.FudaaParamEventProxy;
import org.fudaa.fudaa.commun.projet.FudaaParamListener;
import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.commun.projet.FudaaProjetEvent;
import org.fudaa.fudaa.commun.projet.FudaaProjetListener;
/**
 * Une fenetre fille pour le mailleur.
 *
 * @version      $Revision: 1.11 $ $Date: 2007-06-28 09:28:18 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class VagFilleMailleur
  extends EbliFilleCalques
  implements FormeEventListener, FudaaProjetListener, FudaaParamListener {
  BGroupeCalque gc_;
  FudaaProjet projet_;
  VagMaillage maillage_;
  BCalquePoint cqSemis_;
  BCalqueGrilleReguliere cqGrille_;
  BCalqueFormeInteraction cqFormeI_;
  BCalqueMaillageElement cqMaillage_;
  BCalqueGrille cg;
  BCalqueDomaine dom;
  BCalqueRosace r;
  VagCalqueCotes c;
  GrPolygone rectangle_;
  GrSegment segArret_;
  GrPoint pointOrth_;
  int nbMaillesX_, nbMaillesY_;
  double nord_;
  BuButton btImportSemis_, btParamsMaill_, btTriang_, btMailler_;
  BuToggleButton btRect_, btArret_, btOrth_;
  boolean readOnly_;
  JComponent[] tools_;
  public VagFilleMailleur(BArbreCalque _arbre, FudaaProjet p, VagMaillage m) {
    //    super(_vc, null,p.getInformationsDocument());
    super(null, p.getInformationsDocument());
    addInternalFrameListener(_arbre);
    setName("MAILLEUR");
    setTitle("Mailleur");
    setSelectionVisible(false);
    readOnly_= false;
    tools_= null;
    projet_= p;
    rectangle_= null;
    segArret_= null;
    pointOrth_= null;
    nbMaillesX_= nbMaillesY_= 0;
    nord_= 90.;
    FudaaParamEventProxy.FUDAA_PARAM.addFudaaParamListener(this);
    projet_.addFudaaProjetListener(this);
    gc_= construitCalques();
    setCalque(gc_);
    //restaurer();
    maillage_= m;
    btImportSemis_= new BuButton("Semis");
    btImportSemis_.setName("SEMIS");
    btImportSemis_.setEnabled(true);
    btImportSemis_.setToolTipText("Importer un semis de points");
    btImportSemis_.addActionListener(this);
    addBouton(btImportSemis_);
    btTriang_= new BuButton("Mailler");
    btTriang_.setName("TRIANGULATION");
    btTriang_.setEnabled(false);
    btTriang_.setToolTipText("Lancer le maillage");
    btTriang_.addActionListener(this);
    addBouton(btTriang_);
    btRect_= new BuToggleButton("Rectangle");
    btRect_.setName("RECTANGLE");
    btRect_.setEnabled(false);
    btRect_.setToolTipText("Tracer le rectangle de la grille");
    btRect_.addActionListener(this);
    addBouton(btRect_);
    btParamsMaill_= new BuButton("Grille");
    btParamsMaill_.setName("GRILLE");
    btParamsMaill_.setEnabled(false);
    btParamsMaill_.setToolTipText("Param�tres de la grille");
    btParamsMaill_.addActionListener(this);
    addBouton(btParamsMaill_);
    btMailler_= new BuButton("Interpoler");
    btMailler_.setName("INTERPOLATION");
    btMailler_.setEnabled(false);
    btMailler_.setToolTipText("Lancer l'interpolation");
    btMailler_.addActionListener(this);
    addBouton(btMailler_);
    btArret_= new BuToggleButton("Arret");
    btArret_.setName("ARRET");
    btArret_.setEnabled(false);
    btArret_.setToolTipText("Segment d'arret");
    btArret_.addActionListener(this);
    addBouton(btArret_);
    btOrth_= new BuToggleButton("Orthogonale");
    btOrth_.setName("ORTHOGONALE");
    btOrth_.setEnabled(false);
    btOrth_.setToolTipText("Point de d�part de 1�re orthogonale");
    btOrth_.addActionListener(this);
    addBouton(btOrth_);
    associeBoutonsCalqueInteraction(
      cqFormeI_,
      new JToggleButton[] { btRect_, btArret_, btOrth_ });
  }
  // LidoParamListener
  public void paramStructCreated(FudaaParamEvent e) {
    if (e.getSource() != this)
      updateCalques(e);
  }
  public void paramStructDeleted(FudaaParamEvent e) {
    if (e.getSource() != this)
      updateCalques(e);
  }
  public void paramStructModified(FudaaParamEvent e) {
    if (e.getSource() != this)
      updateCalques(e);
  }
  // fudaaprojet listener
  public void dataChanged(FudaaProjetEvent e) {
    switch (e.getID()) {
      case FudaaProjetEvent.PARAM_ADDED :
      case FudaaProjetEvent.PARAM_IMPORTED :
        {
          if (e.getSource() != this)
            updateCalques(null);
          break;
        }
    }
  }
  public void statusChanged(FudaaProjetEvent e) {
    switch (e.getID()) {
      case FudaaProjetEvent.PROJECT_OPENED :
      case FudaaProjetEvent.PROJECT_CLOSED :
        {
          if (e.getSource() != this)
            updateCalques(null);
          break;
        }
    }
  }
  private void calcSpecificTools() {
    JComponent[] tools= super.getSpecificTools();
    JComponent[] res= new JComponent[tools.length + 2];
    for (int i= 0; i < tools.length; i++)
      res[i]= tools[i];
    BSelecteurReduitParametresGouraud sg=
      new BSelecteurReduitParametresGouraud();
    sg.addPropertyChangeListener(getArbreCalqueModel());
    sg.doLayout();
    BuPopupButton pbg= new BuPopupButton(FudaaLib.getS("Gouraud"), sg);
    pbg.setToolTipText(FudaaLib.getS("Param�tres de lissage"));
    pbg.setIcon(EbliResource.EBLI.getIcon("lissage"));
    pbg.setDesktop((BuDesktop)getDesktopPane());
    /*fred deniger: ces 6 lignes sont a supprimer par la suite. Il y a un
    probleme d'affichage pour ce BuPopupButton:les sliders ne sont pas
    affiches la premiere fois.
    Solution temporaire: la fenetre de ce BuPopButton est affichee puis cachee
    aussitot.
    */
    //pour ne pas voir l'affichage temporaire.
    pbg.setLocation(-1000, -1000);
    pbg.setSelected(true);
    pbg.setVisible(true);
    pbg.setVisible(false);
    pbg.setSelected(false);
    pbg.setLocation(0, 0);
    BPaletteProprietesSurface ccs= new BPaletteProprietesSurface();
    ccs.addPropertyChangeListener(getArbreCalqueModel());
    BuPopupButton popCcs= new BuPopupButton("Surface", ccs);
    popCcs.setToolTipText("Param�tres de surface");
    popCcs.setIcon(EbliResource.EBLI.getIcon("surface"));
    popCcs.setDesktop((BuDesktop)getDesktopPane());
    res[tools.length]= pbg;
    res[tools.length + 1]= popCcs;
    tools_= res;
  }
  public JComponent[] getSpecificTools() {
    if (tools_ == null)
      calcSpecificTools();
    return tools_;
  }
  public void setLectureSeule() {
    readOnly_= true;
    btImportSemis_.setEnabled(false);
    btMailler_.setEnabled(false);
    btParamsMaill_.setEnabled(false);
    btRect_.setEnabled(false);
    btTriang_.setEnabled(false);
    btArret_.setEnabled(false);
    btOrth_.setEnabled(false);
  }
  protected BGroupeCalque construitCalques() {
    try {
      dom= new BCalqueDomaine();
      dom.setName("cqDOMAINEMAILLEUR");
      dom.setTitle("Domaine");
      dom.setFont(new Font("SansSerif", Font.PLAIN, 8));
      dom.setAttenue(false);
      dom.setVisible(false);
      cg= new BCalqueGrille();
      cg.setForeground(Color.lightGray);
      cg.setTitle("Grille");
      cg.setName("cqGRILLEMAILLEUR");
      BCalqueLegende leg= new BCalqueLegende();
      leg.setTitle("L�gende");
      leg.setName("cqLEGENDEMAILLEUR");
      leg.setForeground(java.awt.Color.blue);
      c= new VagCalqueCotes();
      c.setTitle("Cotes");
      c.setName("cqCOTESMAILLEUR");
      c.setParametres((SParametres02)projet_.getParam(VagResource.VAG02));
      cqSemis_= new BCalquePoint();
      cqSemis_.setTitle("Semis");
      cqSemis_.setName("cqPOINTMAILLEUR");
      cqGrille_= new BCalqueGrilleReguliere();
      cqGrille_.setTitle("Grille maillage");
      cqGrille_.setName("cqGRILLEREGMAILLEUR");
      cqGrille_.setBackground(new Color(200, 0, 0));
      cqGrille_.setForeground(Color.black);
      cqGrille_.setParametresGouraud(null);
      cqGrille_.setLegende(leg);
      cqGrille_.setContour(true);
      cqGrille_.setSurface(false);
      cqGrille_.setIsolignes(false);
      cqGrille_.setIsosurfaces(false);
      cqFormeI_= new BCalqueFormeInteraction(cqSemis_);
      cqFormeI_.setTitle("Saisie Rectangle");
      cqFormeI_.setName("cqFORMEIMAILLEUR");
      cqFormeI_.setForeground(Color.red);
      cqFormeI_.setTypeForme(DeForme.RECTANGLE);
      cqFormeI_.setTypeTrait(TraceLigne.LISSE);
      cqFormeI_.addFormeEventListener(this);
      cqFormeI_.setGele(true);
      cqMaillage_= new BCalqueMaillageElement();
      cqMaillage_.setTitle("Maillage");
      cqMaillage_.setName("cqMAILLAGEMAILLEUR");
      cqMaillage_.setAttenue(true);
      r= new BCalqueRosace();
      r.setTitle("Fl�che Nord");
      r.setName("cqROSACEMAILLEUR");
      SParametres01 par= (SParametres01)projet_.getParam(VagResource.VAG01);
      if (par != null) {
        r.setAngleNord(par.flecheNordDir * Math.PI / 180.);
        nord_= par.flecheNordDir * Math.PI / 180.;
      }
      r.setBackground(new Color(224, 224, 255));
      BGroupeCalque gc= new BGroupeCalque();
      gc.setTitle("Mailleur");
      gc.add(leg);
      gc.add(c);
      cqGrille_.add(cqFormeI_);
      gc.add(cqSemis_);
      gc.add(cqGrille_);
      //gc.add(cb);
      gc.add(cqMaillage_);
      gc.add(r);
      gc.add(cg);
      gc.add(dom);
      gc.doLayout();
      GrBoite bo= gc.getDomaine();
      if (bo != null)
        cg.setBoite(bo);
      return gc;
    } catch (Throwable t) {
      t.printStackTrace();
    }
    return null;
  }
  protected void reinitialise() {
    nbMaillesX_= nbMaillesY_= 0;
    rectangle_= null;
    nord_= 90.;
    c.reinitialise();
    cqSemis_.reinitialise();
    r.setAngleNord(Math.PI / 2);
    cqGrille_.reinitialise();
    cqFormeI_.setGele(true);
    cqMaillage_.enleveTous();
    cqMaillage_.ajoute(new GrMaillageElement());
    cg.setBoite(null);
    btRect_.setSelected(false);
    btArret_.setSelected(false);
    btOrth_.setSelected(false);
    restaurer();
  }
  public void delete() {
    reinitialise();
    gc_.removeAll();
    gc_= null;
    c= null;
    cqSemis_= null;
    cqGrille_.remove(cqFormeI_);
    cqGrille_= null;
    cqFormeI_= null;
    cqMaillage_= null;
    cg= null;
    dom= null;
    r= null;
    maillage_.delete();
    maillage_= null;
    FudaaParamEventProxy.FUDAA_PARAM.removeFudaaParamListener(this);
    if (projet_ != null)
      projet_.removeFudaaProjetListener(this);
    projet_= null;
    tools_= null;
    rectangle_= null;
  }
  protected void updateCalques(FudaaParamEvent e) {
    Object param= null;
    if (e != null)
      param= e.getStruct();
    BCalque[] calques= gc_.getTousCalques();
//    BCalqueGrille cg= null;
    SParametres01 p01= (SParametres01)projet_.getParam(VagResource.VAG01);
    SParametres02 p02= (SParametres02)projet_.getParam(VagResource.VAG02);
    for (int i= 0; i < calques.length; i++) {
      if ("cqDOMAINEMAILLEUR"
        .equals(calques[i].getName())) {} else if ("cqGRILLEMAILLEUR"
        .equals(calques[i].getName())) {
//        cg= (BCalqueGrille)calques[i];
      } else if ("cqCOTESMAILLEUR".equals(calques[i].getName())) {
        if (p02 != null)
          if (param == p02)
             ((VagCalqueCotes)calques[i]).setParametres(p02);
          else
             ((VagCalqueCotes)calques[i]).reinitialise();
      } else if ("cqMAILLAGEMAILLEUR".equals(calques[i].getName())) {
        //((BCalqueMaillageElement)calques[i]).setMaillage(null);
      } else if ("cqPOINTMAILLEUR".equals(calques[i].getName())) {} else if (
        "cqGRILLEREGMAILLEUR".equals(calques[i].getName())) {
        if (p01 == null)
           ((BCalqueGrilleReguliere)calques[i]).reinitialise();
      } else if ("cqFORMEIMAILLEUR".equals(calques[i].getName())) {} else if (
        "cqROSACEMAILLEUR".equals(calques[i].getName())) {
        if (p01 != null)
          if (param == p01) {
            ((BCalqueRosace)calques[i]).setAngleNord(
              p01.flecheNordDir * Math.PI / 180.);
            nord_= p01.flecheNordDir;
          } else {
            ((BCalqueRosace)calques[i]).setAngleNord(Math.PI / 2);
            nord_= 90.;
          }
      }
    }
    if (cg != null)
      cg.setBoite(gc_.getDomaine());
  }
  public void repereModifie(RepereEvent e) {
    if (e.aChange(2, 2)) {
      double rot= e.valeur(2, 2);
      if (e.relatif(2, 2)) {
        nord_ -= rot * 180. / Math.PI;
      } else {
        nord_= 90. - rot * 180. / Math.PI;
      }
    }
  }
  public void restaurer() {
    nord_= 90.;
    super.restaurer();
  }
  public void formeSaisie(FormeEvent e) {
    //    cqFormeI_.setGele(true);
    try {
      if (readOnly_)
        return;
      GrObjet forme= e.getForme().getGeometrie();
      if (btRect_.isSelected()) {
        if (!(forme instanceof GrPolygone))
          return;
        if (!btParamsMaill_.isEnabled())
          btParamsMaill_.setEnabled(true);
        // reclassement des points du rectangle
        GrPolygone tmpRect= (GrPolygone)forme;
        reclasseRectangle(tmpRect);
        if ((nbMaillesX_ == 0) || (nbMaillesY_ == 0)) {
          VagGrilleParamEditor ed=
            new VagGrilleParamEditor(
              VagFilleMailleur.this,
              rectangle_,
              nbMaillesX_,
              nbMaillesY_);
          ed.show();
          if (!ed.isValidee())
            return;
          rectangle_= ed.getRectangle();
          nbMaillesX_= ed.getMailleX();
          nbMaillesY_= ed.getMailleY();
        }
        recalculeGrille();
        getVueCalque().repaint();
        if (!btMailler_.isEnabled())
          btMailler_.setEnabled(true);
      } else if (btArret_.isSelected()) {
        if (!(forme instanceof GrSegment))
          return;
        segArret_= (GrSegment)forme;
      } else if (btOrth_.isSelected()) {
        if (!(forme instanceof GrPoint))
          return;
        pointOrth_= (GrPoint)forme;
      }
      //    btRect_.setSelected(false);
      //    btArret_.setSelected(false);
      //    btOrth_.setSelected(false);
      updateParams01();
    } finally {
      cqFormeI_.setGele(true);
    }
  }
  public Rectangle2D.Double getRectangle() {
    double ox= rectangle_.sommets_.renvoie(3).x_;
    double oy= rectangle_.sommets_.renvoie(3).y_;
    double l=
      Math.abs(
        rectangle_.sommets_.renvoie(1).x_ - rectangle_.sommets_.renvoie(0).x_);
    double h=
      Math.abs(
        rectangle_.sommets_.renvoie(3).y_ - rectangle_.sommets_.renvoie(0).y_);
    return new Rectangle2D.Double(ox, oy, l, h);
  }
  public void actionPerformed(ActionEvent e) {
    Object src= e.getSource();
    if (src == btImportSemis_) {
      JFileChooser chooser= new JFileChooser();
      int returnVal= chooser.showOpenDialog(VagImplementation.FRAME);
      String filename= null;
      if (returnVal == JFileChooser.APPROVE_OPTION) {
        filename= chooser.getSelectedFile().getAbsolutePath();
      }
      if (filename == null)
        return;
      final String fic= filename;
      File f= new File(fic);
      if ((!f.exists()) || (!f.canRead())) {
        new BuDialogError(
          (BuCommonInterface)VagImplementation.FRAME,
          ((BuCommonInterface)VagImplementation.FRAME)
            .getInformationsSoftware(),
          "fichier " + fic + " introuvable ou illisible")
          .activate();
        return;
      }
      BuTaskOperation t=
        new BuTaskOperation(
          (BuCommonInterface)VagImplementation.FRAME,
          "Importation") {
        public void act() {
          VagFilleMailleur.this.reinitialise();
          maillage_.importerSemis(new File(fic));
          cqSemis_.setPoints(maillage_.getSemis());
          cg.setBoite(gc_.getDomaine());
          restaurer();
          btMailler_.setEnabled(false);
          btParamsMaill_.setEnabled(false);
          btRect_.setEnabled(false);
          btArret_.setEnabled(false);
          btOrth_.setEnabled(false);
          if (!btTriang_.isEnabled())
            btTriang_.setEnabled(true);
          restaurer();
        }
      };
      t.start();
    } else if (src == btMailler_) {
      BuTaskOperation t=
        new BuTaskOperation(
          (BuCommonInterface)VagImplementation.FRAME,
          "Interpolation") {
        public void act() {
          try {
            maillage_.interpoler(rectangle_, nbMaillesX_, nbMaillesY_);
            cqGrille_.setSurface(true);
            cqGrille_.setContour(false);
            cqGrille_.setRectangle(rectangle_);
            cqGrille_.setValeurs(maillage_.getValeurs());
            getVueCalque().repaint();
            updateParams03();
            if (!btArret_.isEnabled())
              btArret_.setEnabled(true);
            if (!btOrth_.isEnabled())
              btOrth_.setEnabled(true);
          } catch (IllegalArgumentException ex) {
            new BuDialogError(
              (BuCommonInterface)VagImplementation.FRAME,
              ((BuCommonInterface)VagImplementation.FRAME)
                .getInformationsSoftware(),
              ex.getMessage())
              .activate();
          }
        }
      };
      t.start();
    } else if (src == btTriang_) {
      BuTaskOperation t=
        new BuTaskOperation(
          (BuCommonInterface)VagImplementation.FRAME,
          "Maillage") {
        public void act() {
          maillage_.mailler();
          cqMaillage_.enleveTous();
          cqMaillage_.ajoute(maillage_.getMaillage());
          getVueCalque().repaint();
          if (!btRect_.isEnabled())
            btRect_.setEnabled(true);
        }
      };
      t.start();
    } else if (src == btRect_) {
      //      btOrth_.setSelected(false);
      //      btArret_.setSelected(false);
      //      btRect_.setSelected(true);
      cqFormeI_.setTypeForme(DeForme.RECTANGLE);
      //      cqFormeI_.setGele(false);
    } else if (src == btArret_) {
      //      btRect_.setSelected(false);
      //      btOrth_.setSelected(false);
      //      btArret_.setSelected(true);
      cqFormeI_.setTypeForme(DeForme.TRAIT);
      //      cqFormeI_.setGele(false);
    } else if (src == btOrth_) {
      //      btArret_.setSelected(false);
      //      btRect_.setSelected(false);
      //      btOrth_.setSelected(true);
      cqFormeI_.setTypeForme(DeForme.POINT);
      //      cqFormeI_.setGele(false);
    } else if (src == btParamsMaill_) {
      VagGrilleParamEditor ed=
        new VagGrilleParamEditor(
          VagFilleMailleur.this,
          rectangle_,
          nbMaillesX_,
          nbMaillesY_);
      ed.show();
      if (!ed.isValidee())
        return;
      rectangle_= ed.getRectangle();
      nbMaillesX_= ed.getMailleX();
      nbMaillesY_= ed.getMailleY();
      recalculeGrille();
      if (!btMailler_.isEnabled())
        btMailler_.setEnabled(true);
      updateParams01();
    } else
      super.actionPerformed(e);
  }
  private void recalculeGrille() {
    double[][] vvz= new double[nbMaillesX_ + 1][nbMaillesY_ + 1];
    for (int i= 0; i < vvz.length; i++)
      for (int j= 0; j < vvz[0].length; j++)
        vvz[i][j]= 0.;
    cqGrille_.setSurface(false);
    cqGrille_.setContour(true);
    cqGrille_.setRectangle(rectangle_);
    cqGrille_.setValeurs(vvz);
  }
  private void reclasseRectangle(GrPolygone tmpRect) {
    int miny1= 0;
    int miny2= 0;
    int maxy1= 0;
    int maxy2= 0;
    double y= Double.POSITIVE_INFINITY;
    GrPoint tmpP;
    for (int i= 1; i < 4; i++) {
      tmpP= tmpRect.sommets_.renvoie(i);
      if ((tmpP.y_ < y)) {
        miny1= i;
        y= tmpP.y_;
      }
    }
    y= Double.POSITIVE_INFINITY;
    for (int i= 0; i < 4; i++) {
      if (i == miny1)
        continue;
      tmpP= tmpRect.sommets_.renvoie(i);
      if ((tmpP.y_ < y)) {
        miny2= i;
        y= tmpP.y_;
      }
    }
    for (int i= 0; i < 4; i++) {
      if ((i == miny1) || (i == miny2))
        continue;
      maxy1= i;
    }
    for (int i= 0; i < 4; i++) {
      if ((i == miny1) || (i == miny2) || (i == maxy1))
        continue;
      maxy2= i;
    }
    rectangle_= new GrPolygone();
    GrPoint p1= tmpRect.sommets_.renvoie(maxy1);
    GrPoint p2= tmpRect.sommets_.renvoie(maxy2);
    if (p1.x_ < p2.x_) {
      rectangle_.sommets_.ajoute(p1);
      rectangle_.sommets_.ajoute(p2);
    } else {
      rectangle_.sommets_.ajoute(p2);
      rectangle_.sommets_.ajoute(p1);
    }
    p1= tmpRect.sommets_.renvoie(miny1);
    p2= tmpRect.sommets_.renvoie(miny2);
    if (p1.x_ < p2.x_) {
      rectangle_.sommets_.ajoute(p2);
      rectangle_.sommets_.ajoute(p1);
    } else {
      rectangle_.sommets_.ajoute(p1);
      rectangle_.sommets_.ajoute(p2);
    }
  }
  private void updateParams01() {
    if (rectangle_ == null)
      return;
    SParametres01 params01= (SParametres01)projet_.getParam(VagResource.VAG01);
    SParametres04 params04= (SParametres04)projet_.getParam(VagResource.VAG04);
    params01.echelle= 1000.;
    double X, Y;
    double ox= rectangle_.sommets_.renvoie(3).x_;
    double oy= rectangle_.sommets_.renvoie(3).y_;
    double gx= rectangle_.sommets_.renvoie(3).x_;
    double gy= rectangle_.sommets_.renvoie(3).y_;
    double a= - (nord_ - 90.) * Math.PI / 180.;
    double dx= rectangle_.sommets_.renvoie(2).x_;
    double dy= rectangle_.sommets_.renvoie(2).y_;
    double l=
      (rectangle_.sommets_.renvoie(1).x_ - rectangle_.sommets_.renvoie(0).x_)
        * (rectangle_.sommets_.renvoie(1).x_ - rectangle_.sommets_.renvoie(0).x_)
        + (rectangle_.sommets_.renvoie(1).y_ - rectangle_.sommets_.renvoie(0).y_)
          * (rectangle_.sommets_.renvoie(1).y_ - rectangle_.sommets_.renvoie(0).y_);
    l= Math.sqrt(l);
    double h=
      (rectangle_.sommets_.renvoie(3).x_ - rectangle_.sommets_.renvoie(0).x_)
        * (rectangle_.sommets_.renvoie(3).x_ - rectangle_.sommets_.renvoie(0).x_)
        + (rectangle_.sommets_.renvoie(3).y_ - rectangle_.sommets_.renvoie(0).y_)
          * (rectangle_.sommets_.renvoie(3).y_ - rectangle_.sommets_.renvoie(0).y_);
    h= Math.sqrt(h);
    if ((gx != params01.coinInfGX)
      || (gy != params01.coinInfGY)
      || (dx != params01.coinInfDX)
      || (dy != params01.coinInfDY)
      || (l != params01.rectLargeur)
      || (h != params01.rectHauteur)) {
      params01.coinInfGX= gx;
      params01.coinInfGY= gy;
      params01.coinInfDX= dx;
      params01.coinInfDY= dy;
      params01.rectLargeur= l;
      params01.rectHauteur= h;
      FudaaParamEventProxy.FUDAA_PARAM.fireParamStructModified(
        new FudaaParamEvent(
          this,
          0,
          VagResource.VAG01,
          params01,
          VagResource.VAG01 + ":rectangle"));
    }
    if ((params01.nbMaillesX != nbMaillesX_)
      || (params01.nbMaillesY != nbMaillesY_)) {
      params01.nbMaillesX= nbMaillesX_;
      params01.nbMaillesY= nbMaillesY_;
      FudaaParamEventProxy.FUDAA_PARAM.fireParamStructModified(
        new FudaaParamEvent(
          this,
          0,
          VagResource.VAG01,
          params01,
          VagResource.VAG01 + ":nbMailles"));
    }
    if (params01.flecheNordDir != nord_) {
      params01.flecheNordDir= nord_;
      FudaaParamEventProxy.FUDAA_PARAM.fireParamStructModified(
        new FudaaParamEvent(
          this,
          0,
          VagResource.VAG01,
          params01,
          VagResource.VAG01 + ":flecheNordDir"));
    }
    if (segArret_ != null) {
      X= segArret_.o_.x_;
      Y= segArret_.o_.y_;
      double sadx= (X - ox) * Math.cos(a) + (Y - oy) * Math.sin(a);
      double sady= (Y - oy) * Math.cos(a) - (X - ox) * Math.sin(a);
      X= segArret_.e_.x_;
      Y= segArret_.e_.y_;
      double safx= (X - ox) * Math.cos(a) + (Y - oy) * Math.sin(a);
      double safy= (Y - oy) * Math.cos(a) - (X - ox) * Math.sin(a);
      if ((sadx != params01.segArretDebutX)
        || (sady != params01.segArretDebutY)
        || (safx != params01.segArretFinX)
        || (safy != params01.segArretFinY)) {
        params01.segArretDebutX= sadx;
        params01.segArretDebutY= sady;
        params01.segArretFinX= safx;
        params01.segArretFinY= safy;
        FudaaParamEventProxy.FUDAA_PARAM.fireParamStructModified(
          new FudaaParamEvent(
            this,
            0,
            VagResource.VAG01,
            params01,
            VagResource.VAG01 + ":seg arret"));
      }
    }
    if (pointOrth_ != null) {
      X= pointOrth_.x_;
      Y= pointOrth_.y_;
      double pdox= (X - ox) * Math.cos(a) + (Y - oy) * Math.sin(a);
      double pdoy= (Y - oy) * Math.cos(a) - (X - ox) * Math.sin(a);
      if ((pdox != params04.premiereOrthDepartX)
        || (pdoy != params04.premiereOrthDepartY)) {
        params04.premiereOrthDepartX= pdox;
        params04.premiereOrthDepartY= pdoy;
        FudaaParamEventProxy.FUDAA_PARAM.fireParamStructModified(
          new FudaaParamEvent(
            this,
            0,
            VagResource.VAG04,
            params04,
            VagResource.VAG04 + ":depart orth"));
      }
    }
  }
  void updateParams03() {
    SParametres03 params03= (SParametres03)projet_.getParam(VagResource.VAG03);
    params03.vz= maillage_.getValeursVecteur();
    FudaaParamEventProxy.FUDAA_PARAM.fireParamStructModified(
      new FudaaParamEvent(
        this,
        0,
        VagResource.VAG03,
        params03,
        VagResource.VAG03 + ":bathy"));
    //cb.setParametres01((SParametres01)projet_.getParam(VagResource.VAG01));
    //cb.setParametres03((SParametres03)projet_.getParam(VagResource.VAG03));
  }
}
class VagGrilleParamEditor extends JDialog implements ActionListener {
  BuTextField[] tfX_;
  BuTextField[] tfY_;
  BuTextField tfMailleX_, tfMailleY_;
  BuButton btOk_, btCancel_;
  GrPolygone rectangle_;
  int mailleX_, mailleY_;
  int resp_;
  NumberFormat nf_;
  public VagGrilleParamEditor(
    JComponent parent,
    GrPolygone rect,
    int nbX,
    int nbY) {
    super(VagImplementation.FRAME, "Param�tres");
    setModal(true);
    setLocationRelativeTo(parent);
    nf_= NumberFormat.getInstance(Locale.US);
    nf_.setMaximumFractionDigits(3);
    nf_.setGroupingUsed(false);
    resp_= 0;
    rectangle_= rect;
    mailleX_= nbX;
    mailleY_= nbY;
    Container cp= this.getContentPane();
    cp.setLayout(new BorderLayout());
    JPanel pnMain= new JPanel();
    pnMain.setLayout(new BuGridLayout(2));
    int n= 0;
    pnMain.add(new BuLabel("Mailles X"), n++);
    pnMain.add(new BuLabel("Mailles Y"), n++);
    tfMailleX_= BuTextField.createIntegerField();
    tfMailleX_.setValue(new Integer(mailleX_));
    pnMain.add(tfMailleX_, n++);
    tfMailleY_= BuTextField.createIntegerField();
    tfMailleY_.setValue(new Integer(mailleY_));
    pnMain.add(tfMailleY_, n++);
    tfX_= new BuTextField[4];
    tfY_= new BuTextField[4];
    for (int i= 0; i < 4; i++) {
      int m= 0;
      JPanel pn= new JPanel();
      pn.setBorder(new TitledBorder("P" + (i + 1)));
      pn.setLayout(new BuGridLayout(2));
      pn.add(new BuLabel("X: "), m++);
      tfX_[i]= BuTextField.createDoubleField();
      tfX_[i].setColumns(10);
      tfX_[i].setDisplayFormat(nf_);
      pn.add(tfX_[i], m++);
      pn.add(new BuLabel("Y: "), m++);
      tfY_[i]= BuTextField.createDoubleField();
      tfY_[i].setColumns(10);
      tfY_[i].setDisplayFormat(nf_);
      pn.add(tfY_[i], m++);
      if ((rectangle_ != null)
        && (rectangle_.sommets_.nombre() >= (i + 1))
        && (rectangle_.sommets_.renvoie(i) != null)) {
        tfX_[i].setValue(new Double(rectangle_.sommets_.renvoie(i).x_));
        tfY_[i].setValue(new Double(rectangle_.sommets_.renvoie(i).y_));
      }
      pnMain.add(pn, n++);
    }
    JPanel pnOk= new JPanel();
    pnOk.setLayout(new FlowLayout(FlowLayout.CENTER));
    btCancel_= new BuButton("Annuler");
    btCancel_.addActionListener(this);
    pnOk.add(btCancel_);
    btOk_= new BuButton("Valider");
    btOk_.addActionListener(this);
    pnOk.add(btOk_);
    cp.add(BorderLayout.CENTER, pnMain);
    cp.add(BorderLayout.SOUTH, pnOk);
    this.pack();
  }
  public boolean isValidee() {
    return resp_ == 1;
  }
  public void actionPerformed(ActionEvent e) {
    Object src= e.getSource();
    if (src == btOk_) {
      mailleX_= ((Integer)tfMailleX_.getValue()).intValue();
      mailleY_= ((Integer)tfMailleY_.getValue()).intValue();
      if ((mailleX_ == 0) || (mailleY_ == 0)) {
        new BuDialogError(
          (BuCommonInterface)VagImplementation.FRAME,
          ((BuCommonInterface)VagImplementation.FRAME)
            .getInformationsSoftware(),
          "Le nombre de mailles est nul !")
          .activate();
        return;
      }
      rectangle_= new GrPolygone();
      for (int i= 0; i < 4; i++) {
        Double val= (Double)tfX_[i].getValue();
        double x= (val == null ? 0. : val.doubleValue());
        val= (Double)tfY_[i].getValue();
        double y= (val == null ? 0. : val.doubleValue());
        rectangle_.sommets_.ajoute(new GrPoint(x, y, 0.));
      }
      resp_= 1;
    } else if (src == btCancel_) {
      resp_= 0;
    }
    this.dispose();
  }
  public GrPolygone getRectangle() {
    return rectangle_;
  }
  public int getMailleX() {
    return mailleX_;
  }
  public int getMailleY() {
    return mailleY_;
  }
}
