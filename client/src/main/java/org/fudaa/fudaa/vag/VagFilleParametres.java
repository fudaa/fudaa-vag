/*
 * @creation     1998-10-05
 * @modification $Date: 2007-06-28 09:28:18 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.vag;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.StringTokenizer;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;

import org.fudaa.dodico.corba.vag.SParametres01;
import org.fudaa.dodico.corba.vag.SParametres02;
import org.fudaa.dodico.corba.vag.SParametres03;
import org.fudaa.dodico.corba.vag.SParametres04;

import org.fudaa.fudaa.commun.projet.FudaaParamEvent;
import org.fudaa.fudaa.commun.projet.FudaaParamEventProxy;
import org.fudaa.fudaa.commun.projet.FudaaParamListener;
import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.commun.projet.FudaaProjetEvent;
import org.fudaa.fudaa.commun.projet.FudaaProjetListener;

/**
 * Une fenetre fille pour entrer les parametres.
 * 
 * @version $Revision: 1.9 $ $Date: 2007-06-28 09:28:18 $ by $Author: deniger $
 * @author Axel von Arnim
 */
public class VagFilleParametres extends BuInternalFrame implements FudaaProjetListener, FudaaParamListener,
    ActionListener {
  FudaaProjet project_;
  JButton btValider_, btAnnuler_;
  // BuTextFields 01
  BuTextField tf_rect_largeur;
  BuTextField tf_rect_hauteur;
  BuTextField tf_nb_maillesX;
  BuTextField tf_nb_maillesY;
  BuTextField tf_seg_arret_debutX;
  BuTextField tf_seg_arret_debutY;
  BuTextField tf_seg_arret_finX;
  BuTextField tf_seg_arret_finY;
  BuTextField tf_fleche_nordDir;
  BuTextField tf_code_imp;
  BuTextField tf_pas_calcul;
  BuTextField tf_coin_infGX;
  BuTextField tf_coin_infGY;
  BuTextField tf_coin_infDX;
  BuTextField tf_coin_infDY;
  // JComponents 02
  JLabel lb_import02;
  JButton bt_import02;
  // JComponent 03
  JLabel lb_import03;
  JButton bt_import03;
  // BuTextFields 04
  BuTextField tf_periode;
  BuTextField tf_hauteur_mer;
  BuTextField tf_nb_orthogonales;
  BuTextField tf_angle_houle_horiz;
  BuTextField tf_espacement_orth;
  BuTextField tf_premiere_orth_departX;
  BuTextField tf_premiere_orth_departY;
  JComponent content_;
  BuCommonInterface appli_;
  NumberFormat nf_;

  public VagFilleParametres(BuCommonInterface _appli, FudaaProjet projet) {
    super("", false, true, false, true);
    appli_ = _appli;
    project_ = projet;
    FudaaParamEventProxy.FUDAA_PARAM.addFudaaParamListener(this);
    project_.addFudaaProjetListener(this);
    nf_ = NumberFormat.getInstance(Locale.US);
    nf_.setMaximumFractionDigits(3);
    nf_.setGroupingUsed(false);
    int n;
    // Parametres Vag 01
    JPanel pn01 = new JPanel();
    BuVerticalLayout lo01 = new BuVerticalLayout();
    pn01.setLayout(lo01);
    pn01.setBorder(new EmptyBorder(5, 5, 5, 5));
    n = 0;
    tf_rect_largeur = BuTextField.createDoubleField();
    tf_rect_largeur.setDisplayFormat(nf_);
    tf_rect_largeur.setName("GRILLE_RECT_LARGEUR");
    tf_rect_hauteur = BuTextField.createDoubleField();
    tf_rect_hauteur.setDisplayFormat(nf_);
    tf_rect_hauteur.setName("GRILLE_RECT_HAUTEUR");
    tf_nb_maillesX = BuTextField.createIntegerField();
    tf_nb_maillesX.setName("GRILLE_NB_MAILLESX");
    tf_nb_maillesY = BuTextField.createIntegerField();
    tf_nb_maillesY.setName("GRILLE_NB_MAILLESY");
    tf_seg_arret_debutX = BuTextField.createDoubleField();
    tf_seg_arret_debutX.setDisplayFormat(nf_);
    tf_seg_arret_debutX.setName("GRILLE_SEG_ARRET_DEBUTX");
    tf_seg_arret_debutY = BuTextField.createDoubleField();
    tf_seg_arret_debutY.setDisplayFormat(nf_);
    tf_seg_arret_debutY.setName("GRILLE_SEG_ARRET_DEBUTY");
    tf_seg_arret_finX = BuTextField.createDoubleField();
    tf_seg_arret_finX.setName("GRILLE_SEG_ARRET_FINX");
    tf_seg_arret_finX.setDisplayFormat(nf_);
    tf_seg_arret_finY = BuTextField.createDoubleField();
    tf_seg_arret_finY.setDisplayFormat(nf_);
    tf_seg_arret_finY.setName("GRILLE_SEG_ARRET_FINY");
    tf_fleche_nordDir = BuTextField.createDoubleField();
    tf_fleche_nordDir.setDisplayFormat(nf_);
    tf_fleche_nordDir.setName("GRILLE_FLECHE_NORD_DIR");
    tf_fleche_nordDir.setEditable(false);
    tf_code_imp = BuTextField.createIntegerField();
    tf_code_imp.setName("GRILLE_CODE_IMP");
    tf_pas_calcul = BuTextField.createIntegerField();
    tf_pas_calcul.setName("GRILLE_PAS_CALCUL");
    tf_coin_infGX = BuTextField.createDoubleField();
    tf_coin_infGX.setDisplayFormat(nf_);
    tf_coin_infGX.setName("GRILLE_COIN_INF_GX");
    tf_coin_infGY = BuTextField.createDoubleField();
    tf_coin_infGY.setDisplayFormat(nf_);
    tf_coin_infGY.setName("GRILLE_COIN_INF_GY");
    tf_coin_infDX = BuTextField.createDoubleField();
    tf_coin_infDX.setDisplayFormat(nf_);
    tf_coin_infDX.setName("GRILLE_COIN_INF_DX");
    tf_coin_infDY = BuTextField.createDoubleField();
    tf_coin_infDY.setDisplayFormat(nf_);
    tf_coin_infDY.setName("GRILLE_COIN_INF_DY");
    // les .setColumns();
    JPanel pnGrille = new JPanel();
    pnGrille.setBorder(new TitledBorder("Grille"));
    BuGridLayout loGri = new BuGridLayout();
    loGri.setColumns(3);
    loGri.setHgap(5);
    loGri.setVgap(5);
    pnGrille.setLayout(loGri);
    int ng = 0;
    tf_rect_largeur.setColumns(10);
    JLabel lbGri = new JLabel("Rectangle largeur:", SwingConstants.RIGHT);
    pnGrille.add(lbGri, ng++);
    pnGrille.add(tf_rect_largeur, ng++);
    pnGrille.add(new JLabel("(m)", SwingConstants.LEFT), ng++);
    pnGrille.add(new JLabel("Rectangle hauteur:", SwingConstants.RIGHT), ng++);
    pnGrille.add(tf_rect_hauteur, ng++);
    pnGrille.add(new JLabel("(m)", SwingConstants.LEFT), ng++);
    pnGrille.add(new JLabel("Nombre de mailles en X:", SwingConstants.RIGHT), ng++);
    pnGrille.add(tf_nb_maillesX, ng++);
    pnGrille.add(new JLabel("(entier)", SwingConstants.LEFT), ng++);
    pnGrille.add(new JLabel("Nombre de mailles en Y:", SwingConstants.RIGHT), ng++);
    pnGrille.add(tf_nb_maillesY, ng++);
    pnGrille.add(new JLabel("(entier)", SwingConstants.LEFT), ng++);
    pnGrille.add(new JLabel("X du coin inf�rieur gauche:", SwingConstants.RIGHT), ng++);
    pnGrille.add(tf_coin_infGX, ng++);
    pnGrille.add(new JLabel("(m)", SwingConstants.LEFT), ng++);
    pnGrille.add(new JLabel("Y du coin inf�rieur gauche:", SwingConstants.RIGHT), ng++);
    pnGrille.add(tf_coin_infGY, ng++);
    pnGrille.add(new JLabel("(m)", SwingConstants.LEFT), ng++);
    pnGrille.add(new JLabel("X du coin inf�rieur droit:", SwingConstants.RIGHT), ng++);
    pnGrille.add(tf_coin_infDX, ng++);
    pnGrille.add(new JLabel("(m)", SwingConstants.LEFT), ng++);
    pnGrille.add(new JLabel("Y du coin inf�rieur droit:", SwingConstants.RIGHT), ng++);
    pnGrille.add(tf_coin_infDY, ng++);
    pnGrille.add(new JLabel("(m)", SwingConstants.LEFT), ng++);
    pn01.add(pnGrille, n++);
    // Parametres Vag 02
    JPanel pn02 = new JPanel();
    BuGridLayout lo02 = new BuGridLayout();
    lo02.setColumns(1);
    lo02.setHfilled(true);
    lo02.setHgap(5);
    lo02.setVgap(5);
    pn02.setLayout(lo02);
    pn02.setBorder(new EmptyBorder(5, 5, 5, 5));
    n = 0;
    lb_import02 = new JLabel("Il n'y a pas de param�tres Vag 02");
    bt_import02 = new JButton("Importer");
    bt_import02.setActionCommand("IMPORTVAG" + VagResource.VAG02);
    bt_import02.addActionListener(_appli);
    pn02.add(lb_import02, n++);
    pn02.add(bt_import02, n++);
    // Parametres Vag 03
    JPanel pn03 = new JPanel();
    BuGridLayout lo03 = new BuGridLayout();
    lo03.setColumns(1);
    lo03.setHfilled(true);
    lo03.setHgap(5);
    lo03.setVgap(5);
    pn03.setLayout(lo03);
    pn03.setBorder(new EmptyBorder(5, 5, 5, 5));
    n = 0;
    lb_import03 = new JLabel("Il n'y a pas de param�tres Vag 03");
    bt_import03 = new JButton("Importer");
    bt_import03.setActionCommand("IMPORTVAG" + VagResource.VAG03);
    bt_import03.addActionListener(_appli);
    pn03.add(lb_import03, n++);
    pn03.add(bt_import03, n++);
    // Parametres Vag 04
    JPanel pn04 = new JPanel();
    BuVerticalLayout lo04 = new BuVerticalLayout();
    pn04.setLayout(lo04);
    pn04.setBorder(new EmptyBorder(5, 5, 5, 5));
    n = 0;
    JPanel pnCalcul = new JPanel();
    pnCalcul.setBorder(new TitledBorder("Calcul"));
    BuGridLayout loCal = new BuGridLayout();
    loCal.setColumns(3);
    loCal.setHgap(5);
    loCal.setVgap(5);
    pnCalcul.setLayout(loCal);
    JPanel pnHydro = new JPanel();
    pnHydro.setBorder(new TitledBorder("Hydraulique"));
    BuGridLayout loHyd = new BuGridLayout();
    loHyd.setColumns(3);
    loHyd.setHgap(5);
    loHyd.setVgap(5);
    pnHydro.setLayout(loHyd);
    int nc = 0, nh = 0;
    tf_periode = BuTextField.createDoubleField();
    tf_periode.setDisplayFormat(nf_);
    tf_periode.setName("HOULE_PERIODE");
    tf_hauteur_mer = BuTextField.createDoubleField();
    tf_hauteur_mer.setDisplayFormat(nf_);
    tf_hauteur_mer.setName("HOULE_HAUTEUR_MER");
    tf_nb_orthogonales = BuTextField.createIntegerField();
    tf_nb_orthogonales.setName("HOULE_NB_ORTHOGNALES");
    tf_angle_houle_horiz = BuTextField.createDoubleField();
    tf_angle_houle_horiz.setDisplayFormat(nf_);
    tf_angle_houle_horiz.setName("HOULE_ANGLE_HOULE_HORIZ");
    tf_espacement_orth = BuTextField.createDoubleField();
    tf_espacement_orth.setDisplayFormat(nf_);
    tf_espacement_orth.setName("HOULE_ESPACEMENT_ORTH");
    tf_premiere_orth_departX = BuTextField.createDoubleField();
    tf_premiere_orth_departX.setDisplayFormat(nf_);
    tf_premiere_orth_departX.setName("HOULE_PREMIERE_ORTH_DEPART_X");
    tf_premiere_orth_departY = BuTextField.createDoubleField();
    tf_premiere_orth_departY.setDisplayFormat(nf_);
    tf_premiere_orth_departY.setName("HOULE_PREMIERE_ORTH_DEPART_Y");
    tf_periode.setColumns(10);
    JLabel lbHou = new JLabel("P�riode de la houle:", SwingConstants.RIGHT);
    pnHydro.add(lbHou, nh++);
    pnHydro.add(tf_periode, nh++);
    pnHydro.add(new JLabel("(secondes)", SwingConstants.LEFT), nh++);
    pnHydro.add(new JLabel("Hauteur de la mer:", SwingConstants.RIGHT), nh++);
    pnHydro.add(tf_hauteur_mer, nh++);
    pnHydro.add(new JLabel("(m)", SwingConstants.LEFT), nh++);
    pnHydro.add(new JLabel("Nombre d'orthogonales:", SwingConstants.RIGHT), nh++);
    pnHydro.add(tf_nb_orthogonales, nh++);
    pnHydro.add(new JLabel("(entier)", SwingConstants.LEFT), nh++);
    pnHydro.add(new JLabel("Espacement des orthogonales:", SwingConstants.RIGHT), nh++);
    pnHydro.add(tf_espacement_orth, nh++);
    pnHydro.add(new JLabel("(m)", SwingConstants.LEFT), nh++);
    pnHydro.add(new JLabel("Angle houle/Nord:", SwingConstants.RIGHT), nh++);
    pnHydro.add(tf_angle_houle_horiz, nh++);
    pnHydro.add(new JLabel("(degr�s)", SwingConstants.LEFT), nh++);
    pnCalcul.add(new JLabel("Direction de la fl�che Nord:", SwingConstants.RIGHT), nc++);
    pnCalcul.add(tf_fleche_nordDir, nc++);
    pnCalcul.add(new JLabel("(degr�s)", SwingConstants.LEFT), nc++);
    pnCalcul.add(new JLabel("Pas d'impression:", SwingConstants.RIGHT), nc++);
    pnCalcul.add(tf_code_imp, nc++);
    pnCalcul.add(new JLabel("(entier)", SwingConstants.LEFT), nc++);
    pnCalcul.add(new JLabel("Pas de calcul:", SwingConstants.RIGHT), nc++);
    pnCalcul.add(tf_pas_calcul, nc++);
    pnCalcul.add(new JLabel("(entier)", SwingConstants.LEFT), nc++);
    JLabel lbOrt = new JLabel("X de d�part de la 1�re orthogonale:", SwingConstants.RIGHT);
    lbHou.setPreferredSize(lbOrt.getPreferredSize());
    tf_premiere_orth_departX.setColumns(10);
    pnCalcul.add(lbOrt, nc++);
    pnCalcul.add(tf_premiere_orth_departX, nc++);
    pnCalcul.add(new JLabel("(m)", SwingConstants.LEFT), nc++);
    pnCalcul.add(new JLabel("Y de d�part de la 1�re orthogonale:", SwingConstants.RIGHT), nc++);
    pnCalcul.add(tf_premiere_orth_departY, nc++);
    pnCalcul.add(new JLabel("(m)", SwingConstants.LEFT), nc++);
    pnCalcul.add(new JLabel("X de d�but du segment d'arret:", SwingConstants.RIGHT), nc++);
    pnCalcul.add(tf_seg_arret_debutX, nc++);
    pnCalcul.add(new JLabel("(m)", SwingConstants.LEFT), nc++);
    pnCalcul.add(new JLabel("Y de d�but du segment d'arret:", SwingConstants.RIGHT), nc++);
    pnCalcul.add(tf_seg_arret_debutY, nc++);
    pnCalcul.add(new JLabel("(m)", SwingConstants.LEFT), nc++);
    pnCalcul.add(new JLabel("X de fin du segment d'arret:", SwingConstants.RIGHT), nc++);
    pnCalcul.add(tf_seg_arret_finX, nc++);
    pnCalcul.add(new JLabel("(m)", SwingConstants.LEFT), nc++);
    pnCalcul.add(new JLabel("Y de fin du segment d'arret:", SwingConstants.RIGHT), nc++);
    pnCalcul.add(tf_seg_arret_finY, nc++);
    pnCalcul.add(new JLabel("(m)", SwingConstants.LEFT), nc++);
    pn04.add(pnHydro, n++);
    pn04.add(pnCalcul, n++);
    JTabbedPane tpMain = new JTabbedPane();
    JPanel cd01 = new JPanel();
    cd01.setLayout(new BorderLayout());
    cd01.add("North", pn01);
    tpMain.addTab("Grille", null, cd01, "Grille du maillage");
    JPanel cd02 = new JPanel();
    cd02.setLayout(new BorderLayout());
    bt_import02.setName("COTES");
    cd02.add("North", pn02);
    tpMain.addTab("Cotes", null, cd02, "D�finition des cotes");
    JPanel cd03 = new JPanel();
    cd03.setLayout(new BorderLayout());
    bt_import03.setName("BATHY");
    cd03.add("North", pn03);
    tpMain.addTab("Bathy", null, cd03, "Bathym�trie");
    JPanel cd04 = new JPanel();
    cd04.setLayout(new BorderLayout());
    cd04.add("North", pn04);
    tpMain.addTab("Houle", null, cd04, "Param�tres de la houle");
    JPanel pnValider = new JPanel();
    btValider_ = new BuButton("Valider");
    btValider_.addActionListener(this);
    pnValider.add(btValider_);
    btAnnuler_ = new BuButton("Annuler");
    btAnnuler_.addActionListener(this);
    pnValider.add(btAnnuler_);
    content_ = (JComponent) getContentPane();
    content_.setLayout(new BorderLayout());
    content_.setBorder(new EmptyBorder(5, 5, 5, 5));
    content_.add(BorderLayout.CENTER, tpMain);
    content_.add(BorderLayout.SOUTH, pnValider);
    updatePanels();
    grilleDisabled();
    setTitle("Param�tres de calcul");
    setFrameIcon(BuResource.BU.getIcon("parametre"));
    setLocation(40, 40);
    pack();
  }

  public void delete() {
    FudaaParamEventProxy.FUDAA_PARAM.removeFudaaParamListener(this);
    if (project_ != null) project_.removeFudaaProjetListener(this);
    project_ = null;
  }

  // Methodes publiques
  public void setProjet(FudaaProjet project) {
    project_ = project;
    updatePanels();
  }

  public void actionPerformed(ActionEvent e) {
    Object src = e.getSource();
    if (src == btValider_) valider();
    else
      try {
        setClosed(true);
        setSelected(false);
      } catch (PropertyVetoException ex) {}
  }

  public void valider() {
    try {
      getValeurs01();
      getValeurs04();
    } catch (IllegalArgumentException e) {
      return;
    }
    try {
      setClosed(true);
      setSelected(false);
    } catch (PropertyVetoException e) {}
  }

  public void grilleDisabled() {
    tf_rect_largeur.setEditable(false);
    tf_rect_hauteur.setEditable(false);
    tf_nb_maillesX.setEditable(false);
    tf_nb_maillesY.setEditable(false);
    tf_coin_infGX.setEditable(false);
    tf_coin_infGY.setEditable(false);
    tf_coin_infDX.setEditable(false);
    tf_coin_infDY.setEditable(false);
  }

  // LidoParamListener
  public void paramStructCreated(FudaaParamEvent e) {
    if (e.getSource() != this) updatePanels();
  }

  public void paramStructDeleted(FudaaParamEvent e) {
    if (e.getSource() != this) updatePanels();
  }

  public void paramStructModified(FudaaParamEvent e) {
    if (e.getSource() != this) updatePanels();
  }

  // fudaaprojet listener
  public void dataChanged(FudaaProjetEvent e) {
    switch (e.getID()) {
    case FudaaProjetEvent.PARAM_ADDED:
    case FudaaProjetEvent.PARAM_IMPORTED: {
      if (e.getSource() != this) updatePanels();
      break;
    }
    }
  }

  public void statusChanged(FudaaProjetEvent e) {
    switch (e.getID()) {
    case FudaaProjetEvent.PROJECT_OPENED:
    case FudaaProjetEvent.PROJECT_CLOSED: {
      if (e.getSource() != this) updatePanels();
      break;
    }
    }
  }

  private void updatePanels() {
    System.err.println("VagFilleParametres: update");
    SParametres02 params02 = (SParametres02) project_.getParam(VagResource.VAG02);
    SParametres03 params03 = (SParametres03) project_.getParam(VagResource.VAG03);
    setValeurs();
    if ((params02 == null) || (params02.nbLignes == 0)) {
      lb_import02.setText("Il n'y a pas de parametres de cotes");
      bt_import02.setEnabled(true);
    } else {
      lb_import02.setText("Les parametres de cotes sont charg�s");
      bt_import02.setEnabled(false);
    }
    if ((params03 == null) || (params03.vz == null) || (params03.vz.length == 0)) {
      lb_import03.setText("Il n'y a pas de parametres de bathy");
      bt_import03.setEnabled(true);
    } else {
      lb_import03.setText("Les parametres de bathy sont charg�s");
      bt_import03.setEnabled(false);
    }
    lb_import02.revalidate();
    lb_import03.revalidate();
  }

  private void setValeurs() {
    SParametres01 _paramsVag01 = (SParametres01) project_.getParam(VagResource.VAG01);
    SParametres04 _paramsVag04 = (SParametres04) project_.getParam(VagResource.VAG04);
    if (_paramsVag01 != null) {
      tf_rect_largeur.setValue(new Double(Math.round(_paramsVag01.rectLargeur * VagResource.PRECISION)
          / VagResource.PRECISION));
      tf_rect_hauteur.setValue(new Double(Math.round(_paramsVag01.rectHauteur * VagResource.PRECISION)
          / VagResource.PRECISION));
      tf_nb_maillesX.setValue(new Integer(_paramsVag01.nbMaillesX));
      tf_nb_maillesY.setValue(new Integer(_paramsVag01.nbMaillesY));
      tf_seg_arret_debutX.setValue(new Double(Math.round(_paramsVag01.segArretDebutX * VagResource.PRECISION)
          / VagResource.PRECISION));
      tf_seg_arret_debutY.setValue(new Double(Math.round(_paramsVag01.segArretDebutY * VagResource.PRECISION)
          / VagResource.PRECISION));
      tf_seg_arret_finX.setValue(new Double(Math.round(_paramsVag01.segArretFinX * VagResource.PRECISION)
          / VagResource.PRECISION));
      tf_seg_arret_finY.setValue(new Double(Math.round(_paramsVag01.segArretFinY * VagResource.PRECISION)
          / VagResource.PRECISION));
      tf_fleche_nordDir.setValue(new Double(Math.round(_paramsVag01.flecheNordDir * VagResource.PRECISION)
          / VagResource.PRECISION));
      tf_code_imp.setValue(new Integer(_paramsVag01.codeImp));
      tf_pas_calcul.setValue(new Integer(_paramsVag01.pasCalcul));
      tf_coin_infGX.setValue(new Double(Math.round(_paramsVag01.coinInfGX * VagResource.PRECISION)
          / VagResource.PRECISION));
      tf_coin_infGY.setValue(new Double(Math.round(_paramsVag01.coinInfGY * VagResource.PRECISION)
          / VagResource.PRECISION));
      tf_coin_infDX.setValue(new Double(Math.round(_paramsVag01.coinInfDX * VagResource.PRECISION)
          / VagResource.PRECISION));
      tf_coin_infDY.setValue(new Double(Math.round(_paramsVag01.coinInfDY * VagResource.PRECISION)
          / VagResource.PRECISION));
    }
    if (_paramsVag04 != null) {
      tf_periode.setValue(new Double(Math.round(_paramsVag04.periode * VagResource.PRECISION) / VagResource.PRECISION));
      tf_hauteur_mer.setValue(new Double(Math.round(_paramsVag04.hauteurMer * VagResource.PRECISION)
          / VagResource.PRECISION));
      tf_nb_orthogonales.setValue(new Integer(_paramsVag04.nbOrthogonales));
      tf_angle_houle_horiz.setValue(new Double(Math.round((_paramsVag04.angleHouleHoriz - 90.) * VagResource.PRECISION)
          / VagResource.PRECISION));
      tf_espacement_orth.setValue(new Double(Math.round(_paramsVag04.espacementOrth * VagResource.PRECISION)
          / VagResource.PRECISION));
      tf_premiere_orth_departX.setValue(new Double(Math.round(_paramsVag04.premiereOrthDepartX * VagResource.PRECISION)
          / VagResource.PRECISION));
      tf_premiere_orth_departY.setValue(new Double(Math.round(_paramsVag04.premiereOrthDepartY * VagResource.PRECISION)
          / VagResource.PRECISION));
    }
  }

  private void getValeurs01() throws IllegalArgumentException {
    SParametres01 paramsVag01 = (SParametres01) project_.getParam(VagResource.VAG01);
    String changed = "";
    double tmpD = 0.;
    int tmpI = 0;
    try {
      tmpD = paramsVag01.rectLargeur;
      paramsVag01.rectLargeur = ((Double) tf_rect_largeur.getValue()).doubleValue();
      if (paramsVag01.rectLargeur != tmpD) changed += "rectLargeur|";
      tmpD = paramsVag01.rectHauteur;
      paramsVag01.rectHauteur = ((Double) tf_rect_hauteur.getValue()).doubleValue();
      if (paramsVag01.rectHauteur != tmpD) changed += "rectHauteur|";
      tmpI = paramsVag01.nbMaillesX;
      paramsVag01.nbMaillesX = ((Integer) tf_nb_maillesX.getValue()).intValue();
      if (paramsVag01.nbMaillesX != tmpI) changed += "nbMaillesX|";
      tmpI = paramsVag01.nbMaillesY;
      paramsVag01.nbMaillesY = ((Integer) tf_nb_maillesY.getValue()).intValue();
      if (paramsVag01.nbMaillesY != tmpI) changed += "nbMaillesY|";
      tmpD = paramsVag01.segArretDebutX;
      paramsVag01.segArretDebutX = ((Double) tf_seg_arret_debutX.getValue()).doubleValue();
      if (paramsVag01.segArretDebutX != tmpD) changed += "segArretDebutX|";
      tmpD = paramsVag01.segArretDebutY;
      paramsVag01.segArretDebutY = ((Double) tf_seg_arret_debutY.getValue()).doubleValue();
      if (paramsVag01.segArretDebutY != tmpD) changed += "segArretDebutY|";
      tmpD = paramsVag01.segArretFinX;
      paramsVag01.segArretFinX = ((Double) tf_seg_arret_finX.getValue()).doubleValue();
      if (paramsVag01.segArretFinX != tmpD) changed += "segArretFinX|";
      tmpD = paramsVag01.segArretFinY;
      paramsVag01.segArretFinY = ((Double) tf_seg_arret_finY.getValue()).doubleValue();
      if (paramsVag01.segArretFinY != tmpD) changed += "segArretFinY|";
      tmpD = paramsVag01.flecheNordDir;
      paramsVag01.flecheNordDir = ((Double) tf_fleche_nordDir.getValue()).doubleValue();
      if (paramsVag01.flecheNordDir != tmpD) changed += "flecheNordDir|";
      tmpI = paramsVag01.codeImp;
      paramsVag01.codeImp = ((Integer) tf_code_imp.getValue()).intValue();
      if (paramsVag01.codeImp != tmpI) changed += "codeImp|";
      tmpI = paramsVag01.pasCalcul;
      paramsVag01.pasCalcul = ((Integer) tf_pas_calcul.getValue()).intValue();
      if (paramsVag01.pasCalcul != tmpI) changed += "pasCalcul|";
      tmpD = paramsVag01.coinInfGX;
      paramsVag01.coinInfGX = ((Double) tf_coin_infGX.getValue()).doubleValue();
      if (paramsVag01.coinInfGX != tmpD) changed += "coinInfGX|";
      tmpD = paramsVag01.coinInfGY;
      paramsVag01.coinInfGY = ((Double) tf_coin_infGY.getValue()).doubleValue();
      if (paramsVag01.coinInfGY != tmpD) changed += "coinInfGY|";
      tmpD = paramsVag01.coinInfDX;
      paramsVag01.coinInfDX = ((Double) tf_coin_infDX.getValue()).doubleValue();
      if (paramsVag01.coinInfDX != tmpD) changed += "coinInfDX|";
      tmpD = paramsVag01.coinInfDY;
      paramsVag01.coinInfDY = ((Double) tf_coin_infDY.getValue()).doubleValue();
      if (paramsVag01.coinInfDY != tmpD) changed += "coinInfDY|";
    } catch (NumberFormatException e) {
      paramsVag01 = null;
      new BuDialogError(appli_, appli_.getInformationsSoftware(),
          "parametres 01 => \nun champ est vide ou de format incorrect ! ").activate();
      System.err.println("Parametres01 : NumberFormatException");
      throw new IllegalArgumentException("parametres 01 incorrects");
    }
    StringTokenizer tok = new StringTokenizer(changed, "|");
    while (tok.hasMoreTokens()) {
      FudaaParamEventProxy.FUDAA_PARAM.fireParamStructModified(new FudaaParamEvent(this, 0, VagResource.VAG01,
          paramsVag01, VagResource.VAG01 + ":" + tok.nextToken()));
    }
  }

  private void getValeurs04() throws IllegalArgumentException {
    SParametres01 paramsVag01 = (SParametres01) project_.getParam(VagResource.VAG01);
    SParametres04 paramsVag04 = (SParametres04) project_.getParam(VagResource.VAG04);
    String changed = "";
    double tmpD = 0.;
    int tmpI = 0;
    try {
      tmpD = paramsVag04.periode;
      paramsVag04.periode = ((Double) tf_periode.getValue()).doubleValue();
      if (paramsVag04.periode != tmpD) changed += "periode|";
      tmpD = paramsVag04.hauteurMer;
      paramsVag04.hauteurMer = ((Double) tf_hauteur_mer.getValue()).doubleValue();
      if (paramsVag04.hauteurMer != tmpD) changed += "hauteurMer|";
      tmpI = paramsVag04.nbOrthogonales;
      paramsVag04.nbOrthogonales = ((Integer) tf_nb_orthogonales.getValue()).intValue();
      if (paramsVag04.nbOrthogonales != tmpI) changed += "nbOrthogonales|";
      tmpD = paramsVag04.angleHouleHoriz;
      paramsVag04.angleHouleHoriz = ((Double) tf_angle_houle_horiz.getValue()).doubleValue()
          + paramsVag01.flecheNordDir;
      if (paramsVag04.angleHouleHoriz != tmpD) changed += "angleHouleHoriz|";
      tmpD = paramsVag04.espacementOrth;
      paramsVag04.espacementOrth = ((Double) tf_espacement_orth.getValue()).doubleValue();
      if (paramsVag04.espacementOrth != tmpD) changed += "espacementOrth|";
      tmpD = paramsVag04.premiereOrthDepartX;
      paramsVag04.premiereOrthDepartX = ((Double) tf_premiere_orth_departX.getValue()).doubleValue();
      if (paramsVag04.premiereOrthDepartX != tmpD) changed += "premiereOrthDepartX|";
      tmpD = paramsVag04.premiereOrthDepartY;
      paramsVag04.premiereOrthDepartY = ((Double) tf_premiere_orth_departY.getValue()).doubleValue();
      if (paramsVag04.premiereOrthDepartY != tmpD) changed += "premiereOrthDepartY|";
    } catch (NumberFormatException e) {
      paramsVag04 = null;
      new BuDialogError(appli_, appli_.getInformationsSoftware(),
          "parametres 04 => \nun champ est vide ou de format incorrect ! ").activate();
      System.err.println("Parametres04 : NumberFormatException");
      throw new IllegalArgumentException("parametres 01 incorrects");
    }
    StringTokenizer tok = new StringTokenizer(changed, "|");
    while (tok.hasMoreTokens()) {
      FudaaParamEventProxy.FUDAA_PARAM.fireParamStructModified(new FudaaParamEvent(this, 0, VagResource.VAG04,
          paramsVag04, VagResource.VAG04 + ":" + tok.nextToken()));
    }
  }
}
