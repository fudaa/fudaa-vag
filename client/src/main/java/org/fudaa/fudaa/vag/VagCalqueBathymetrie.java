/*
 * @creation     1999-02-16
 * @modification $Date: 2007-06-28 09:28:18 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.vag;
import org.fudaa.dodico.corba.vag.SParametres01;
import org.fudaa.dodico.corba.vag.SParametres03;

import org.fudaa.ebli.calque.BCalqueGrilleReguliere;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolygone;
/**
 * Un calque pour la bathymetrie.
 *
 * @version      $Revision: 1.7 $ $Date: 2007-06-28 09:28:18 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class VagCalqueBathymetrie extends BCalqueGrilleReguliere {
  // donnees membres publiques
  // donnees membres privees
  // Constructeur
  public VagCalqueBathymetrie() {
    super();
    params01_= null;
    params03_= null;
    setIsosurfaces(true);
  }
  public void reinitialise() {
    params01_= null;
    params03_= null;
    super.reinitialise();
  }
  /*
    protected void construitLegende()
    {
      BCalqueLegende leg=getLegende();
      if( leg==null ) return;
      leg.reinitialise(this);
      leg.ajoute(this, new BuColorIcon(java.awt.Color.blue), "Extrémités");
      leg.ajoute(this, new BuColorIcon(java.awt.Color.red), "Biefs");
      leg.ajoute(this, new BuColorIcon(java.awt.Color.magenta), "Noeuds");
      leg.ajoute(this, new BuColorIcon(java.awt.Color.green), "Profils");
    }
  */
  /**********************************************/
  // PROPRIETES INTERNES
  /**********************************************/
  // PB: 2 PARAMETRES ICI: 03 et 01 => PAS GENERIQUE % FUTUR EVENEMENT PARAM
  // Propriete parametres01
  private SParametres01 params01_;
  public SParametres01 getParametres01() {
    return params01_;
  }
  public void setParametres01(SParametres01 _params) {
    if (_params == null)
      return;
    SParametres01 vp= params01_;
    params01_= _params;
    super.reinitialise();
    // PB: SUPER FOIREUX, CETTE INTER-DEPENDANCE DES PROPRIETES!!
    if (params03_ != null)
      construitPoints();
    firePropertyChange("parametres01", vp, params01_);
  }
  // Propriete parametres03
  private SParametres03 params03_;
  public SParametres03 getParametres03() {
    return params03_;
  }
  public void setParametres03(SParametres03 _params) {
    if (_params == null)
      return;
    SParametres03 vp= params03_;
    params03_= _params;
    super.reinitialise();
    // PB: SUPER FOIREUX, CETTE INTER-DEPENDANCE DES PROPRIETES!!
    if (params01_ != null)
      construitPoints();
    firePropertyChange("parametres03", vp, params03_);
  }
  // Methodes privees
  private void construitPoints() {
    double[][] vvz=
      new double[params01_.nbMaillesX + 1][params01_.nbMaillesY + 1];
    for (int i= 0; i < vvz.length; i++) {
      for (int j= 0; j < vvz[i].length; j++) {
        vvz[i][j]= -params03_.vz[(vvz[i].length - j - 1) * vvz.length + i];
      }
    }
    GrPolygone rect= new GrPolygone();
    //    rect.sommets.ajoute(new GrPoint(params01_.coinInfGX, params01_.coinInfGY+params01_.rectHauteur, 0.));
    //    rect.sommets.ajoute(new GrPoint(params01_.coinInfGX+params01_.rectLargeur, params01_.coinInfGY+params01_.rectHauteur, 0.));
    //    rect.sommets.ajoute(new GrPoint(params01_.coinInfGX+params01_.rectLargeur, params01_.coinInfGY, 0.));
    //    rect.sommets.ajoute(new GrPoint(params01_.coinInfGX, params01_.coinInfGY, 0.));
    rect.sommets_.ajoute(new GrPoint(0., params01_.rectHauteur, 0.));
    rect.sommets_.ajoute(
      new GrPoint(params01_.rectLargeur, params01_.rectHauteur, 0.));
    rect.sommets_.ajoute(new GrPoint(params01_.rectLargeur, 0., 0.));
    rect.sommets_.ajoute(new GrPoint(0., 0., 0.));
    setRectangle(rect);
    setValeurs(vvz);
  }
}
