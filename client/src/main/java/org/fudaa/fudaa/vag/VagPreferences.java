/*
 * @file         VagPreferences.java
 * @creation     1999-01-05
 * @modification $Date: 2005-02-01 16:38:12 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.vag;
import com.memoire.bu.BuPreferences;
/**
 * Preferences pour Vag.
 *
 * @version      $Revision: 1.5 $ $Date: 2005-02-01 16:38:12 $ by $Author: clavreul $
 * @author       Axel von Arnim 
 */
public final class VagPreferences extends BuPreferences {
  
    public final static VagPreferences VAG= new VagPreferences();
 private VagPreferences() {
    
 }
 
  public void applyOn(Object _o) {
    if (!(_o instanceof VagImplementation))
      throw new RuntimeException("" + _o + " is not a VagImplementation.");
    VagImplementation _appli= (VagImplementation)_o;
    VagFilleCalques fc= _appli.getFilleCalques();
    if (fc != null)
      fc.setInteractif(getStringProperty("vag.calques.interactif"));
  }
}
