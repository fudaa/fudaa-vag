/*
 * @creation     1999-02-23
 * @modification $Date: 2007-06-28 09:28:18 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.vag;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;

import javax.swing.JComponent;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuInformationsDocument;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuToggleButton;

import org.fudaa.ebli.graphe.BGraphe;
import org.fudaa.ebli.graphe.BGraphePersonnaliseur;
import org.fudaa.ebli.graphe.EbliFilleGraphe;
import org.fudaa.ebli.graphe.Graphe;
/**
 * Une fenetre fille pour les graphes.
 *
 * @version      $Revision: 1.7 $ $Date: 2007-06-28 09:28:18 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class VagFilleGraphe
  extends EbliFilleGraphe
  implements ActionListener, PropertyChangeListener {
  //private BGraphe graphe_;
  private BuToggleButton btPerso_;
  JInternalFrame popup_;
  public VagFilleGraphe(BuCommonInterface _app, BuInformationsDocument _id) {
    super("Graphe", true, false, true, true, _app, _id);
    addInternalFrameListener(new InternalFrameAdapter() {
      public void internalFrameClosing(InternalFrameEvent e) {
        if (popup_.isVisible())
          popup_.setVisible(false);
        JDesktopPane pn= getDesktopPane();
        if (pn != null)
          pn.remove(popup_);
      }
    });
    btPerso_= new BuToggleButton(BuResource.BU.getIcon("configurer"), "Graphe");
    btPerso_.setToolTipText("Paramètres du graphe");
    btPerso_.addActionListener(this);
    popup_= new JInternalFrame("", false, false, false, false);
    popup_.setOpaque(true);
    setBackground(Color.white);
    graphe_= new BGraphe();
    setPreferredSize(new Dimension(640, 480));
    getContentPane().setLayout(new BorderLayout());
    getContentPane().add(graphe_, BorderLayout.CENTER);
  }
  public BGraphe getGraphe() {
    return graphe_;
  }
  public void setGraphe(BGraphe _graphe) {
    getContentPane().removeAll();
    graphe_= _graphe;
    getContentPane().add(graphe_, BorderLayout.CENTER);
  }
  public String[] getEnabledActions() {
    String[] r= new String[] { "IMPRIMER", "PREVISUALISER", "MISEENPAGE" };
    return r;
  }
  public JComponent[] getSpecificTools() {
    JComponent[] r= new JComponent[1];
    r[0]= btPerso_;
    return r;
  }
  public void actionPerformed(ActionEvent e) {
    Object src= e.getSource();
    if (src == btPerso_) {
      Graphe g= graphe_.getGraphe();
      if (g == null)
        return;
      if (btPerso_.isSelected()) {
        BGraphePersonnaliseur p= new BGraphePersonnaliseur(g);
        p.addPropertyChangeListener(this);
        p.setOpaque(true);
        popup_.getContentPane().removeAll();
        popup_.getContentPane().add(p);
        popup_.pack();
        popup_.setVisible(true);
        getDesktopPane().add(popup_);
        try {
          popup_.setSelected(true);
        } catch (PropertyVetoException pve) {}
      } else {
        popup_.setVisible(false);
        getDesktopPane().remove(popup_);
      }
    }
  }
  public void propertyChange(PropertyChangeEvent e) {
    Object src= e.getSource();
    if (src instanceof BGraphePersonnaliseur) {
      graphe_.fullRepaint();
    }
  }
  /*   public void print(PrintJob _job,Graphics _g)
    {
      BuPrinter.INFO_DOC     =new BuInformationsDocument();
      BuPrinter.INFO_DOC.name=getTitle();
      BuPrinter.INFO_DOC.logo=null;//BuResource.BU.getIcon("tableau",24);
      BuPrinter.printComponent(_job,_g,graphe_);
    }


    public int print(Graphics _g,PageFormat _format, int _page)
    {
      if(_page!=0) return Printable.NO_SUCH_PAGE;
      EbliPrinter.printBGraphe(_g,_format,graphe_);
      return Printable.PAGE_EXISTS;
    }

    public int getNumberOfPages()
    {
      return 1;
    }*/
}
