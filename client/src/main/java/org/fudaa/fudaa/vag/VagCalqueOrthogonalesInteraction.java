/*
 * @creation     1998-10-15
 * @modification $Date: 2007-06-28 09:28:18 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.vag;
import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Vector;

import javax.swing.JInternalFrame;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import org.fudaa.dodico.corba.vag.SResultatPasCalcul05;
import org.fudaa.dodico.corba.vag.SResultatPasInterOrth;
import org.fudaa.dodico.corba.vag.SResultatUnitaire05;
import org.fudaa.dodico.corba.vag.SResultatUnitaireInterOrth;
import org.fudaa.dodico.corba.vag.SResultats05;
import org.fudaa.dodico.corba.vag.SResultatsInterOrth;

import org.fudaa.ebli.calque.BCalqueInteraction;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.graphe.Axe;
import org.fudaa.ebli.graphe.BGraphe;
import org.fudaa.ebli.graphe.CourbeDefault;
import org.fudaa.ebli.graphe.Graphe;
import org.fudaa.ebli.graphe.Marges;
import org.fudaa.ebli.graphe.Valeur;

/**
 * Un calque d'interaction pour les resultats de Vag.
 *
 * @version      $Revision: 1.11 $ $Date: 2007-06-28 09:28:18 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class VagCalqueOrthogonalesInteraction
  extends BCalqueInteraction
  implements MouseListener {
  // donnees membres privees
  static int PAS_CALCUL= 1;
  static int ORTHOGONALE= 2;
  private static String ANGLE= "Angle";
  private static String LONGUEUR_ONDE= "Longueur d'onde";
  private static String HAUTEUR_EAU= "Hauteur d'eau";
  private static String HAUTEUR_HOULE= "Hauteur de houle";
  static String TABLEAU_ORTH= "Tableau Orth";
  static String TABLEAU_IORTH= "Tableau Inter";
  VagPopupMessage popup_;
  boolean isPopupShown;
  int xsel, ysel;
  SelectTask stask;
  VagCalqueOrthogonales calque_;
  NumberFormat nf_, nf2_, nf3_;
  BGraphe graphe_;
  VagTableauResultats tableau_;
  SResultatPasCalcul05 selectedPas;
  SResultatUnitaire05 selectedOrth;
  SResultatUnitaireInterOrth selectedIOrth;
  // Constructeur
  public VagCalqueOrthogonalesInteraction(
    VagCalqueOrthogonales _calque,
    BGraphe _graphe,
    VagTableauResultats _tableau) {
    calque_= _calque;
    isPopupShown= false;
    stask= null;
    nf_= NumberFormat.getInstance(Locale.FRENCH);
    nf_.setMaximumFractionDigits(2);
    nf2_= NumberFormat.getInstance(Locale.US);
    nf2_.setMaximumFractionDigits(2);
    nf2_.setGroupingUsed(false);
    nf3_= NumberFormat.getInstance(Locale.US);
    nf3_.setMaximumFractionDigits(3);
    nf3_.setGroupingUsed(false);
    graphe_= _graphe;
    tableau_= _tableau;
  }
  private double nord_;
  public void setFlecheNord(double angle) {
    nord_= angle;
    tableau_.setFlecheNord(angle);
  }
  /**********************************************/
  // EVENEMENTS
  /**********************************************/
  // Mouse
  public void mousePressed(MouseEvent _evt) {
    if (isGele())
      return;
    if (_evt.getModifiers() == InputEvent.BUTTON1_MASK) {
      //int yclick=calque_.getPreferredSize().height-_evt.getY();
      //Graphics g=calque_.getGraphics();
      //g.setColor(Color.blue);
      //g.drawRect((int)click.x-1, calque_.getPreferredSize().height-(int)click.y-1, 2, 2);
      //g.drawRect((int)click.x-1, (int)click.y-1, 2, 2);
      stask= new SelectTask(_evt.getX(), _evt.getY(), PAS_CALCUL);
      stask.start();
    } else if (_evt.getModifiers() == InputEvent.BUTTON3_MASK) {
      stask= new SelectTask(_evt.getX(), _evt.getY(), ORTHOGONALE);
      stask.start();
    }
  }
  public void mouseReleased(MouseEvent _evt) {
    if (isGele())
      return;
    if (isPopupShown) {
      if ((_evt.getModifiers() == InputEvent.BUTTON1_MASK)
        || (_evt.getModifiers() == InputEvent.BUTTON3_MASK)) {
        if (popup_ != null)
          popup_.dispose();
        calque_.paintImmediately(xsel - 2, ysel - 2, 4, 4);
      }
      isPopupShown= false;
    } else if (stask != null) {
      stask.stopSelect();
      stask= null;
    }
  }
  public void mouseClicked(MouseEvent _evt) {}
  public void mouseEntered(MouseEvent _evt) {}
  public void mouseExited(MouseEvent _evt) {}
  // Pas beau: trouver autre methode de recuperer l'espacement des orth
  //  private double espacementOrth=1.;
  //  public void setEspacementOrth(double _e) { espacementOrth=_e; }
  // Methodes privees
  String getInfoResultat(
    SResultatPasCalcul05 r,
    SResultatPasInterOrth s) {
    String info= "";
    info += "pas             : " + r.numeroPas + "\n";
    info += "x               : " + nf_.format(r.pointCourantX) + " (m)\n";
    info += "y               : " + nf_.format(r.pointCourantY) + " (m)\n";
    info += "angle           : "
      + nf_.format(r.angleHoriz * 180 / Math.PI - nord_)
      + " (degr�s)\n";
    info += "longueur d'onde : " + nf_.format(r.longueurOnde) + " (m)\n";
    info += "hauteur d'eau   : " + nf_.format(r.hauteurEau) + " (m)\n";
    if (s != null)
      info += "hauteur de houle: " + nf_.format(s.hauteurHoule) + " (m)\n";
    return info;
  }
  void popInfoBox(String info) {
    Graphics g= calque_.getGraphics();
    Font f= g.getFont();
    Color c= g.getColor();
    popup_= new VagPopupMessage(calque_);
    popup_.setText(info);
    popup_.setFont(new Font("Courier", f.getStyle(), f.getSize()));
    popup_.setLocation(xsel, ysel);
    popup_.show();
    g.setColor(new Color(0, 200, 20));
    g.drawRect(xsel - 1, ysel - 1, 2, 2);
    g.setColor(c);
    isPopupShown= true;
  }
  void popInfoMenu(String title) {
    OrthMenuListener listener= new OrthMenuListener();
    JPopupMenu menu= new JPopupMenu();
    menu.add(new TitleMenuItem(title));
    menu.addSeparator();
    JMenuItem mi;
    mi= new JMenuItem(ANGLE);
    mi.addActionListener(listener);
    menu.add(mi);
    mi= new JMenuItem(LONGUEUR_ONDE);
    mi.addActionListener(listener);
    menu.add(mi);
    mi= new JMenuItem(HAUTEUR_EAU);
    mi.addActionListener(listener);
    menu.add(mi);
    mi= new JMenuItem(HAUTEUR_HOULE);
    mi.addActionListener(listener);
    if (selectedIOrth == null)
      mi.setEnabled(false);
    menu.add(mi);
    mi= new JMenuItem(TABLEAU_ORTH);
    mi.addActionListener(listener);
    menu.add(mi);
    mi= new JMenuItem(TABLEAU_IORTH);
    mi.addActionListener(listener);
    menu.add(mi);
    menu.show(this, xsel, ysel);
    isPopupShown= true;
  }
  void sendToGraph(
    SResultatUnitaire05 orth,
    SResultatUnitaireInterOrth iorth,
    String courbe) {
    String titre= "Orthogonale " + orth.numeroOrth;
    String ordUnite= "";
    String crbTitre= "";
    Vector vals= new Vector();
    Valeur v= null;
    double s= 0.;
    double maxVal= 0.;
    double minVal= 0.;
    double deltaX= 0., deltaY= 0.;
    if (orth.pas.length == 0)
      return;
    if (courbe.equals(ANGLE)) {
      ordUnite= "deg";
      crbTitre= "angle(s)";
      // angle ./. Nord
      double angle= orth.pas[0].angleHoriz * 180 / Math.PI - 90.;
      v= new Valeur();
      v.s_= s;
      v.v_= angle;
      vals.add(v);
      maxVal= minVal= angle;
    } else if (courbe.equals(LONGUEUR_ONDE)) {
      ordUnite= "m";
      crbTitre= "lambda(s)";
      v= new Valeur();
      v.s_= s;
      v.v_= orth.pas[0].longueurOnde;
      vals.add(v);
      maxVal= minVal= orth.pas[0].longueurOnde;
    } else if (courbe.equals(HAUTEUR_EAU)) {
      ordUnite= "m";
      crbTitre= "he(s)";
      v= new Valeur();
      v.s_= s;
      v.v_= orth.pas[0].hauteurEau;
      vals.add(v);
      maxVal= minVal= orth.pas[0].hauteurEau;
    } else if (courbe.equals(HAUTEUR_HOULE)) {
      ordUnite= "m";
      crbTitre= "hh(s)";
      //      double hhoule=0.;
      //      try { hhoule=calculeHauteurHoule(orth.numeroOrth, 0); }
      //      catch( HauteurHouleException e ) {};
      double hhoule= iorth.pas[0].hauteurHoule;
      v= new Valeur();
      v.s_= s;
      v.v_= hhoule;
      vals.add(v);
      maxVal= minVal= hhoule;
    }
    for (int i= 1; i < orth.pas.length; i++) {
      deltaX= orth.pas[i].pointCourantX - orth.pas[i - 1].pointCourantX;
      deltaY= orth.pas[i].pointCourantY - orth.pas[i - 1].pointCourantY;
      s += Math.sqrt(deltaX * deltaX + deltaY * deltaY);
      if (courbe.equals(ANGLE)) {
        double angle= orth.pas[i].angleHoriz * 180 / Math.PI - 90.;
        v= new Valeur();
        v.s_= s;
        v.v_= angle;
        vals.add(v);
        if (angle > maxVal)
          maxVal= angle;
        if (angle < minVal)
          minVal= angle;
      } else if (courbe.equals(LONGUEUR_ONDE)) {
        v= new Valeur();
        v.s_= s;
        v.v_= orth.pas[i].longueurOnde;
        vals.add(v);
        if (orth.pas[i].longueurOnde > maxVal)
          maxVal= orth.pas[i].longueurOnde;
        if (orth.pas[i].longueurOnde < minVal)
          minVal= orth.pas[i].longueurOnde;
      } else if (courbe.equals(HAUTEUR_EAU)) {
        v= new Valeur();
        v.s_= s;
        v.v_= orth.pas[i].hauteurEau;
        vals.add(v);
        if (orth.pas[i].hauteurEau > maxVal)
          maxVal= orth.pas[i].hauteurEau;
        if (orth.pas[i].hauteurEau < minVal)
          minVal= orth.pas[i].hauteurEau;
      } else if (courbe.equals(HAUTEUR_HOULE)) {
        //        double hhoule=0.;
        //        try { hhoule=calculeHauteurHoule(orth.numeroOrth, i); }
        //        catch( HauteurHouleException e ) {};
        if (i == iorth.pas.length)
          break;
        double hhoule= iorth.pas[i].hauteurHoule;
        v= new Valeur();
        v.s_= s;
        v.v_= hhoule;
        vals.add(v);
        if (hhoule > maxVal)
          maxVal= hhoule;
        if (hhoule < minVal)
          minVal= hhoule;
        titre= "InterOrthogonale " + iorth.orth1 + " - " + iorth.orth2;
      }
    }
    Graphe graphe= new Graphe();
    graphe.titre_= titre + " : " + courbe;
    graphe.animation_= false;
    graphe.legende_= true;
    graphe.marges_= new Marges();
    graphe.marges_.gauche_= 60;
    graphe.marges_.droite_= 80;
    graphe.marges_.haut_= 45;
    graphe.marges_.bas_= 30;
    Axe ax= new Axe();
    ax.titre_= "s";
    ax.unite_= "m";
    ax.vertical_= false;
    ax.graduations_= true;
    ax.grille_= true;
    ax.minimum_= 0.;
    double xx= Math.round(s);
    xx= xx - xx % 10;
    ax.pas_= (xx - ax.minimum_) / 10;
    ax.maximum_= s;
    graphe.ajoute(ax);
    Axe ay= new Axe();
    ay.titre_= courbe;
    ay.unite_= ordUnite;
    ay.vertical_= true;
    ay.graduations_= true;
    ay.grille_= true;
    ay.minimum_= minVal;
    ay.maximum_= maxVal;
    ay.pas_= (Math.round(maxVal) - Math.round(minVal)) / 10;
    ay.pas_= Math.round(ay.pas_);
    graphe.ajoute(ay);
    CourbeDefault c= new CourbeDefault();
    c.titre_= crbTitre;
    //c.type="aire";
    c.trace_= "lineaire";
    c.marqueurs_= true;
    c.valeurs_= vals;
    graphe.ajoute(c);
    graphe_.setGraphe(graphe);
  }

  // Inner classes
  class TitleMenuItem extends JMenuItem {
    public TitleMenuItem(String title) {
      super(title);
    }
    public boolean isSelected() {
      return false;
    }
    public void setSelected(boolean b) {}
    public boolean isArmed() {
      return false;
    }
    public void setArmed(boolean b) {}
  }
  class OrthMenuListener implements ActionListener {
    public void actionPerformed(ActionEvent evt) {
      String s= evt.getActionCommand();
      if (s.equals(TABLEAU_ORTH)) {
        tableau_.setResultats05(selectedOrth);
        Container frame= tableau_.getParent();
        while ((frame != null) && (!(frame instanceof JInternalFrame)))
          frame= frame.getParent();
        if (frame != null)
          ((JInternalFrame)frame).setTitle(
            "Orthogonale " + selectedOrth.numeroOrth);
      } else if (s.equals(TABLEAU_IORTH)) {
        if (selectedIOrth != null) {
          tableau_.setResultatsInterOrth(selectedIOrth);
          Container frame= tableau_.getParent();
          while ((frame != null) && (!(frame instanceof JInternalFrame)))
            frame= frame.getParent();
          if (frame != null)
            ((JInternalFrame)frame).setTitle(
              "Inter-Orthogonale "
                + selectedIOrth.orth1
                + "-"
                + selectedIOrth.orth2);
        }
      } else {
        sendToGraph(selectedOrth, selectedIOrth, evt.getActionCommand());
        if (!graphe_.isInteractif())
          graphe_.setInteractif(true);
      }
    }
  }
  class SelectTask extends Thread {
    int xclick, yclick, mode;

    boolean stop_;
    public SelectTask(int _xclick, int _yclick, int _mode) {
      xclick= _xclick;
      yclick= _yclick;
      mode= _mode;
    }

    public void stopSelect(){
      stop_=true;
      selectedPas= null;
      selectedOrth= null;
      selectedIOrth= null;
    }
    public void run() {
      stop_=false;
      double dist, mindist;
      GrPoint click= new GrPoint(xclick, yclick, 0.).applique(getVersReel());
      SResultats05 r= calque_.getResultats05();
      SResultatsInterOrth s= calque_.getResultatsInterOrth();
      selectedPas= null;
      selectedOrth= null;
      selectedIOrth= null;
      int noOrth= 0, noPoint= 0;
      mindist=
        calque_.getPolyligne(0).sommets_.renvoie(0).distance(click);
      for (int o= 0; o < r.orthogonales.length; o++) {
        for (int p= 0; p < r.orthogonales[o].pas.length; p++) {
          GrPoint curp=
            new GrPoint(
              r.orthogonales[o].pas[p].pointCourantX,
              r.orthogonales[o].pas[p].pointCourantY,
              0.);
          if ((dist= curp.distance(click)) < mindist) {
            mindist= dist;
            selectedOrth= r.orthogonales[o];
            if (o < s.interOrth.length)
              selectedIOrth= s.interOrth[o];
            else
              selectedIOrth= null;
            selectedPas= r.orthogonales[o].pas[p];
            noOrth= o;
            noPoint= p;
          }
        }
      }
      if(stop_) return;
      if (selectedPas != null) {
        GrPoint sel=
          new GrPoint(
            selectedPas.pointCourantX,
            selectedPas.pointCourantY,
            0.).applique(
            getVersEcran());
        if (sel.distance(new GrPoint(xclick, yclick, 0.)) < 10) {
          xsel= (int)sel.x_;
          ysel= (int)sel.y_;
          if (mode == PAS_CALCUL) {
            // calcul hauteur de houle
            //            double H=0.;
            //            boolean Hok=true;
            //            try { H=calculeHauteurHoule(noOrth, noPoint); }
            //            catch( HauteurHouleException e ) { Hok=false; }
            popInfoBox(
              "Orthogonale "
                + noOrth
                + "\n"
                + getInfoResultat(
                  selectedPas,
                  (selectedIOrth == null ? null : selectedIOrth.pas[noPoint])));
          } else if (mode == ORTHOGONALE) {
            popInfoMenu("Orthogonale " + noOrth);
          }
          System.err.println("Orthogonale " + noOrth + ", Point " + noPoint);
        } else {
          xsel= xclick;
          ysel= yclick;
          popInfoBox("Pas de point\nr�sultat ici");
        }
      }
    }
  }
  /*
    class HauteurHouleException extends Exception
    {
      public HauteurHouleException() { super(); }
      public HauteurHouleException(Exception e) { super(e.getMessage()); }
      public HauteurHouleException(String msg) { super(msg); }
    }
  */
}
