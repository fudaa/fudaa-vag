/*
 * @file         VagMaillage.java
 * @creation     1999-11-12
 * @modification $Date: 2007-01-19 13:14:35 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.vag;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.StringTokenizer;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuMainPanel;
import com.memoire.bu.BuTaskOperation;

import org.fudaa.dodico.corba.calcul.SProgression;
import org.fudaa.dodico.corba.dunes.ICalculDunes;
import org.fudaa.dodico.corba.dunes.IParametresDunes;
import org.fudaa.dodico.corba.dunes.IParametresDunesHelper;
import org.fudaa.dodico.corba.dunes.IResultatsDunes;
import org.fudaa.dodico.corba.dunes.IResultatsDunesHelper;
import org.fudaa.dodico.corba.geometrie.SPoint;

import org.fudaa.ebli.geometrie.GrMaillageElement;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.ebli.geometrie.GrVecteur;
import org.fudaa.ebli.geometrie.VecteurGrPoint;

import org.fudaa.fudaa.commun.conversion.FudaaInterpolateurMaillage;
import org.fudaa.fudaa.commun.conversion.FudaaMaillageElement;
import org.fudaa.fudaa.commun.conversion.FudaaPoint;
/**
 * Mailleur pour Vag (remplace TVAG).
 *
 * @version      $Revision: 1.10 $ $Date: 2007-01-19 13:14:35 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class VagMaillage {
  IParametresDunes duPar_;
  ICalculDunes duCal_;
  IResultatsDunes duRes_;
  GrPolygone grilleRectangle_;
  double[][] grilleValeurs_;
  VecteurGrPoint semis_;
  GrMaillageElement maillage_;
  FudaaInterpolateurMaillage interpolateur_;
  BuCommonInterface appli_;
  public VagMaillage(BuCommonInterface appli, ICalculDunes c) {
    appli_= appli;
    duCal_= c;
    duPar_= null;
    duRes_= null;
    grilleRectangle_= null;
    grilleValeurs_= null;
    semis_= null;
    maillage_= null;
    interpolateur_= null;
  }
  public void delete() {
    duPar_= null;
    duCal_= null;
    duRes_= null;
    grilleRectangle_= null;
    grilleValeurs_= null;
    semis_= null;
    maillage_= null;
    interpolateur_= null;
    appli_= null;
  }
  public synchronized void importerSemis(File fic) {
    setProgression(0);
    double minX= Double.POSITIVE_INFINITY;
    double minY= Double.POSITIVE_INFINITY;
    int p= 0;
    int pc= 0, prec= 0;
    try {
      semis_= new VecteurGrPoint();
      LineNumberReader in= new LineNumberReader(new FileReader(fic));
      String line= in.readLine();
      StringTokenizer tok= null;
      while (line != null) {
        if (line.trim().startsWith("#")) {
          line= in.readLine();
          continue;
        }
        tok= new StringTokenizer(line);
        double x= Double.parseDouble(tok.nextToken());
        double y= Double.parseDouble(tok.nextToken());
        double z= Double.parseDouble(tok.nextToken());
        if (x < minX)
          minX= x;
        if (y < minY)
          minY= y;
        semis_.ajoute(new GrPoint(x, y, z));
        p += line.length();
        pc= (int) (100 * p / fic.length());
        if (pc >= (prec + 5)) {
          setProgression(pc);
          prec= pc;
        }
        line= in.readLine();
      }
      in.close();
    } catch (IOException e) {
      semis_= null;
      System.err.println(e);
      return;
    }
    setProgression(100);
    try {
      Thread.sleep(250);
    } catch (InterruptedException ie) {}
    setProgression(0);
    int siz= semis_.nombre();
    prec= 0;
    for (int i= 0; i < siz; i++) {
      GrPoint po= semis_.renvoie(i);
      po.x_ -= minX;
      po.y_ -= minY;
      pc= i / siz;
      if (pc >= (prec + 5)) {
        setProgression(pc);
        prec= pc;
      }
    }
    setProgression(100);
    try {
      Thread.sleep(250);
    } catch (InterruptedException ie) {}
    setProgression(0);
  }
  public GrMaillageElement getMaillage() {
    return maillage_;
  }
  public VecteurGrPoint getSemis() {
    return semis_;
  }
  public double[][] getValeurs() {
    return grilleValeurs_;
  }
  public double[] getValeursVecteur() {
    if ((grilleValeurs_ == null)
      || (grilleValeurs_.length == 0)
      || (grilleValeurs_[0].length == 0))
      return new double[0];
    double[] res= new double[grilleValeurs_.length * grilleValeurs_[0].length];
    for (int i= 0; i < grilleValeurs_.length; i++) {
      for (int j= 0; j < grilleValeurs_[i].length; j++) {
        res[(grilleValeurs_[i].length - 1 - j) * grilleValeurs_.length + i]=
          grilleValeurs_[i][j];
      }
    }
    return res;
  }
  public synchronized void mailler() {
    if (semis_ == null)
      return;
    setProgression(0);
    // trianguler avec Dunes
    SPoint[] sPts= new SPoint[semis_.nombre()];
    for (int i= 0; i < sPts.length; i++)
      sPts[i]= FudaaPoint.gr2S(semis_.renvoie(i));
    duPar_=
      IParametresDunesHelper.narrow(
        duCal_.parametres(VagImplementation.CONNEXION_DUNES));
    duPar_.points(sPts);
    duCal_.optionC(true);
    duCal_.optionQ(false);
    duCal_.optionA(false);
    duCal_.optionO(false);
    if (appli_ == null) {
      setProgression(5);
      duCal_.calcul(VagImplementation.CONNEXION_DUNES);
      setProgression(95);
      duRes_=
        IResultatsDunesHelper.narrow(
          duCal_.resultats(VagImplementation.CONNEXION_DUNES));
      maillage_= FudaaMaillageElement.s2Gr(duRes_.maillage());
      setProgression(100);
      try {
        Thread.sleep(250);
      } catch (InterruptedException ie) {}
      setProgression(0);
      duRes_.maillage(null);
    } else {
      final ICalculDunes dunes= duCal_;
      final BuMainPanel mp= appli_.getMainPanel();
      final BuTaskOperation op=
        new BuTaskOperation(appli_.getApp(), "Triangulation") {
        public void act() {
          dunes.calcul(VagImplementation.CONNEXION_DUNES);
          mp.setMessage("Traitement donn�es");
          VagMaillage.this.setProgression(60);
          duRes_=
            IResultatsDunesHelper.narrow(
              duCal_.resultats(VagImplementation.CONNEXION_DUNES));
          maillage_= FudaaMaillageElement.s2Gr(duRes_.maillage());
          VagMaillage.this.setProgression(100);
          try {
            Thread.sleep(500);
          } catch (InterruptedException ie) {}
          mp.setMessage("");
          VagMaillage.this.setProgression(0);
          //duRes_.maillage(null);

        }
      };
      op.start();
      SProgression msg;
      if (op.isAlive()) {
        msg= dunes.progression();
        while (msg == null) {
          msg= dunes.progression();
          try {
            Thread.sleep(1000);
          } catch (InterruptedException e) {}
        }
      }
      while (op.isAlive()) {
        msg= dunes.progression();
        if (msg == null) {
          mp.setProgression(100);
          mp.setMessage("Op�ration termin�e");
          try {
            Thread.sleep(1000);
          } catch (InterruptedException e) {}
        } else {
          mp.setMessage(msg.operation);
          mp.setProgression(msg.pourcentage);
        }
        try {
          Thread.sleep(1000);
        } catch (InterruptedException e) {}
      }
      mp.setProgression(0);
      mp.setMessage("Op�ration termin�e");
    }
  }
  public synchronized void interpoler(
    GrPolygone rect,
    int nbMaillesX,
    int nbMaillesY) {
    if ((rect == null) || (nbMaillesX == 0) || (nbMaillesY == 0)) {
      System.err.println("VagMaillage: grille nulle");
      return;
    }
    setProgression(0);
    // faire la grille
    GrPoint o= rect.sommet(0);
    GrVecteur pasX= rect.sommet(1).soustraction(o).division(nbMaillesX);
    GrVecteur pasY= rect.sommet(3).soustraction(o).division(nbMaillesY);
    grilleRectangle_= rect;
    grilleValeurs_= new double[nbMaillesX + 1][nbMaillesY + 1];
    // interpoler avec FudaaInterpolateurMaillage
    interpolateur_=
      new FudaaInterpolateurMaillage(
        maillage_,
        FudaaInterpolateurMaillage.METHODE_LINEAIRE);
    int pc= 0, prec= 0;
    for (int i= 0; i < grilleValeurs_.length; i++) {
      pc= 100 * i / grilleValeurs_.length;
      if (pc >= (prec + 5)) {
        setProgression(pc);
        prec= pc;
      }
      GrPoint pi= o.addition(pasX.multiplication(i));
      for (int j= 0; j < grilleValeurs_[i].length; j++) {
        GrPoint p0= pi.addition(pasY.multiplication(j));
        grilleValeurs_[i][j]= interpolateur_.interpolePoint(p0).z_;
      }
    }
    setProgression(100);
    try {
      Thread.sleep(250);
    } catch (InterruptedException ie) {}
    setProgression(0);
  }
  void setProgression(int p) {
    if (appli_ != null) {
      appli_.getMainPanel().setProgression(p);
      //javax.swing.RepaintManager.currentManager(appli_.getMainPanel().getTaskView()).
      //  paintDirtyRegions();
    }
  }
}
