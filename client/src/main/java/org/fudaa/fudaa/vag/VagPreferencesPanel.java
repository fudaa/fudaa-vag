/*
 * @creation     1999-01-05
 * @modification $Date: 2007-06-28 09:28:18 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.vag;
import java.util.Hashtable;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.AbstractBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import com.memoire.bu.BuAbstractPreferencesPanel;
import com.memoire.bu.BuGridLayout;
/**
 * Panneau de preferences pour Vag.
 *
 * @version      $Revision: 1.7 $ $Date: 2007-06-28 09:28:18 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class VagPreferencesPanel extends BuAbstractPreferencesPanel {
  Hashtable optionsStr_;
  BuGridLayout loVag_;
  JPanel pnVagCalq_;
  AbstractBorder boVagCalq_;
  BuGridLayout loVagCalq_;
  JLabel lbVagCalqInter_;
  JComboBox liVagCalqInter_;
  VagPreferences options_;
  VagImplementation vag_;
  public String getTitle() {
    return "Vag";
  }
  // Constructeur
  public VagPreferencesPanel(VagImplementation _vag) {
    super();
    options_= VagPreferences.VAG;
    vag_= _vag;
    int nVag, nVagCalq;
    Object items[];
    optionsStr_= new Hashtable();
    String item;
    String itemKey;
    optionsStr_.put(itemKey= "Orthogonales", item= "O-Interaction");
    optionsStr_.put(item, itemKey);
    optionsStr_.put(itemKey= "Dessin", item= "D-Interaction");
    optionsStr_.put(item, itemKey);
    // Panneau Vag
    loVag_= new BuGridLayout();
    loVag_.setColumns(1);
    this.setLayout(loVag_);
    nVag= 0;
    // Calques
    pnVagCalq_= new JPanel();
    boVagCalq_=
      new CompoundBorder(
        new TitledBorder("Calques"),
        new EmptyBorder(5, 5, 5, 5));
    pnVagCalq_.setBorder(boVagCalq_);
    loVagCalq_= new BuGridLayout();
    loVagCalq_.setColumns(2);
    loVagCalq_.setVgap(5);
    loVagCalq_.setHgap(5);
    pnVagCalq_.setLayout(loVagCalq_);
    nVagCalq= 0;
    // Calque interactif
    lbVagCalqInter_= new JLabel("Interactif");
    items= new String[2];
    items[0]= "Orthogonales";
    items[1]= "Dessin";
    liVagCalqInter_= new JComboBox(items);
    liVagCalqInter_.setEditable(false);
    pnVagCalq_.add(lbVagCalqInter_, nVagCalq++);
    pnVagCalq_.add(liVagCalqInter_, nVagCalq++);
    this.add(pnVagCalq_, nVag++);
    updateComponents();
  }
  public boolean isPreferencesValidable() {
    return true;
  }
  public void validatePreferences() {
    fillTable();
    options_.writeIniFile();
  }
  public boolean isPreferencesApplyable() {
    return true;
  }
  public void applyPreferences() {
    fillTable();
    options_.applyOn(vag_);
  }
  public boolean isPreferencesCancelable() {
    return true;
  }
  public void cancelPreferences() {
    options_.readIniFile();
    updateComponents();
  }
  // Methodes privees
  private void fillTable() {
    String interStr= (String)optionsStr_.get(liVagCalqInter_.getSelectedItem());
    options_.putStringProperty(
      "vag.calques.interactif",
      interStr == null ? "" : interStr);
  }
  private void updateComponents() {
    liVagCalqInter_.setSelectedItem(
      optionsStr_.get(options_.getStringProperty("vag.calques.interactif")));
  }
}
