/*
 * @creation     1998-10-02
 * @modification $Date: 2007-06-28 09:28:18 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.vag;
import java.awt.event.ActionEvent;
import java.util.Vector;

import javax.swing.JComponent;
import javax.swing.event.TreeSelectionEvent;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuInformationsDocument;
import com.memoire.bu.BuToggleButton;

import org.fudaa.ebli.calque.BArbreCalque;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BCalqueInteraction;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.calque.CalqueGuiHelper;
import org.fudaa.ebli.calque.EbliFilleCalques;
/**
 * Une fenetre fille pour les calques.
 *
 * @version      $Revision: 1.9 $ $Date: 2007-06-28 09:28:18 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class VagFilleCalques extends EbliFilleCalques {
  CalqueGuiHelper cGui_;
  JComponent[] tools_;
  BCalque calqueActif_;
  BuToggleButton btDessin_;
  BuToggleButton btOrtho_;
  VagCalqueOrthogonalesInteraction cqOrthoI_;
  BCalqueInteraction cqDessinI_;
  public VagFilleCalques(
    BCalque _cInit,
    BArbreCalque _arbre,
    BuCommonInterface _app,
    BuInformationsDocument _id) {
    super(_cInit, _app, _id);
    addInternalFrameListener(_arbre);
    setSelectionVisible(false);
    cGui_= CalqueGuiHelper.CALQUE_OUTILS;
    cGui_.addPropertyChangeListener(_arbre.getArbreModel());
    tools_= new JComponent[0];
    calqueActif_= null;
    BCalque cqRoot= getVueCalque().getCalque();
    cqOrthoI_= (VagCalqueOrthogonalesInteraction)cqRoot.getCalqueParNom("cqORTHOGONALESI");
    cqDessinI_= (BCalqueInteraction)cqRoot.getCalqueParNom("cqDESSINI");
    btOrtho_= new BuToggleButton("Orthogonales");
    btOrtho_.setName("btORTHO");
    btOrtho_.addActionListener(this);
    associeBoutonCalqueInteraction(cqOrthoI_, btOrtho_);
    addBouton(btOrtho_);
    btDessin_= new BuToggleButton("Dessin");
    btDessin_.setName("btDESSIN");
    btDessin_.addActionListener(this);
    associeBoutonCalqueInteraction(cqDessinI_, btDessin_);
    addBouton(btDessin_);
    buildTools();
  }
  public JComponent[] getSpecificTools() {
//    for (int j= 0; j < tools_.length; j++) {
//      try {
//        tempBp= ((BuPopupButton)tools_[j]);
//        tempBp.setDesktop((BuDesktop)getDesktopPane());
//        /*fred deniger: ces lignes sont a supprimer par la suite. Il y a un
//          probleme d'affichage pour ce BuPopupButton:les sliders ne sont pas
//          affiches la premiere fois.
//          Solution temporaire: la fenetre de ce BuPopButton est affichee puis cachee
//          aussitot.
//          */
//        if ("DEFINIRLISSAGE".equals(tempBp.getActionCommand())) {
//          //pour ne pas voir l'affichage temporaire.
//          tempBp.setLocation(-1000, -1000);
//          tempBp.setSelected(true);
//         // tempBp.setVisible(true);
//         // tempBp.setVisible(false);
//          tempBp.setSelected(false);
//          tempBp.setLocation(0, 0);
//        }
//        //fin du code a supprimer
//      } catch (ClassCastException e) {}
//    }
    JComponent[] supertools= super.getSpecificTools();
    JComponent[] tools= new JComponent[supertools.length + tools_.length];
    for (int i= 0; i < supertools.length; i++) {
      tools[i]= supertools[i];
    }
    for (int i= 0; i < tools_.length; i++) {
      tools[supertools.length + i]= tools_[i];
    }
    //la modif
    return tools_;
  }
  public void setInteractif(String _name) {
    BCalque[] calques= getVueCalque().getCalque().getCalques();
    for (int c= 0; c < calques.length; c++) {
      if ((calques[c] instanceof BGroupeCalque)
        && (calques[c].getTitle().equals("Calques Vag"))) {
        calques= calques[c].getTousCalques();
        for (int i= 0; i < calques.length; i++) {
          if (calques[i] instanceof BCalqueInteraction) {
            BCalqueInteraction cq= (BCalqueInteraction)calques[i];
            if (calques[i].getName().equals(_name)) {
              cq.setGele(false);
              getArbreCalqueModel().setSelectionCalque(cq);
            } else {
              cq.setGele(true);
            }
          }
        }
      }
    }
  }
  public void repaint(int time) {
    super.repaint();
    //vc.repaint(0);
  }
  // Evenement de l'arbre
  public void valueChanged(TreeSelectionEvent _e) {
    super.valueChanged(_e);
    try {
      calqueActif_=
        (BCalque)_e.getNewLeadSelectionPath().getLastPathComponent();
      buildTools();
      if (tools_.length > 0)
        tools_[0].doLayout();
    } catch (NullPointerException e) {}
  }
  public void actionPerformed(ActionEvent e) {
    if (e.getSource() == btDessin_) {
      getArbreCalqueModel().setSelectionCalque(cqDessinI_);
      //      if( btDessin_.isSelected() ) setInteractif("cqDESSINI");
      //      else setInteractif("cqORTHOGONALESI");
      //      getArbreCalque().repaint();
    } else if (e.getSource() == btOrtho_) {
      getArbreCalqueModel().setSelectionCalque(cqOrthoI_);
      //      getArbreCalque().repaint();
    } else
      super.actionPerformed(e);
  }
  // Methodes privees
  private void buildTools() {
    Vector compos= new Vector();
    int v= 0;
    BCalque[] calques= getVueCalque().getCalque().getTousCalques();
    for (int i= 0; i < calques.length; i++) {
      tools_= cGui_.getSpecificTools(calques[i]);
      boolean actif= (calques[i] == calqueActif_);
      for (int j= 0; j < tools_.length; j++) {
        if (actif) {
          tools_[j].setEnabled(true);
          v++;
        }
        if (!compos.contains(tools_[j])) {
          if (!actif) {
            tools_[j].setEnabled(false);
          }
          compos.addElement(tools_[j]);
        }
      }
    }
    //System.err.println("FilleCalques: actif="+(calqueActif_==null?"null":calqueActif_.getName())+", "+v+" visibles");
    tools_= new JComponent[compos.size()];
    compos.copyInto(tools_);
  }
}
