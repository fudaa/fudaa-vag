/*
 * @creation     1999-01-11
 * @modification $Date: 2007-06-28 09:28:18 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.vag;
import java.awt.Dimension;

import org.fudaa.dodico.corba.vag.SResultats05;

import org.fudaa.ebli.calque.BCalquePolyligne;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolyligne;
/**
 * Un calque derive des resultats de Vag.
 *
 * @version      $Revision: 1.7 $ $Date: 2007-06-28 09:28:18 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class VagCalqueCretes extends BCalquePolyligne {
  // donnees membres publiques
  // donnees membres privees
  // Constructeur
  public VagCalqueCretes() {
    super();
    resultats05_= null;
    setPreferredSize(new Dimension(640, 480));
  }
  // Methodes publiques
  public void setResolution(int _resolution) {
    reinitialise();
    construitPolylignes(_resolution);
    //super.setResolution(_resolution);
    repaint();
  }
  /**********************************************/
  // PROPRIETES INTERNES
  /**********************************************/
  // Propriete resultats
  private SResultats05 resultats05_;
  SResultats05 getResultats05() {
    return resultats05_;
  }
  public void setResultats05(SResultats05 _resultats) {
    SResultats05 vp= resultats05_;
    resultats05_= _resultats;
    reinitialise();
    construitPolylignes(getResolution());
    firePropertyChange("resultats05", vp, resultats05_);
  }
  // Methodes privees
  private void construitPolylignes(int _resolution) {
    if (resultats05_.orthogonales == null)
      return;
    int minI= 0;
    int maxI= 0;
    if (resultats05_.orthogonales.length > 0)
      maxI= minI= resultats05_.orthogonales[0].pas.length;
    for (int i= 0; i < resultats05_.orthogonales.length; i++) {
      if (resultats05_.orthogonales[i].pas.length < minI)
        minI= resultats05_.orthogonales[i].pas.length;
      if (resultats05_.orthogonales[i].pas.length > maxI)
        maxI= resultats05_.orthogonales[i].pas.length;
    }
    double pas= (double)1000 / (double) (_resolution == 0 ? 1 : _resolution);
    for (double p= 0.; p < minI; p += pas) {
      GrPolyligne nouvLigne= new GrPolyligne();
      for (int i= 0; i < resultats05_.orthogonales.length; i++)
        nouvLigne.sommets_.ajoute(
          new GrPoint(
            resultats05_.orthogonales[i].pas[(int)p].pointCourantX,
            resultats05_.orthogonales[i].pas[(int)p].pointCourantY,
            0.));
      ajoute(nouvLigne);
    }
    for (double p= 0.; p < maxI; p += pas) {
      GrPolyligne nouvLigne= new GrPolyligne();
      for (int i= 0; i < resultats05_.orthogonales.length; i++)
        if (resultats05_.orthogonales[i].pas.length > p)
          nouvLigne.sommets_.ajoute(
            new GrPoint(
              resultats05_.orthogonales[i].pas[(int)p].pointCourantX,
              resultats05_.orthogonales[i].pas[(int)p].pointCourantY,
              0.));
        else {
          if (nouvLigne.sommets_.nombre() > 0)
            ajoute(nouvLigne);
          nouvLigne= new GrPolyligne();
        }
      if (nouvLigne.sommets_.nombre() > 0)
        ajoute(nouvLigne);
    }
  }
}
