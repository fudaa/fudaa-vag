/*
 * @file         FudaaPopupMessage.java
 * @creation     1998-10-19
 * @modification $Date: 2007-01-19 13:14:35 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.vag;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Point;

import javax.swing.JComponent;

import com.memoire.fu.FuLog;

import org.fudaa.ctulu.CtuluLibString;
/**
 * Une fenetre pop-up pour des messages.
 *
 * @version      $Revision: 1.1 $ $Date: 2007-01-19 13:14:35 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class VagPopupMessage {
  // Donnees membres privees
  private final Container topParent_;
  private final JComponent parent_;
  private int  yloc_;
  private int xloc_;
  private Font font_;
  private String text_;
  // Constructeur
  public VagPopupMessage(final JComponent _parent) {
    Container topParent= _parent.getTopLevelAncestor();
    parent_= _parent;
    if (topParent == null) {
      FuLog.warning(
        "ERROR: FudaaPopupMessage's topParent has not be added to any container!");
      FuLog.warning("       Attaching it to nearest topParent");
      topParent= _parent;
    }
    topParent_=topParent;
    xloc_= 0;
    yloc_= 0;
    font_= null;
    text_= null;
  }
  // Methodes publiques
  public void show() {
    final Graphics g= topParent_.getGraphics();
    final Font ft= getFont();
    String tt= getText();
    final Dimension dm= getPreferredSize();
    final Dimension topDm= topParent_.getSize();
    if ((xloc_ + dm.width + 10) > topDm.width) {
      xloc_= topDm.width - dm.width - 20;
    }
    if ((yloc_ + dm.height + 10) > topDm.height) {
      yloc_= topDm.height - dm.height - 20;
    }
    g.setColor(Color.lightGray);
    g.fill3DRect(xloc_ - 10, yloc_ - 10, dm.width + 20, dm.height + 20, true);
    g.setFont(ft);
    g.setColor(Color.blue);
    final FontMetrics fm= g.getFontMetrics();
    final int x= xloc_;
    int y= yloc_ + fm.getMaxAscent();
    while (!tt.equals("")) {
      final int i= tt.indexOf("\n");
      String s;
      if (i >= 0) {
        s= tt.substring(0, i);
        tt= tt.substring(i + 1);
      } else {
        s= tt;
        tt= "";
      }
      g.drawString(s, x, y);
      y += fm.getMaxAscent() + fm.getMaxDescent();
    }
  }
  public void dispose() {
    final Dimension dm= getPreferredSize();
    topParent_.repaint(xloc_ - 10, yloc_ - 10, dm.width + 20, dm.height + 20);
  }
  public void setLocation(final int _x, final int _y) {
    final Insets insets= parent_.getInsets();
    final Point p= getParentAbsPosition();
    xloc_= _x + p.x + insets.left;
    yloc_= _y + p.y + insets.top;
  }
  public void setAbsoluteLocation(final int _x, final int _y) {
    final Insets insets= topParent_.getInsets();
    xloc_= _x + insets.left;
    yloc_= _y + insets.top;
  }
  public Font getFont() {
    if (font_ == null) {
      return topParent_.getGraphics().getFont();
    }
    return font_;
  }
  public void setFont(final Font _font) {
    font_= _font;
  }
  public String getText() {
    if (text_ == null) {
      return CtuluLibString.EMPTY_STRING;
    }
    return text_;
  }
  public void setText(final String _text) {
    text_= _text;
  }
  // Methodes privees
  private Dimension getPreferredSize() {
    //    Insets      insets=topParent_.getInsets();
    final Font ft= getFont();
    String tt= getText();
    final FontMetrics fm= parent_.getFontMetrics(ft);
    int w= 0;
    int h= 0;
    while (!tt.equals(CtuluLibString.EMPTY_STRING)) {
      final int i= tt.indexOf("\n");
      String s;
      if (i >= 0) {
        s= tt.substring(0, i);
        tt= tt.substring(i + 1);
      } else {
        s= tt;
        tt= CtuluLibString.EMPTY_STRING;
      }
      //fm=ft.getLineMetrics(s, context);
      w= Math.max(w, fm.stringWidth(s));
      h += fm.getHeight();
    }
    //return new Dimension(w+insets.left+insets.right,h+insets.top+insets.bottom);
    return new Dimension(w, h);
  }
  private Point getParentAbsPosition() {
    final Point res= new Point(0, 0);
    Point plus;
    Container p= parent_;
    while (p != null) {
      plus= p.getLocation();
      res.x += plus.x;
      res.y += plus.y;
      p= p.getParent();
    }
    return res;
  }
}
