/*
 * @creation     1998-12-08
 * @modification $Date: 2007-06-28 09:28:18 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.vag;
import com.memoire.bu.BuAssistant;
/**
 * L'assistant du client Vag.
 *
 * @version      $Revision: 1.5 $ $Date: 2007-06-28 09:28:18 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class VagAssistant extends BuAssistant {}
