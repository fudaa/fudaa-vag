/*
 * @creation     2000-04-18
 * @modification $Date: 2007-06-28 09:28:18 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.vag;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.NumberFormat;

import org.fudaa.dodico.corba.vag.SResultats05;
import org.fudaa.dodico.corba.vag.SResultatsInterOrth;
/**
 * @version      $Revision: 1.6 $ $Date: 2007-06-28 09:28:18 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class VagExport {
  private static NumberFormat nf;
  static {
    nf= NumberFormat.getInstance();
    nf.setGroupingUsed(false);
    nf.setMaximumFractionDigits(3);
  }
  public static void exportResultatsIO(
    SResultatsInterOrth resultIO,
    String titre,
    String filename)
    throws IOException {
    System.err.println("export IO dans " + filename);
    PrintWriter fic= new PrintWriter(new FileOutputStream(filename));
    fic.println(
      "R�sultats de vag pour l'�tude " + titre + " (inter-orthogonales)\n");
    for (int i= 0; i < resultIO.interOrth.length; i++) {
      fic.println(
        "\nInter-orthogonale "
          + resultIO.interOrth[i].orth1
          + "-"
          + resultIO.interOrth[i].orth2
          + "\n");
      fic.println("pas X Y angle/horiz longueurOnde hauteurEau hauteurHoule");
      for (int j= 0; j < resultIO.interOrth[i].pas.length; j++) {
        fic.println(
          nf.format(resultIO.interOrth[i].pas[j].numeroPas)
            + " "
            + nf.format(resultIO.interOrth[i].pas[j].pointCourantX)
            + " "
            + nf.format(resultIO.interOrth[i].pas[j].pointCourantY)
            + " "
            + nf.format(resultIO.interOrth[i].pas[j].angleHoriz)
            + " "
            + nf.format(resultIO.interOrth[i].pas[j].longueurOnde)
            + " "
            + nf.format(resultIO.interOrth[i].pas[j].hauteurEau)
            + " "
            + nf.format(resultIO.interOrth[i].pas[j].hauteurHoule));
      }
    }
    fic.close();
  }
  public static void exportResultats05(
    SResultats05 result05,
    String titre,
    String filename)
    throws IOException {
    System.err.println("export 05 dans " + filename);
    PrintWriter fic= new PrintWriter(new FileOutputStream(filename));
    fic.println("R�sultats de vag pour l'�tude " + titre + " (orthogonales)\n");
    for (int i= 0; i < result05.orthogonales.length; i++) {
      fic.println(
        "\nOrthogonale " + result05.orthogonales[i].numeroOrth + "\n");
      fic.println("segmentDepart pas X Y angle/horiz longueurOnde hauteurEau");
      for (int j= 0; j < result05.orthogonales[i].pas.length; j++) {
        fic.println(
          nf.format(result05.orthogonales[i].pas[j].numeroSegDepart)
            + " "
            + nf.format(result05.orthogonales[i].pas[j].numeroPas)
            + " "
            + nf.format(result05.orthogonales[i].pas[j].pointCourantX)
            + " "
            + nf.format(result05.orthogonales[i].pas[j].pointCourantY)
            + " "
            + nf.format(result05.orthogonales[i].pas[j].angleHoriz)
            + " "
            + nf.format(result05.orthogonales[i].pas[j].longueurOnde)
            + " "
            + nf.format(result05.orthogonales[i].pas[j].hauteurEau));
      }
    }
    fic.close();
  }
}
