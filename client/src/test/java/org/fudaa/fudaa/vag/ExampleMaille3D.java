/*
 * @file         TestMaille3D.java
 * @creation
 * @modification $Date: 2006-10-19 14:15:26 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.vag;

import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.media.j3d.BoundingSphere;
import javax.media.j3d.BranchGroup;
import javax.media.j3d.DirectionalLight;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.vecmath.Color3f;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3f;

import com.sun.j3d.utils.applet.MainFrame;

import org.fudaa.dodico.corba.vag.SParametres01;
import org.fudaa.dodico.corba.vag.SParametres03;

import org.fudaa.dodico.vag.DParametresVag;

import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.repere.BTransformationBouton;
import org.fudaa.ebli.repere.RepereEvent;
import org.fudaa.ebli.volume.BGrilleReguliere;
import org.fudaa.ebli.volume.BUnivers;
import org.fudaa.ebli.volume.controles.BControleVolume;
import org.fudaa.ebli.volume.controles.BUniversInteraction;

/**
 * @version $Revision: 1.6 $ $Date: 2006-10-19 14:15:26 $ by $Author: deniger $
 * @author
 */
public final class ExampleMaille3D {
  private ExampleMaille3D() {}

  // Fonction main
  public static void main(final String[] _args) {
    new MainFrame(new ExampleMaille3DApplet(), 512, 512);
  }
}

class ExampleMaille3DApplet extends Applet {
  private BGrilleReguliere mer_;
  private BGrilleReguliere fond_;
  private boolean visible_;
  private JButton bt_;
  private BControleVolume cv_;
  private JPanel pnJp_;
  private JPanel pn_;
  BUnivers u_;
  private JCheckBox cb_;

  public ExampleMaille3DApplet() {
    u_ = new BUnivers();
    // u.setPreferredSize(new Dimension(256,256));
    pn_ = new JPanel();
    pn_.setLayout(new GridLayout(6, 1, 0, 0));
    for (int i = 0; i < 6; i++) {
      pn_.add(new LigneControle(i, u_));
    }
    pnJp_ = new JPanel();
    pnJp_.setLayout(new FlowLayout());
    bt_ = new JButton("Init");
    cv_ = new BControleVolume(u_);
    cv_.addRepereEventListener(new BUniversInteraction(u_));
    pnJp_.add(cv_);
    cb_ = new JCheckBox("Orbital", false);
    // JComponent[] beans=new JComponent[] { u,cb,button,cv};
    // Frame frame=new Frame();
    /*
     * frame.addWindowListener(new WindowAdapter() { public void windowClosing(WindowEvent e) { System.exit(0); } } );
     */
    // frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    // frame.setSize(new Dimension(350,350));
    setLayout(new BorderLayout());
    add("Center", u_.getCanvas3D());
    // add("South",jp);
    // add("East",button);
    // frame.setVisible(true);
    final File f1 = new File("exemples/vag/imports/gien-4051/VAG4051.01");
    final File f3 = new File("exemples/vag/imports/gien-4051/VAG4051.03");
    SParametres01 params01 = null;
    SParametres03 params03 = null;
    try {
      params01 = DParametresVag.litParametres01(f1);
    } catch (final IOException e) {
      System.out.println("Fichier 01 non trouv�");
    };
    try {
      params03 = DParametresVag.litParametres03(f3);;
    } catch (final IOException e) {
      System.out.println("Fichier 03 non trouv�");
    };
    if (params01 == null || params03 == null) return;
    for (int j = 0; j < params01.nbMaillesY + 1; j++) {
      for (int i = 0; i < params01.nbMaillesX + 1; i++) {
        if (params03.vz[i + j * (params01.nbMaillesX + 1)] > 0) {
          params03.vz[i + j * (params01.nbMaillesX + 1)] /= Math.log(params03.vz[i + j * (params01.nbMaillesX + 1)]);
        }
      }
    }
    final DirectionalLight l = new DirectionalLight(new Color3f(1f, 1f, 1f), new Vector3f(1, 1, 0));
    l.setInfluencingBounds(new BoundingSphere(new Point3d(), 1000));
    // AmbientLight l2=new AmbientLight(new Color3f((float)1,(float)1,(float)1));
    // l2.setInfluencingBounds(new BoundingSphere(new Point3d(),1000));
    final DirectionalLight l3 = new DirectionalLight(new Color3f(1f, 1f, 1f), new Vector3f(-1, 1, 0));
    l3.setInfluencingBounds(new BoundingSphere(new Point3d(), 1000));
    // PointLight l4=new PointLight();
    // l4.setInfluencingBounds(new BoundingSphere(new Point3d(),1000));
    // l4.setPosition(0,10,10);
    // l4.setDirection(0,-1,0);
    final BranchGroup lightbg = new BranchGroup();
    lightbg.addChild(l);
    // lightbg.addChild(l2);
    lightbg.addChild(l3);
    // lightbg.addChild(l4);
    u_.addBranchGraph(lightbg);
    fond_ = new BGrilleReguliere();
    fond_.setBoite(new GrPoint(-(params01.rectLargeur) / 2., -(params01.rectHauteur) / 2., 0.), new GrPoint(
        (params01.rectLargeur) / 2., (params01.rectHauteur) / 2., 1.));
    fond_.setGeometrie(params01.nbMaillesX + 1, params01.nbMaillesY + 1, params03.vz);
    fond_.setCouleur(new Color(200, 128, 64));
    // fond.setTexture(new TextureLoader("poweredbylinux.gif",frame).getTexture());
    fond_.setTransparence(0f);
    mer_ = new BGrilleReguliere();
    mer_.setBoite(new GrPoint(-(params01.rectLargeur) / 2., -(params01.rectHauteur) / 2., 0.), new GrPoint(
        (params01.rectLargeur) / 2., (params01.rectHauteur) / 2., 1.));
    final double[] zero = new double[(params01.nbMaillesX + 1) * (params01.nbMaillesY + 1)];
    for (int i = 0; i < (params01.nbMaillesX + 1) * (params01.nbMaillesY + 1); i++) {
      zero[i] = 0f;
    }
    mer_.setGeometrie(params01.nbMaillesX + 1, params01.nbMaillesY + 1, zero);
    mer_.setCouleur(Color.blue);
    mer_.setTransparence(.5f);
    // u.addObjet(mer);
    u_.addObjet(fond_);
    visible_ = true;
    // mer.setVisible(visible);
    fond_.setVisible(visible_);
    u_.getCanvas3D().freeze(false);
    bt_.addActionListener(new ActionListener() {

      public void actionPerformed(final ActionEvent _e) {
        u_.init();

      }

    });
    cb_.addItemListener(new BUniversInteraction(u_));
    u_.init();
  }
}

class LigneControle extends JPanel {
  public LigneControle(final int _s, final BUnivers _u) {
    setLayout(new FlowLayout());
    JLabel label = new JLabel();
    BTransformationBouton plus = new BTransformationBouton();
    BTransformationBouton moins = new BTransformationBouton();
    switch (_s) {
    case 0:
      plus = new BTransformationBouton(RepereEvent.ROT_X);
      moins = new BTransformationBouton(RepereEvent.ROT_X, -1);
      label = new JLabel("ROT_X");
      break;
    case 1:
      plus = new BTransformationBouton(RepereEvent.ROT_Y);
      moins = new BTransformationBouton(RepereEvent.ROT_Y, -1);
      label = new JLabel("ROT_Y");
      break;
    case 2:
      plus = new BTransformationBouton(RepereEvent.ROT_Z);
      moins = new BTransformationBouton(RepereEvent.ROT_Z, -1);
      label = new JLabel("ROT_Z");
      break;
    case 3:
      plus = new BTransformationBouton(RepereEvent.TRANS_X);
      moins = new BTransformationBouton(RepereEvent.TRANS_X, -1);
      label = new JLabel("TRAN_X");
      break;
    case 4:
      plus = new BTransformationBouton(RepereEvent.TRANS_Y);
      moins = new BTransformationBouton(RepereEvent.TRANS_Y, -1);
      label = new JLabel("TRAN_Y");
      break;
    case 5:
      plus = new BTransformationBouton(RepereEvent.TRANS_Z);
      moins = new BTransformationBouton(RepereEvent.TRANS_Z, -1);
      label = new JLabel("TRAN_Z");
      break;
    default:
    }
    add(label);
    add(plus);
    plus.addRepereEventListener(new BUniversInteraction(_u));
    add(moins);
    moins.addRepereEventListener(new BUniversInteraction(_u));
  }
}
