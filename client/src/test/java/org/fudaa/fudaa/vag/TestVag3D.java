/*
 * @file         TestVag3D.java
 * @creation     1998-08-19
 * @modification $Date: 2006-10-19 14:15:26 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.vag;

import java.awt.Color;
import java.io.File;
import java.io.IOException;

import javax.media.j3d.Texture;
import javax.vecmath.Point3d;
import javax.vecmath.Point3f;

import com.sun.j3d.utils.image.TextureLoader;

import org.fudaa.dodico.corba.objet.IConnexion;
import org.fudaa.dodico.corba.objet.IPersonne;
import org.fudaa.dodico.corba.vag.ICalculVag;
import org.fudaa.dodico.corba.vag.ICalculVagHelper;
import org.fudaa.dodico.corba.vag.IParametresVag;
import org.fudaa.dodico.corba.vag.IParametresVagHelper;
import org.fudaa.dodico.corba.vag.IResultatsVagHelper;
import org.fudaa.dodico.corba.vag.SResultatsInterOrth;

import org.fudaa.dodico.objet.CDodico;
import org.fudaa.dodico.objet.ServeurPersonne;
import org.fudaa.dodico.vag.DParametresVag;

import org.fudaa.ebli.geometrie.GrMaillage;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.volume.BEtiquette;
import org.fudaa.ebli.volume.BGrilleIrreguliere;
import org.fudaa.ebli.volume.BGrilleReguliere;
import org.fudaa.ebli.volume.FrameVolumeExample;

/**
 * Une classe client de VagServeur.
 * 
 * @version $Revision: 1.6 $ $Date: 2006-10-19 14:15:26 $ by $Author: deniger $
 * @author Axel von Arnim
 */
final class Util {

  private Util() {}

  public static GrMaillage construitMaillage(final SResultatsInterOrth _resultatsInterOrth) {
    if ((_resultatsInterOrth.interOrth == null) || (_resultatsInterOrth.interOrth.length == 0)) {
      return null;
    }
    int nb = 0;
    int pos = 0;
    final GrMaillage m = new GrMaillage();
    // calcul du nombre total de points
    for (int o = 0; o < _resultatsInterOrth.interOrth.length; o++) {
      nb += _resultatsInterOrth.interOrth[o].pas.length;
    }
    final double[] vals = new double[nb];
    // nombre de points ajoutes sur les o-2 orth precedentes
    nb = 0;
    // o=0;
    // p=0;
    m.noeuds_.ajoute(new GrPoint(_resultatsInterOrth.interOrth[0].pas[0].pointCourantX,
        _resultatsInterOrth.interOrth[0].pas[0].pointCourantY, _resultatsInterOrth.interOrth[0].pas[0].hauteurHoule));
    vals[pos] = _resultatsInterOrth.interOrth[0].pas[0].hauteurHoule;
    pos++;
    for (int p = 1; p < _resultatsInterOrth.interOrth[0].pas.length; p++) {
      m.noeuds_.ajoute(new GrPoint(_resultatsInterOrth.interOrth[0].pas[p].pointCourantX,
          _resultatsInterOrth.interOrth[0].pas[p].pointCourantY, _resultatsInterOrth.interOrth[0].pas[p].hauteurHoule));
      vals[pos] = _resultatsInterOrth.interOrth[0].pas[p].hauteurHoule;
      // pas de connectivite sur la premiere orth
      pos++;
    }
    // on ne touche pas a nb car nb totalise les points des o-2 orth prec
    for (int o = 1; o < _resultatsInterOrth.interOrth.length; o++) {
      // p=0;
      m.noeuds_.ajoute(new GrPoint(_resultatsInterOrth.interOrth[o].pas[0].pointCourantX,
          _resultatsInterOrth.interOrth[o].pas[0].pointCourantY, _resultatsInterOrth.interOrth[o].pas[0].hauteurHoule));
      vals[pos] = _resultatsInterOrth.interOrth[o].pas[0].hauteurHoule;
      // pas de connectivite pour le premier pas
      pos++;
      for (int p = 1; p < _resultatsInterOrth.interOrth[o].pas.length; p++) {
        m.noeuds_
            .ajoute(new GrPoint(_resultatsInterOrth.interOrth[o].pas[p].pointCourantX,
                _resultatsInterOrth.interOrth[o].pas[p].pointCourantY,
                _resultatsInterOrth.interOrth[o].pas[p].hauteurHoule));
        vals[pos] = _resultatsInterOrth.interOrth[o].pas[p].hauteurHoule;
        // test: le point p de l'orth prec existe-t-il?
        if (p < _resultatsInterOrth.interOrth[o - 1].pas.length) {
          final int[] c = new int[4];
          // p-1 sur l'orth prec
          c[0] = nb + p - 1;
          // p sur l'orth prec
          c[1] = nb + p;
          // p sur l'orth cour
          c[2] = pos;
          // p-1 sur l'orth cour
          c[3] = pos - 1;
          m.connectivites_.add(c);
        }
        pos++;
      }
      nb += _resultatsInterOrth.interOrth[o - 1].pas.length;
    }
    return m;
  }
}

public final class TestVag3D {
  private TestVag3D() {}

  public static void main(final String[] _args) {
    ICalculVag vag = null;
    System.out.println("VagClient");
    IParametresVag p = null;
    // do
    // {
    // System.err.println("### essai "+(++essai));
    // try { vag=(ICalculVag)CDodico.findServer("::vag::ICalculVag"); }
    try {
      vag = ICalculVagHelper.narrow(CDodico.findServerByName("un-serveur-vag"));
      // } while(vag==null);
      System.out.println("Trouv� : " + vag);
      System.out.println("Creation de la connexion");
      final IPersonne sp = ServeurPersonne.createPersonne("test-personne-vag", "test-organisme-vag");
      System.out.println("Connexion au serveur Vag : " + vag);
      final IConnexion c = vag.connexion(sp);
      p = IParametresVagHelper.narrow(vag.parametres(c));
      try {
        p.parametres01(DParametresVag.litParametres01(new File("exemples/vag/imports/gien-4051/VAG4051.01")));
        p.parametres02(DParametresVag.litParametres02(new File("exemples/vag/imports/gien-4051/VAG4051.02")));
        p.parametres03(DParametresVag.litParametres03(new File("exemples/vag/imports/gien-4051/VAG4051.03")));
        p.parametres04(DParametresVag.litParametres04(new File("exemples/vag/imports/gien-4051/VAG4051.04")));
      } catch (final IOException e) {
        System.out.println("Fichiers non trouv�s");
      }
      vag.calcul(c);
      final SResultatsInterOrth resultats = IResultatsVagHelper.narrow(vag.resultats(c)).resultatsInterOrth();
      // SResultatsInterOrth resultats=vag.resultats()).resultatsInterOrth();
      final GrMaillage m = Util.construitMaillage(resultats);
      System.out.println(m);
      final int nbNoeuds = m.noeuds_.nombre();
      // Construction des noeuds (format J3D)
      final Point3d[] points = new Point3d[nbNoeuds];
      for (int i = 0; i < nbNoeuds; i++) {
        // System.out.println(m.noeuds.renvoie(i));
        points[i] = new Point3d(m.noeuds_.renvoie(i).x_, m.noeuds_.renvoie(i).y_, m.noeuds_.renvoie(i).z_);
      }
      final int nbConnect = m.connectivites_.size();
      // Construction des triangles
      final int[] triangles = new int[nbConnect * 3 * 2];
      for (int i = 0; i < nbConnect; i++) {
        // premier triangle: points 0,1,2
        triangles[6 * i + 0] = ((int[]) m.connectivites_.get(i))[0];
        triangles[6 * i + 1] = ((int[]) m.connectivites_.get(i))[1];
        triangles[6 * i + 2] = ((int[]) m.connectivites_.get(i))[2];
        // premier triangle: points 1,2,3
        triangles[6 * i + 3] = ((int[]) m.connectivites_.get(i))[2];
        triangles[6 * i + 4] = ((int[]) m.connectivites_.get(i))[3];
        triangles[6 * i + 5] = ((int[]) m.connectivites_.get(i))[0];
      }
      final BGrilleIrreguliere irr = new BGrilleIrreguliere("Houle");
      final FrameVolumeExample frame = new FrameVolumeExample();
      frame.addEchelleListener(irr);
      final double[] z = new double[nbNoeuds];
      for (int i = 0; i < nbNoeuds; i++) {
        z[i] = points[i].z;
      }
      frame.addPaletteListener(irr, z);
      irr.setBoite(m.boite());
      irr.setGeometrie(nbNoeuds, points, triangles.length, triangles);
      irr.setTransparence(.5f);
      irr.setCouleur(Color.blue);
      irr.setEchelleZ(50);
      final BGrilleReguliere fond = new BGrilleReguliere("Fond");
      // fond.setBoite(new GrPoint(-(p.parametres01().rectLargeur)/2.,-(p.parametres01().rectHauteur)/2.,0.),new
      // GrPoint((p.parametres01().rectLargeur)/2.,(p.parametres01().rectHauteur)/2.,1.));
      fond.setBoite(new GrPoint(0, 0, 0.), new GrPoint((p.parametres01().rectLargeur), (p.parametres01().rectHauteur),
          1.));
      fond.setGeometrie(p.parametres01().nbMaillesX + 1, p.parametres01().nbMaillesY + 1, p.parametres03().vz);
      fond.setCouleur(new Color(255, 200, 100));
      // fond.setTexture(new TextureLoader("poweredbylinux.gif",frame).getTexture());
      fond.setTransparence(0);
      final BGrilleReguliere mer = new BGrilleReguliere("Mer");
      // mer.setBoite(new GrPoint(-(params01.rectLargeur)/2.,-(params01.rectHauteur)/2.,0.),new
      // GrPoint((params01.rectLargeur)/2.,(params01.rectHauteur)/2.,1.));
      // System.out.println("irr.getBoite() :"+irr.getBoite().o+" - "+irr.getBoite().e);
      // mer.setBoite(new GrPoint(-(p.parametres01().rectLargeur)/2.,-(p.parametres01().rectHauteur)/2.,0.),new
      // GrPoint((p.parametres01().rectLargeur)/2.,(p.parametres01().rectHauteur)/2.,1.));
      mer.setBoite(new GrPoint(0, 0, 0.), new GrPoint((p.parametres01().rectLargeur), (p.parametres01().rectHauteur),
          1.));
      final double[] zero = new double[4];
      for (int i = 0; i < 4; i++) {
        zero[i] = -.1f;
      }
      mer.setGeometrie(2, 2, zero);
      mer.setCouleur(Color.blue);
      mer.setTransparence(.5f);
      final Texture t = new TextureLoader("map5.gif", frame).getTexture();
      t.setCapability(Texture.ALLOW_ENABLE_WRITE);
      t.setBoundaryColor(0f, .9f, 0f, 0f);
      t.setEnable(true);
      // i.setTexture(new TextureLoader("map5.gif",frame).getTexture());
      fond.setTexture(t);
      // i.setTransparence(.5f);
      // i.setFilaire();
      // fond.setEclairage(true);
      System.out.println("Creation d'une BEtiquette");
      final BEtiquette etiquette = new BEtiquette("Test", new Point3f(0, 0, 0), Color.red);
      System.out.println("Boite");
      etiquette.setBoite(new GrPoint(-1, -1, -1), new GrPoint(1, 1, 1));
      frame.getUnivers().addObjet(mer);
      frame.getUnivers().addObjet(irr);
      frame.getUnivers().addObjet(fond);
      System.out.println("Ajout a l'univers");
      frame.getUnivers().addObjet(etiquette);
      mer.setVisible(true);
      mer.setRapide(false);
      irr.setVisible(true);
      irr.setRapide(false);
      fond.setVisible(true);
      fond.setRapide(false);
      System.out.println("Visible");
      etiquette.setVisible(true);
      System.out.println("Pas Rapide");
      etiquette.setRapide(false);
      frame.getUnivers().init();
      frame.setVisible(true);
    } catch (final Exception exc) {
      System.out.println("Exception " + exc);
    }
  }
}
