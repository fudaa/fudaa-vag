/**
 * @creation     2000-02-16
 * @modification $Date: 2007-03-27 16:09:55 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.vag;
import java.util.Date;

import org.fudaa.dodico.objet.CDodico;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Une classe serveur pour Vag.
 *
 * @version      $Revision: 1.13 $ $Date: 2007-03-27 16:09:55 $ by $Author: deniger $
 * @author       Guillaume Desnoix 
 */
public final class ServeurVag {
  
  private ServeurVag(){
    
  }
  /**
   * @param _args utilise si present pour le nom du serveur.
   */
  public static void main(final String[] _args) {
    final String nom=
      (_args.length > 0 ? _args[0] : CDodico.generateName("::vag::ICalculVag"));
    //Cas particulier : il s'agit de creer un serveur de calcul dans une jvm donne
    //Cette M�thode n'est pas a imiter. If faut utiliser Boony pour creer des objet corba.
    CDodico.rebind(nom, UsineLib.createService(DCalculVag.class));
    System.out.println("Vag server running... ");
    System.out.println("Name: " + nom);
    System.out.println("Date: " + new Date());
  }
}
