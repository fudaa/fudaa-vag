/**
 * @creation 1998-08-11
 * @modification $Date: 2007-03-27 16:09:55 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
* @mail devel@fudaa.fr
 */
package org.fudaa.dodico.vag;

import java.io.File;
import java.io.IOException;

import org.fudaa.dodico.corba.objet.IConnexion;
import org.fudaa.dodico.corba.vag.ICalculVag;
import org.fudaa.dodico.corba.vag.ICalculVagOperations;
import org.fudaa.dodico.corba.vag.IParametresVag;
import org.fudaa.dodico.corba.vag.IParametresVagHelper;
import org.fudaa.dodico.corba.vag.IResultatsVag;
import org.fudaa.dodico.corba.vag.IResultatsVagHelper;

import org.fudaa.dodico.calcul.DCalcul;
import org.fudaa.dodico.objet.CDodico;
import org.fudaa.dodico.objet.CExec;
/**
 * Classe Calcul de Vag.
 * @version $Revision: 1.12 $ $Date: 2007-03-27 16:09:55 $ by $Author: deniger $
 * @author Axel von Arnim
 */
public class DCalculVag extends DCalcul implements ICalculVag, ICalculVagOperations {

  /**
   * Initialise les extensions des fichiers.
   */
  public DCalculVag() {
    super();
    setFichiersExtensions(4, new String[] { ".01", ".02", ".03", ".04", ".05", ".08", ".out",
        ".in", ".lpt"});
  }

  public void calcul(final IConnexion c){
    if (!verifieConnexion(c)) {
      return;
    }
    final IParametresVag params = IParametresVagHelper.narrow(parametres(c));
    if (params == null) {
      CDodico.exceptionAxel(this, new Exception("params non definis (null)"));
    }
    final IResultatsVag results = IResultatsVagHelper.narrow(resultats(c));
    if (results == null) {
      CDodico.exceptionAxel(this, new Exception("results non definis (null)"));
    }
    if ((params == null) || (params.parametres01() == null) || (params.parametres02() == null)
        || (params.parametres03() == null) || (params.parametres04() == null)) {
      CDodico.exceptionAxel(this, new Exception("params non definis (null)"));
    }
    log(c, "lancement du calcul");
    final String os = System.getProperty("os.name");
    final String path = cheminServeur();
    try {
      System.out.println("Ecriture des fichiers parametres");
      final File fic01 = getFichier(c, ".01");
      final File fic02 = getFichier(c, ".02");
      final File fic03 = getFichier(c, ".03");
      final File fic04 = getFichier(c, ".04");
      final File fic05 = getFichier(c, ".05");
      //      File fic08=getFichier(c, ".06");
      //      File ficOUT=getFichier(c, ".out");
      //      File ficLPT=getFichier(c, ".lpt");
      final String noEtude = fic01.getName().substring(3, 7);
      DParametresVag.ecritParametres01(fic01, params.parametres01(), noEtude);
      DParametresVag.ecritParametres02(fic02, params.parametres02());
      DParametresVag.ecritParametres03(fic03, params.parametres03());
      DParametresVag.ecritParametres04(fic04, params.parametres04());
      results.setFichier(path, noEtude);
      results.setPasImpression(params.parametres01().codeImp);
      System.out.println("Appel de l'executable vag pour l'etude " + noEtude);
      String[] cmd;
      if (os.startsWith("Windows")) {
        cmd = new String[4];
        cmd[0] = path + "vag-win.bat";
        if (path.indexOf(':') != -1) {
          // lettre de l'unite (ex: "C:")
          cmd[1] = path.substring(0, path.indexOf(':') + 1);
          // chemin du serveur
          cmd[2] = path.substring(path.indexOf(':') + 1);
        }
        else {
          // si pas de lettre dans le chemin
          cmd[1] = "fake_cmd";
          cmd[2] = path;
        }
        cmd[3] = noEtude;
        System.out.println(cmd[0] + " " + cmd[1] + " " + cmd[2] + " " + cmd[3]);
      }
      else {
        cmd = new String[3];
        cmd[0] = path + "vag.sh";
        cmd[1] = path;
        cmd[2] = noEtude;
        System.out.println(cmd[0] + " " + cmd[1] + " " + cmd[2]);
      }
      final CExec ex = new CExec();
      ex.setCommand(cmd);
      ex.setOutStream(System.out);
      ex.setErrStream(System.err);
      ex.exec();
      System.out.println("Fin du calcul");
      System.out.println("Les orthogonales sont dans " + fic05.getName());
      log(c, "calcul termin�");
    }
    catch (final IOException e) {
      System.out.println("***************************************");
      log(c, "erreur du calcul");
      System.err.println(e);
      CDodico.exceptionAxel(this, e);
    }
    catch (final Exception ex) {
      System.out.println("***************************************");
      log(c, "erreur du calcul");
      CDodico.exceptionAxel(this, ex);
    }
  }

  public Object clone(){
    return new DCalculVag();
  }

  public String description(){
    return "Vag, serveur de calcul pour la houle: " + super.description();
  }

  public String toString(){
    return "DCalculVag()";
  }
}