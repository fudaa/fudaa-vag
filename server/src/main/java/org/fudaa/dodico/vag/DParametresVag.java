/**
 * @creation 1998-08-12
 * @modification $Date: 2007-03-27 16:09:55 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.vag;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.dodico.corba.vag.IParametresVag;
import org.fudaa.dodico.corba.vag.IParametresVagOperations;
import org.fudaa.dodico.corba.vag.SParametres01;
import org.fudaa.dodico.corba.vag.SParametres02;
import org.fudaa.dodico.corba.vag.SParametres03;
import org.fudaa.dodico.corba.vag.SParametres04;
import org.fudaa.dodico.corba.vag.SParametresLigne02;

import org.fudaa.dodico.calcul.DParametres;
import org.fudaa.dodico.fichiers.NativeBinaryInputStream;
import org.fudaa.dodico.fichiers.NativeBinaryOutputStream;
import org.fudaa.dodico.fortran.FortranReader;
import org.fudaa.dodico.fortran.FortranWriter;
/**
 * Les parametres Vag.
 * @version $Revision: 1.15 $ $Date: 2007-03-27 16:09:55 $ by $Author: deniger $
 * @author Axel von Arnim
 */
public class DParametresVag extends DParametres implements IParametresVag, IParametresVagOperations {

  private SParametres01 params01_;
  private SParametres02 params02_;
  private SParametres03 params03_;
  private SParametres04 params04_;

  /**
   * Ne fait rien.
   */
  public DParametresVag() {
    super();
  }

  public Object clone(){
    return new DParametresVag();
  }

  public String toString(){
    return "DParametresVag()";
  }

  // 01
  public SParametres01 parametres01(){
    return params01_;
  }

  public void parametres01(final SParametres01 _p){
    params01_ = _p;
  }

  /**
   * @param _f01 flux cible
   * @param _params parametre a ecrire
   * @param _noEtude num etude
   * @throws IOException
   */
  public static void ecritParametres01(final OutputStream _f01,final SParametres01 _params,final String _noEtude)
      throws IOException{
    System.out.println("Ecriture des parametres 01");
    final FortranWriter f01 = new FortranWriter(new OutputStreamWriter(_f01));
    f01.setLineSeparator(CtuluLibString.LINE_SEP);
    final String vide = "";
    int[] fmtI;
    int[] fmtD;
    fmtI = new int[] { 40, 36, 4};
    f01.stringField(0, _params.titreEtude);
    f01.stringField(1, _params.codeInconnu);
    f01.stringField(2, _noEtude + "");
    f01.writeFields(fmtI);
    fmtD = new int[] { 10, 10, 10, 10, 40};
    fmtI = new int[] { 5, 5, 5, 25, 40};
    f01.doubleField(0, _params.echelle);
    f01.stringField(1, vide); // remplissage des colonnes vides
    f01.stringField(2, vide);
    f01.stringField(3, vide);
    f01.stringField(4, "ECHELLE DE LA CARTE");
    f01.writeFields(fmtD);
    f01.doubleField(0, _params.rectLargeur);
    f01.doubleField(1, _params.rectHauteur);
    f01.stringField(2, vide);
    f01.stringField(3, vide);
    f01.stringField(4, "DIMENSION DU RECTANGLE");
    f01.writeFields(fmtD);
    f01.intField(0, _params.terminal);
    f01.intField(1, _params.digitaliseur);
    f01.intField(2, _params.traceur);
    f01.stringField(3, vide);
    f01.stringField(4, "TERMINAL, DIGITALISEUR ET TRACEUR");
    f01.writeFields(fmtI);
    f01.intField(0, _params.nbMaillesX);
    f01.intField(1, _params.nbMaillesY);
    f01.stringField(2, vide);
    f01.stringField(3, vide);
    f01.stringField(4, "NOMBRE DE MAILLES SUIVANT OX ET OY");
    f01.writeFields(fmtI);
    f01.doubleField(0, _params.segArretDebutX);
    f01.doubleField(1, _params.segArretDebutY);
    f01.stringField(2, vide);
    f01.stringField(3, vide);
    f01.stringField(4, "COOR. DE DEBUT DU SEGMENT D'ARRET");
    f01.writeFields(fmtD);
    f01.doubleField(0, _params.segArretFinX);
    f01.doubleField(1, _params.segArretFinY);
    f01.stringField(2, vide);
    f01.stringField(3, vide);
    f01.stringField(4, "COOR. DE FIN DU SEGMENT D'ARRET");
    f01.writeFields(fmtD);
    f01.doubleField(0, _params.flecheNordX);
    f01.doubleField(1, _params.flecheNordY);
    f01.doubleField(2, _params.flecheNordDir);
    f01.stringField(3, vide);
    f01.stringField(4, "COOR. ET DIREC. DE LA FLECHE NORD");
    f01.writeFields(fmtD);
    f01.intField(0, _params.codeImp);
    f01.intField(1, _params.pasCalcul);
    f01.stringField(2, vide);
    f01.stringField(3, vide);
    f01.stringField(4, "CODE D'IMP. ET PAS DE CALCUL");
    f01.writeFields(fmtI);
    f01.doubleField(0, _params.echTracePapier);
    f01.stringField(1, vide);
    f01.stringField(2, vide);
    f01.stringField(3, vide);
    f01.stringField(4, "ECHELLE DES TRACES SUR PAPIER");
    f01.writeFields(fmtD);
    f01.doubleField(0, _params.coinInfGX);
    f01.doubleField(1, _params.coinInfGY);
    f01.stringField(2, vide);
    f01.stringField(3, vide);
    f01.stringField(4, "COOR. DU COIN INFERIEUR GAUCHE");
    f01.writeFields(fmtD);
    f01.doubleField(0, _params.coinInfDX);
    f01.doubleField(1, _params.coinInfDY);
    f01.stringField(2, vide);
    f01.stringField(3, vide);
    f01.stringField(4, "COOR. DU COIN INFERIEUR DROIT");
    f01.writeFields(fmtD);
    f01.doubleField(0, _params.cadreTracesHauteur);
    f01.stringField(1, vide);
    f01.stringField(2, vide);
    f01.stringField(3, vide);
    f01.stringField(4, "HAUTEUR DU CADRE DES TRACES");
    f01.writeFields(fmtD);
    f01.intField(0, _params.cartoucheTracesPosition);
    f01.stringField(1, vide);
    f01.stringField(2, vide);
    f01.stringField(3, vide);
    f01.stringField(4, "POSITION DU CARTOUCHE DES TRACES");
    f01.writeFields(fmtI);
    f01.flush();
  }

  /**
   * @param _f01 cible
   * @param _params param
   * @param _noEtude num etude.
   * @throws IOException
   */
  public static void ecritParametres01(final File _f01,final SParametres01 _params,final String _noEtude)
      throws IOException{
    final FileOutputStream fichier = new FileOutputStream(_f01);
    ecritParametres01(fichier, _params, _noEtude);
    fichier.close();
  }

  /**
   * @param _f01 flux a lire
   * @return parametres lus.
   * @throws IOException
   */
  public static SParametres01 litParametres01(final InputStream _f01) throws IOException{
    System.out.println("Lecture des parametres 01");
    final SParametres01 params = new SParametres01();
    final FortranReader f01 = new FortranReader(new InputStreamReader(_f01));
    int[] fmtI;
    int[] fmtD;
    fmtI = new int[] { 40, 36, 4};
    f01.readFields(fmtI);
    params.titreEtude = f01.stringField(0);
    params.codeInconnu = f01.stringField(1);
    fmtD = new int[] { 10, 10, 10, 10, 40};
    fmtI = new int[] { 5, 5, 5, 25, 40};
    f01.readFields(fmtD);
    params.echelle = f01.doubleField(0);
    f01.readFields(fmtD);
    params.rectLargeur = f01.doubleField(0);
    params.rectHauteur = f01.doubleField(1);
    f01.readFields(fmtI);
    params.terminal = f01.intField(0);
    params.digitaliseur = f01.intField(1);
    params.traceur = f01.intField(2);
    f01.readFields(fmtI);
    params.nbMaillesX = f01.intField(0);
    params.nbMaillesY = f01.intField(1);
    f01.readFields(fmtD);
    params.segArretDebutX = f01.doubleField(0);
    params.segArretDebutY = f01.doubleField(1);
    f01.readFields(fmtD);
    params.segArretFinX = f01.doubleField(0);
    params.segArretFinY = f01.doubleField(1);
    f01.readFields(fmtD);
    params.flecheNordX = f01.doubleField(0);
    params.flecheNordY = f01.doubleField(1);
    params.flecheNordDir = f01.doubleField(2);
    f01.readFields(fmtI);
    params.codeImp = f01.intField(0);
    params.pasCalcul = f01.intField(1);
    f01.readFields(fmtD);
    params.echTracePapier = f01.doubleField(0);
    f01.readFields(fmtD);
    params.coinInfGX = f01.doubleField(0);
    params.coinInfGY = f01.doubleField(1);
    f01.readFields(fmtD);
    params.coinInfDX = f01.doubleField(0);
    params.coinInfDY = f01.doubleField(1);
    f01.readFields(fmtD);
    params.cadreTracesHauteur = f01.doubleField(0);
    f01.readFields(fmtI);
    params.cartoucheTracesPosition = f01.intField(0);
    return params;
  }

  /**
   * @param _f01 fichier 01 a lire
   * @return parametres 01.
   * @throws IOException
   */
  public static SParametres01 litParametres01(final File _f01) throws IOException{
    SParametres01 res = null;
    final FileInputStream fichier = new FileInputStream(_f01);
    res = litParametres01(fichier);
    fichier.close();
    return res;
  }

  // 02
  public SParametres02 parametres02(){
    return params02_;
  }

  public void parametres02(final SParametres02 _p){
    params02_ = _p;
  }

  /**
   * @param _f02 fichier O2
   * @param _params les parametres 02
   * @throws IOException
   */
  public static void ecritParametres02(final OutputStream _f02,final SParametres02 _params) throws IOException{
    System.out.println("Ecriture des parametres 02");
    final FortranWriter f02 = new FortranWriter(new OutputStreamWriter(_f02));
    f02.setLineSeparator("\r\n"); // fichiers DOS (\r)
    int[] fmt;
    fmt = new int[] { 5, 20};
    f02.intField(0, _params.nbLignes);
    f02.stringField(1, "                    ");
    f02.writeFields(fmt);
    fmt = new int[] { 5, 10, 10};
    for (int i = 0; i < _params.nbLignes - 1; i++) {
      f02.intField(0, _params.xy[i].trait);
      f02.doubleField(1, _params.xy[i].x);
      f02.doubleField(2, _params.xy[i].y);
      f02.writeFields(fmt);
    }
    f02.flush();
  }

  /**
   * @param _f02 fichier 02 a ecrire
   * @param _params les parametres sources
   * @throws IOException
   */
  public static void ecritParametres02(final File _f02,final SParametres02 _params) throws IOException{
    final FileOutputStream fichier = new FileOutputStream(_f02);
    ecritParametres02(fichier, _params);
    fichier.close();
  }

  /**
   * @param _f02 flux a lire
   * @return parametres lus.
   * @throws IOException
   */
  public static SParametres02 litParametres02(final InputStream _f02) throws IOException{
    System.out.println("Lecture des parametres 02");
    final SParametres02 params = new SParametres02();
    final FortranReader f02 = new FortranReader(new InputStreamReader(_f02));
    f02.readFields();
    params.nbLignes = f02.intField(0);
    if (params.nbLignes == 0) {
      params.nbLignes = 1;
    }
    params.xy = new SParametresLigne02[params.nbLignes - 1];
    for (int i = 0; i < params.nbLignes - 1; i++) {
      f02.readFields();
      params.xy[i] = new SParametresLigne02();
      params.xy[i].trait = f02.intField(0);
      params.xy[i].x = f02.doubleField(1);
      params.xy[i].y = f02.doubleField(2);
    }
    return params;
  }

  /**
   * @param _f02 fichier a lire
   * @return parametres lus.
   * @throws IOException
   */
  public static SParametres02 litParametres02(final File _f02) throws IOException{
    SParametres02 res = null;
    final FileInputStream fichier = new FileInputStream(_f02);
    res = litParametres02(fichier);
    fichier.close();
    return res;
  }

  // 03
  public SParametres03 parametres03(){
    return params03_;
  }

  public void parametres03(final SParametres03 _p){
    params03_ = _p;
  }

  /**
   * @param _f03 fichier cible
   * @param _params les parametres a ecrire
   * @throws IOException
   */
  public static void ecritParametres03(final OutputStream _f03,final SParametres03 _params) throws IOException{
    System.out.println("Ecriture des parametres 03");
    // ATTENTION: le flux genere est incompatible avec VAG.
    //            Utiliser ecritParametres03(File _f03, SParametres03 _params) pour VAG.
    //            En debut du flux figure un entier natif qui donne
    //            le nombre de flottants a ecrire.
    final NativeBinaryOutputStream f03 = new NativeBinaryOutputStream(_f03, getOsArch());
    f03.writeInt32(_params.vz.length);
    for (int i = 0; i < _params.vz.length; i++) {
      f03.writeFloat32((float) _params.vz[i]);
    }
  }

  private static String getOsArch() {
    return System.getProperty("os.arch");
      }

  /**
   * @param _f03 le fichier a cree
   * @param _params les parametres sources
   * @throws IOException
   */
  public static void ecritParametres03(final File _f03,final SParametres03 _params) throws IOException{
    System.out.println("Ecriture des parametres 03");
    final NativeBinaryOutputStream f03 = new NativeBinaryOutputStream(new FileOutputStream(_f03), getOsArch());
    for (int i = 0; i < _params.vz.length; i++) {
      f03.writeFloat32((float) _params.vz[i]);
    }
    f03.close();
    }

  /**
   * @param _f03 le fichier a lire
   * @return les parametres lus.
   * @throws IOException
   */
  public static SParametres03 litParametres03(final InputStream _f03) throws IOException{
    System.out.println("Lecture des parametres 03");
    final SParametres03 params = new SParametres03();
    // ATTENTION: le flux lu est incompatible avec les fichiers VAG.
    //            Utiliser litParametres03(File _f03) pour VAG.
    //            En debut du stream doit figurer un entier Java indiquant
    //            le nombre de flottants a lire.
    final NativeBinaryInputStream f03 = new NativeBinaryInputStream(_f03, getOsArch());
    params.vz = new double[f03.readInt32()];
    for (int i = 0; i < params.vz.length; i++) {
      params.vz[i] = f03.readFloat32();
      //if( Math.abs(params.vz[i])>20000 ) // Test de validite des donnees
      //  throw new StreamCorruptedException("hauteur invalide: pos "+i+", valeur "+params.vz[i]);
    }
    return params;
  }

  /**
   * @param _f03 le fichier a lire
   * @return structure resultat
   * @throws IOException
   */
  public static SParametres03 litParametres03(final File _f03) throws IOException{
    System.out.println("Lecture des parametres 03");
    final SParametres03 params = new SParametres03();
    final NativeBinaryInputStream f03 = new NativeBinaryInputStream(new FileInputStream(_f03), getOsArch());
    params.vz = new double[(int) _f03.length() / 4]; // Un float=4 octets
    for (int i = 0; i < params.vz.length; i++) {
      params.vz[i] = f03.readFloat32();
      //if( Math.abs(params.vz[i])>20000 ) // Test de validite des donnees
      //  throw new StreamCorruptedException("hauteur invalide: pos "+i+", valeur "+params.vz[i]);
    }
    f03.close();
    return params;
  }

  // 04
  public SParametres04 parametres04(){
    return params04_;
  }

  public void parametres04(final SParametres04 _p){
    params04_ = _p;
  }

  /**
   * @param _f04 le fichier a ecrire
   * @param _params les parametres source pour ce fichier
   * @throws IOException
   */
  public static void ecritParametres04(final OutputStream _f04,final SParametres04 _params) throws IOException{
    System.out.println("Ecriture des parametres 04");
    final FortranWriter f04 = new FortranWriter(new OutputStreamWriter(_f04));
    f04.setLineSeparator("\r\n"); // fichiers DOS (\r)
    int[] fmtI;
    int[] fmtD;
    fmtD = new int[] { 10, 10, 10, 10, 40};
    fmtI = new int[] { 5, 5, 5, 25, 40};
    f04.intField(0, _params.numeroHoule);
    f04.stringField(1, ""); // remplissage des colonnes vides
    f04.stringField(2, "");
    f04.stringField(3, "");
    f04.stringField(4, "NOUVELLE HOULE");
    f04.writeFields(fmtI);
    f04.doubleField(0, _params.periode);
    f04.stringField(1, "");
    f04.stringField(2, "");
    f04.stringField(3, "");
    f04.stringField(4, "PERIODE DE LA HOULE");
    f04.writeFields(fmtD);
    f04.doubleField(0, _params.hauteurMer);
    f04.stringField(1, "");
    f04.stringField(2, "");
    f04.stringField(3, "");
    f04.stringField(4, "HAUTEUR DE LA MER");
    f04.writeFields(fmtD);
    f04.intField(0, _params.nbOrthogonales);
    f04.stringField(1, "");
    f04.stringField(2, "");
    f04.stringField(3, "");
    f04.stringField(4, "NOMBRE D'ORTHOGONALES");
    f04.writeFields(fmtI);
    f04.doubleField(0, _params.angleHouleHoriz);
    f04.stringField(1, "");
    f04.stringField(2, "");
    f04.stringField(3, "");
    f04.stringField(4, "ANGLE DE LA HOULE AVEC L'HORIZONTALE");
    f04.writeFields(fmtD);
    f04.doubleField(0, _params.espacementOrth);
    f04.stringField(1, "");
    f04.stringField(2, "");
    f04.stringField(3, "");
    f04.stringField(4, "ESPACEMENT ENTRE LES ORTHOGONALES");
    f04.writeFields(fmtD);
    f04.doubleField(0, _params.premiereOrthDepartX);
    f04.doubleField(1, _params.premiereOrthDepartY);
    f04.stringField(2, "");
    f04.stringField(3, "");
    f04.stringField(4, "COOR. DE DEPART DE LA 1ERE ORTHOGONALE");
    f04.writeFields(fmtD);
    f04.flush();
  }

  /**
   * @param _f04 le fichier a ecrire
   * @param _params les parametres sources
   * @throws IOException
   */
  public static void ecritParametres04(final File _f04,final SParametres04 _params) throws IOException{
    final FileOutputStream fichier = new FileOutputStream(_f04);
    ecritParametres04(fichier, _params);
    fichier.close();
  }

  /**
   * @param  _f04 le flux a lire.
   * @return les parametres lus.
   * @throws IOException
   */
  public static SParametres04 litParametres04(final InputStream _f04) throws IOException{
    System.out.println("Lecture des parametres 04");
    final SParametres04 params = new SParametres04();
    final FortranReader f04 = new FortranReader(new InputStreamReader(_f04));

    int[] fmtI;
    int[] fmtD;
    fmtD = new int[] { 10, 10, 10, 10, 40};
    fmtI = new int[] { 5, 5, 5, 25, 40};
    f04.readFields(fmtI);
    params.numeroHoule = f04.intField(0);
    f04.readFields(fmtD);
    params.periode = f04.doubleField(0);
    f04.readFields(fmtD);
    params.hauteurMer = f04.doubleField(0);
    f04.readFields(fmtI);
    params.nbOrthogonales = f04.intField(0);
    f04.readFields(fmtD);
    params.angleHouleHoriz = f04.doubleField(0);
    f04.readFields(fmtD);
    params.espacementOrth = f04.doubleField(0);
    f04.readFields(fmtD);
    params.premiereOrthDepartX = f04.doubleField(0);
    params.premiereOrthDepartY = f04.doubleField(1);
    return params;
  }

  /**
   * @param _f04 le fichier a lire.
   * @return les structures lues.
   * @throws IOException
   */
  public static SParametres04 litParametres04(final File _f04) throws IOException{
    SParametres04 res = null;
    final FileInputStream fichier = new FileInputStream(_f04);
    res = litParametres04(fichier);
    fichier.close();
    return res;
  }
}