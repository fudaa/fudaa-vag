/**
 * @file DResultatsVag.java
 * @creation 1998-03-12
 * @modification $Date: 2007-03-27 16:09:55 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.org
 */
package org.fudaa.dodico.vag;

import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;

import org.fudaa.dodico.corba.vag.IResultatsVag;
import org.fudaa.dodico.corba.vag.IResultatsVagOperations;
import org.fudaa.dodico.corba.vag.SResultatPasCalcul05;
import org.fudaa.dodico.corba.vag.SResultatPasInterOrth;
import org.fudaa.dodico.corba.vag.SResultatUnitaire05;
import org.fudaa.dodico.corba.vag.SResultatUnitaireInterOrth;
import org.fudaa.dodico.corba.vag.SResultats05;
import org.fudaa.dodico.corba.vag.SResultatsInterOrth;
import org.fudaa.dodico.corba.vag.SResultatsOUT;

import org.fudaa.dodico.calcul.DResultats;
import org.fudaa.dodico.fichiers.NativeBinaryInputStream;
import org.fudaa.dodico.mathematiques.CMathReel;
import org.fudaa.dodico.objet.CDodico;
/**
 * Les resultats Lido.
 * @version $Revision: 1.12 $ $Date: 2007-03-27 16:09:55 $ by $Author: deniger $
 * @author Axel von Arnim
 */
public class DResultatsVag extends DResultats implements IResultatsVag, IResultatsVagOperations {

  private String noEtude_;
  private String path_;
  private int pasImp_;
  private SResultats05 results05_;
  private SResultatsInterOrth resultsInterOrth_;
  private SResultatsOUT resultsOUT_;

  /**
   * pasImpression et boolean.
   */
  public DResultatsVag() {
    super();
    pasImp_ = 1;
    dirty05_ = true;
    dirtyIO_ = true;
    dirtyOUT_ = true;
  }

  public void setFichier(final String _path,final String _noEtude){
    path_ = _path;
    noEtude_ = _noEtude;
  }

  // Object
  public Object clone(){
    final DResultatsVag r = new DResultatsVag();
    r.setFichier(path_, noEtude_);
    return r;
  }

  public String toString(){
    return "DResultatsVag(" + noEtude_ + ")";
  }

  public void setPasImpression(final int _pas){
    pasImp_ = _pas;
    setDirty();
  }

  boolean dirty05_;
  boolean dirtyIO_;
  boolean dirtyOUT_;

  /**
   * @return true si pas a jour
   */
  public boolean isDirty(){
    return dirty05_ || dirtyIO_ || dirtyOUT_;
  }

  /**
   * Met tous les booleens a true.
   */
  public void setDirty(){
    dirty05_ = true;
    dirtyIO_ = true;
    dirtyOUT_ = true;
  }

  // 05
  private void litResultats05(){
    System.out.println("Lecture des resultats 05 (pas d'impression " + pasImp_ + ")");
    try {
      final NativeBinaryInputStream f05 = new NativeBinaryInputStream(new FileInputStream(path_ + "vag"
        + noEtude_ + ".05"), System.getProperty("os.arch"));
      results05_ = new SResultats05();
      final int nbPas = f05.readInt32() - 1;
      final int[] orthogonales = new int[nbPas + 1];
      orthogonales[0] = 0;
      int nbOrth = 0;
      int noPasPrec = 0;
      f05.skipBytes(24);
      final SResultatPasCalcul05[] pas = new SResultatPasCalcul05[nbPas];
      for (int i = 0; i < nbPas; i++) {
        pas[i] = new SResultatPasCalcul05();
        pas[i].numeroSegDepart = f05.readInt32();
        pas[i].numeroPas = f05.readInt32();
        pas[i].pointCourantX = f05.readFloat32();
        pas[i].pointCourantY = f05.readFloat32();
        pas[i].angleHoriz = f05.readFloat32();
        pas[i].longueurOnde = f05.readFloat32();
        pas[i].hauteurEau = f05.readFloat32();
        if (pas[i].numeroPas <= noPasPrec) {
          orthogonales[1 + nbOrth++] = i;
        }
        noPasPrec = pas[i].numeroPas;
      }
      orthogonales[1 + nbOrth++] = nbPas;
      f05.close();
      results05_.orthogonales = new SResultatUnitaire05[nbOrth];
      for (int i = 0; i < nbOrth; i++) {
        results05_.orthogonales[i] = new SResultatUnitaire05();
        results05_.orthogonales[i].numeroOrth = i;
        results05_.orthogonales[i].pas = new SResultatPasCalcul05[1
          + ((orthogonales[i + 1] - 1) - orthogonales[i]) / pasImp_];
        for (int j = orthogonales[i]; j < orthogonales[i + 1]; j += pasImp_) {
          results05_.orthogonales[i].pas[(j - orthogonales[i]) / pasImp_] = pas[j];
        }
      }
    }
    catch (final Exception e) {
      //System.err.println(e);
      CDodico.exceptionAxel(this, e);
    }
  }

  private void analyseResultats05(){
    litResultats05();
    dirty05_ = false;
  }

  public SResultats05 resultats05(){
    if ((results05_ == null) || dirty05_) {
      analyseResultats05();
    }
    return results05_;
  }

  public SResultatUnitaire05 resultatUnitaire05(final int _n){
    if ((results05_ == null) || dirty05_) {
      analyseResultats05();
    }
    SResultatUnitaire05 r = null;
    try {
      r = results05_.orthogonales[_n];
    }
    catch (final Exception e) {
      CDodico.exception(this, e);
    }
    return r;
  }

  // InterOrth
  private void analyseResultatsInterOrth(){
    resultsInterOrth_ = new SResultatsInterOrth();
    resultsInterOrth_.interOrth = new SResultatUnitaireInterOrth[results05_.orthogonales.length - 1];
    for (int o = 0; o < resultsInterOrth_.interOrth.length; o++) {
      resultsInterOrth_.interOrth[o] = new SResultatUnitaireInterOrth();
      resultsInterOrth_.interOrth[o].orth1 = o;
      resultsInterOrth_.interOrth[o].orth2 = o + 1;
      resultsInterOrth_.interOrth[o].pas = new SResultatPasInterOrth[Math.min(
          results05_.orthogonales[o].pas.length, results05_.orthogonales[o + 1].pas.length)];
      for (int p = 0; p < resultsInterOrth_.interOrth[o].pas.length; p++) {
        try {
          calculeHauteurHoule(o, p);
        }
        catch (final HauteurHouleException e) {}
      }
    }
    dirtyIO_ = false;
  }

  public SResultatsInterOrth resultatsInterOrth(){
    if ((results05_ == null) || dirty05_) {
      analyseResultats05();
    }
    if ((resultsInterOrth_ == null) || dirtyIO_) {
      try {
        analyseResultatsInterOrth();
      }
      catch (final Throwable t) {
        t.printStackTrace();
      }
    }
    return resultsInterOrth_;
  }

  // OUT
  private void litResultatsOUT(){
    System.out.println("Lecture des resultats OUT");
    String r = "";
    try {
      final FileReader fout = new FileReader(path_ + "vag" + noEtude_ + ".out");
      final char[] buf = new char[512];
      int nbRead = 0;
      while (fout.ready()) {
        nbRead = fout.read(buf, 0, 512);
        r += new String(buf, 0, nbRead);
      }
      fout.close();
    }
    catch (final IOException e) {
      CDodico.exception(this, e);
    }
    resultsOUT_ = new SResultatsOUT();
    resultsOUT_.sortieEcran = r;
  }

  private void analyseResultatsOUT(){
    litResultatsOUT();
    dirtyOUT_ = false;
  }

  public SResultatsOUT resultatsOUT(){
    if ((resultsOUT_ == null) || dirtyOUT_) {
      analyseResultatsOUT();
    }
    return resultsOUT_;
  }

  // privees
  private void calculeHauteurHoule(final int _noOrth,final int _noPoint) throws HauteurHouleException{
    final SResultats05 r = results05_;
    double h = 0D;
    // H0=hauteur de houle au large (1)
    final double h0 = 1D;
    // e0 est l'espacement au large
    double e0 = 1D;
    try {
      if ((_noOrth < r.orthogonales.length) && (_noPoint < r.orthogonales[_noOrth + 1].pas.length)) {
        final double x = r.orthogonales[_noOrth + 1].pas[0].pointCourantX
          - r.orthogonales[_noOrth].pas[0].pointCourantX;
        final double y = r.orthogonales[_noOrth + 1].pas[0].pointCourantY
          - r.orthogonales[_noOrth].pas[0].pointCourantY;
        e0 = Math.sqrt(x * x + y * y);
      } else {
        throw new HauteurHouleException("noOrth=" + _noOrth + ",  noPoint=" + _noPoint);
      }
    }
    catch (final ArrayIndexOutOfBoundsException e1) {
      throw new HauteurHouleException(e1);
    }
    catch (final NullPointerException e2) {
      throw new HauteurHouleException(e2);
    }
    // d=hauteur d'eau (interpolee entre les 2 orth)
    final double d = (r.orthogonales[_noOrth + 1].pas[_noPoint].hauteurEau + 
        r.orthogonales[_noOrth].pas[_noPoint].hauteurEau) / 2;
    // L=longueur d'onde (interpolee entre les 2 orth)
    final double l = (r.orthogonales[_noOrth + 1].pas[_noPoint].longueurOnde + 
        r.orthogonales[_noOrth].pas[_noPoint].longueurOnde) / 2;
    // e est l'espacement entre les 2 orth
    double e = e0;
    // tests deja faits pour e0
    final double x = r.orthogonales[_noOrth + 1].pas[_noPoint].pointCourantX
      - r.orthogonales[_noOrth].pas[_noPoint].pointCourantX;
    final double y = r.orthogonales[_noOrth + 1].pas[_noPoint].pointCourantY
      - r.orthogonales[_noOrth].pas[_noPoint].pointCourantY;
    e = Math.sqrt(x * x + y * y);
    final double k1 = Math.sqrt(e0 / e);
    final double k2 = 1. / Math.sqrt(CMathReel.tanh(2 * Math.PI * d / l)
      * (1 + (4 * Math.PI * d / l) / (CMathReel.sinh(4 * Math.PI * d / l))));
    h = h0 * k1 * k2;
    //System.err.println("noOrth="+noOrth+", noPoint="+noPoint+", e0="+e0+", e="+e+", d="+d+",
    // L="+L+", K1="+K1+", K2="+K2+", H="+H);
    resultsInterOrth_.interOrth[_noOrth].pas[_noPoint] = new SResultatPasInterOrth();
    resultsInterOrth_.interOrth[_noOrth].pas[_noPoint].numeroPas = _noPoint;
    resultsInterOrth_.interOrth[_noOrth].pas[_noPoint].pointCourantX = (r.orthogonales[_noOrth + 1].pas[_noPoint].pointCourantX + r.orthogonales[_noOrth].pas[_noPoint].pointCourantX) / 2;
    resultsInterOrth_.interOrth[_noOrth].pas[_noPoint].pointCourantY = (r.orthogonales[_noOrth + 1].pas[_noPoint].pointCourantY + r.orthogonales[_noOrth].pas[_noPoint].pointCourantY) / 2;
    resultsInterOrth_.interOrth[_noOrth].pas[_noPoint].angleHoriz = (r.orthogonales[_noOrth + 1].pas[_noPoint].angleHoriz + r.orthogonales[_noOrth].pas[_noPoint].angleHoriz) / 2;
    resultsInterOrth_.interOrth[_noOrth].pas[_noPoint].longueurOnde = l;
    resultsInterOrth_.interOrth[_noOrth].pas[_noPoint].hauteurEau = d;
    resultsInterOrth_.interOrth[_noOrth].pas[_noPoint].hauteurHoule = h;
  }

  class HauteurHouleException extends Exception {

    public HauteurHouleException() {
      super();
    }

    public HauteurHouleException(final Exception _e) {
      super(_e.getMessage());
    }

    public HauteurHouleException(final String _msg) {
      super(_msg);
    }
  }
}